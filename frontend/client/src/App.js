import React from 'react';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import PlayGame from './components/PlayGame/PlayGame';
import HomePage from './components/HomePage/HomePage';
import MakeGame from './components/MakeGame/MakeGame';
import LoginPage from "./components/LoginPage/LoginPage";
import privacyAndPolicy from "./components/privacyAndPolicy/privacyAndPolicy";
import termsAndServeices from "./components/termsAndServices/termsAndServices";
import CategoriesSelecting from './components/CategoriesSelecting/CategoriesSelecting';
import OtherThanCategoriesSelecting from './components/OtherThanCategoriesSelecting/OtherThanCategoriesSelecting';

const App = () => (
  <Router>
    <Route path="/" exact component={HomePage} />
    <Route path="/create-new-game" exact component={CategoriesSelecting} />
    <Route path="/game-props" exact component={OtherThanCategoriesSelecting} />
    <Route path="/makeGame" exact component={MakeGame} />
    {/* <Route path="/" exact component={LoginPage} /> */}
    <Route path="/PlayGame/:gameId" component={PlayGame} />
    <Route path="/termsAndServeices" component={termsAndServeices} />
    <Route path="/privacyAndPolicy" component={privacyAndPolicy} />
  </Router>
);

export default App;

// import React from 'react';
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }
//
// export default App;

"""
Clone server Model Five

Author: Min RK <benjaminrk@gmail.com
"""
import json
import os
import random
import uuid
import sys
from pathlib import Path
from random import choice, shuffle
import time
from uuid import uuid4

import django
# from django.conf import settings

import gevent

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'esmFamilProject.settings')

# from django.conf import settings
# settings.configure(DEBUG=True)
# django.setup()

import logging
import struct
import time
# from struct import calcsize

import zmq
# from zmq.eventloop.ioloop import IOLoop, PeriodicCallback
from tornado.ioloop import IOLoop, PeriodicCallback
from zmq.eventloop.zmqstream import ZMQStream

path_to_cwd = Path(os.getcwd())
print(path_to_cwd.parent)
# path_to_cwd = path_to_cwd.parent

sys.path.append(os.path.join(path_to_cwd.parent))
# sys.path.insert(0, os.path.join(os.getcwd(), 'models.py'))
# print(sys.path)

# Ensure settings are read
# from django.core.wsgi import get_wsgi_application

# application = get_wsgi_application()

# from creators.models import Game, Player
from kvmsg import KVMsg
from zhelpers import dump
# from constants import json_state_template


# simple struct for routing information for a key-value snapshot
class Route:
    def __init__(self, socket, identity, subtree):
        self.socket = socket  # ROUTER socket to send to
        self.identity = identity  # Identity of peer who requested state
        self.subtree = subtree  # Client subtree specification


# slice dict based on an string, return all key values in which keys start with an string.
def slicedict(d, s):
    return {k: v for k, v in d.items() if k.startswith(s)}


def send_single(key, kvmsg, route):
    """Send one state snapshot key-value pair to a socket"""
    # check front of key against subscription subtree:
    if kvmsg.key.startswith(route.subtree):
        # Send identity of recipient first
        route.socket.send(route.identity, zmq.SNDMORE)
        kvmsg.send(route.socket)


game_steps = ['joinGame', 'beforeStart', 'round', 'referee', 'roundResult', 'gameResult']
game_steps_props = {'joinGame': {'duration': 30}, 'beforeStart': {'duration': 15},
                    'round': {'duration': 60, 'number_of_rounds': 2}, 'referee': {'duration': 30},
                    'roundResult': {'duration': 30}, 'gameResult': {'duration': 30}}

persian_alphabet = ['الف', 'ب', 'پ', 'ت', 'ث', 'ج', 'چ', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'ژ', 'س', 'ش', 'ص', 'ض',
                    'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ک', 'گ', 'ل', 'م', 'ن', 'و', 'ه', 'ی']

classic_game_fields = ['اسم پسر', 'اسم دختر', 'فامیل', 'شهر', 'کشور', 'میوه', 'ماشین', 'اعضای بدن', 'اشیا',
                       'مشاهیر', 'ورزش', 'فیلم',
                       'کارتون', 'شغل', 'پوشاک']


def get_form_of_round(selected_letters, form_fields, referee=''):  # todo make this forms and cache them.
    random_letter = choice(                                         # todo referee selection.
        [x for x in persian_alphabet if x not in selected_letters])  # random letter that not selected yet.
    form_round = {}
    for field in form_fields:
        form_item = {'l': random_letter, 'f': field, 'w': None, 'r': referee, 'c': None}
        # form_round.append(form_item)
        form_round[field] = form_item
    return random_letter ,form_round


def get_next_step_props(current_step, current_round_number):
    last_round = game_steps_props['round']['number_of_rounds']
    if current_step == 'joinGame':
        next_step = 'beforeStart'
        duration = game_steps_props['beforeStart']['duration']
        round_number = 0
    elif current_step == 'beforeStart':
        next_step = 'round'
        duration = game_steps_props['round']['duration']
        round_number = 1
    elif current_step == 'round':
        next_step = 'referee'
        duration = game_steps_props['referee']['duration']
        round_number = current_round_number
    elif current_step == 'referee':
        is_last_round = False if current_round_number != last_round else True
        next_step = 'roundResult'
        duration = game_steps_props['roundResult']['duration']
        round_number = current_round_number
    elif current_step == 'roundResult':
        if current_round_number < last_round:
            next_step = 'round'
            duration = game_steps_props['round']['duration']
            round_number = current_round_number + 1
        else:
            next_step = 'gameResult'
            duration = game_steps_props['gameResult']['duration']
            round_number = 100
    print(time.time())
    return {'nextStep': next_step, 'endTime': int(time.time()) + duration, 'roundNumber': round_number, 'lastRound': last_round}


def get_latest_kvmsg_related_to_game(last_updates):
    biggest_sequence = 0
    key_with_biggest_sequence = ''
    kvmsg_with_biggest_sequence = {}
    for k, v in last_updates.items():
        if v.sequence > biggest_sequence:
            biggest_sequence = v.sequence
            key_with_biggest_sequence = k
            kvmsg_with_biggest_sequence = v
    return key_with_biggest_sequence, kvmsg_with_biggest_sequence


class CloneServer(object):
    # Our server is defined by these properties
    ip_address = '*'
    ctx = None  # Context wrapper
    kvmap = None  # Key-value store(for messages)
    player_games_kvmap = None  # key-value store(for games of each player)
    game_players_kvmap = None  # key-value store(for players of games)
    game_rounds_forms_kvmap = None  # key-value store(for forms of rounds of games)
    game_name_kvmap = None  # key-value store(for name of games)
    loop = None  # IOLoop reactor
    port = None  # Main port we're working on
    sequence = 0  # How many updates we're at
    assign_game = None  # Handle snapshot requests
    snapshot = None  # Handle snapshot requests
    publisher = None  # Publish updates to clients
    collector = None  # Collect updates from clients

    def __init__(self, port=5555):
        self.port = port
        self.ctx = zmq.Context()
        self.kvmap = {}
        self.player_games_kvmap = {}
        self.game_players_kvmap = {}
        self.game_rounds_forms_kvmap = {}
        self.game_name_kvmap = {}
        self.loop = IOLoop.instance()

        # Set up our clone server sockets
        # self.assign_game = self.ctx.socket(zmq.ROUTER)
        # we can use snapshot when we need request-reply pattern. router-dealer pattern is req-rep pattern but async.
        # but can this pattern push a message to a specific user?
        self.snapshot = self.ctx.socket(zmq.ROUTER)
        self.publisher = self.ctx.socket(zmq.PUB)
        self.collector = self.ctx.socket(zmq.PULL)
        # self.assign_game.bind("tcp://*:%d" % self.port)
        self.snapshot.bind("tcp://{}:{}".format(self.ip_address, self.port + 1))
        # self.snapshot.bind("tcp://*:%d" % (self.port + 1))
        self.publisher.bind("tcp://{}:{}".format(self.ip_address, self.port + 2))
        # self.publisher.bind("tcp://*:%d" % (self.port + 2))
        self.collector.bind("tcp://{}:{}".format(self.ip_address, self.port + 3))
        # self.collector.bind("tcp://*:%d" % (self.port + 3))

        # Wrap sockets in ZMQStreams for IOLoop handlers
        # self.assign_game = ZMQStream(self.assign_game)
        self.snapshot = ZMQStream(self.snapshot)
        self.publisher = ZMQStream(self.publisher)
        self.collector = ZMQStream(self.collector)

        # Register our handlers with reactor
        # self.assign_game.on_recv(self.handle_assign_game)
        self.snapshot.on_recv(self.handle_snapshot)
        self.collector.on_recv(self.handle_collect)
        self.flush_callback = PeriodicCallback(self.flush_ttl, 1000)

        # basic log formatting:
        logging.basicConfig(format="%(asctime)s [%(filename)s:%(lineno)d] %(message)s ", datefmt="%Y-%m-%d %H:%M:%S",
                            level=logging.INFO)

    def start(self):
        # Run reactor until process interrupted
        # self.flush_callback.start()  # todo active this after experiment.
        try:
            self.loop.start()
        except KeyboardInterrupt:
            pass

    '''
    def handle_assign_game(self, msg):
        """assign game requests"""
        if len(msg) != 4 or msg[1] != b"IWANTTOPLAY?":
            print("E: bad request, aborting")
            dump(msg)
            self.loop.stop()
            return
        identity, request, subtree, device_id = msg
        if subtree:
            # Send state snapshot to client
            route = Route(self.assign_game, identity, subtree)

            # For each entry in kvmap, send kvmsg to client
            for k, v in self.kvmap.items():
                send_single(k, v, route)

            # Now send END message with sequence number
            player = Player.objects.get_or_create(device_id=device_id.decode('utf8'))
            # it must be checked that a player do not participate in two games simultaneously. if current game lasted for
            # time that extends max duration of a game. first that game must be finished, second assigned to a game.
            game = Game.objects.get_or_create(is_full=False)
            game[0].player.add(player[0])
            # if attendees of game extends number of players of game, is_full is equal to True.
            logging.info("I: assign game to player=%d %s", self.sequence, game[0].id)
            self.assign_game.send(identity, zmq.SNDMORE)
            kvmsg = KVMsg(self.sequence)
            kvmsg.key = b"GAMEIDSENT"
            # kvmsg.body = subtree
            kvmsg.body = bytes(str(game[0].id), 'utf8')
            kvmsg.send(self.assign_game)
    '''

    def handle_snapshot(self, msg):
        """snapshot requests"""
        if len(msg) != 4 or msg[1] != b"ICANHAZ?":
            print("E: bad request, aborting")
            dump(msg)
            self.loop.stop()
            return
        identity, request, subtree, player_id = msg
        player_id = player_id.decode('utf8')
        # identity, request, subtree = msg
        game_id = subtree
        if subtree:
            # Send state snapshot to client
            route = Route(self.snapshot, identity, subtree)

            # For each entry in kvmap, send kvmsg to client
            for k, v in self.kvmap.items():
                send_single(k, v, route)

            # Now send END message with sequence number
            # logging.info("I: Sending state shapshot=%d" % self.sequence)
            logging.info(
                f'I: Sending state snapshot={self.sequence} identity={identity} request={request} subtree={subtree} msg={msg}')
            self.snapshot.send(identity, zmq.SNDMORE)
            kvmsg = KVMsg(self.sequence)  # todo sequence number in node and python.... . not correct.
            kvmsg.key = b"KTHXBAI"
            kvmsg.body = subtree
            kvmsg.send(self.snapshot)
        else:  # todo check player_id, if player_id is not uniqe send error messagge. this is not necesssary for mobile. because we have id of installation.
            '''
            if a user come with no game_id(subtree) means he/she wants to make a game or he/she wants to join a game but
            he/she do not have or know game_id yet. we must show him/her a message that if he/she wants to join a game 
            import game_id or if he/she wants to make a game click button related to it. 
            '''
            self.snapshot.send(identity, zmq.SNDMORE)
            kvmsg = KVMsg(self.sequence)  # todo this is unused.
            kvmsg.key = b"NOSUBTREE"

            # get active games for this user.
            if player_id in self.player_games_kvmap:
                active_games = self.player_games_kvmap[player_id]
            else:
                active_games = []

            # this game id is used if user would request making a game.
            uuidForGameId = str(uuid4())[:8]
            print(uuidForGameId)
            state_json = {'step': 'gameNotAvailable', 'uuidForGameId': uuidForGameId, 'activeGames': active_games}
            kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
            kvmsg.send(self.snapshot)
            logging.info(
                f'I: Sending gameNotAvailable={self.sequence} identity={identity} request={request} subtree={subtree} msg={msg}')

    def handle_collect(self, msg):  # todo node receive "" updates.
        """Collect updates from clients"""
        # todo add players and join of players to back and front.

        # transform frames to key value message
        kvmsg = KVMsg.from_msg(msg)

        # mark or grade the forms

        # convert json to dic
        body_dic = json.loads(kvmsg.body.decode('utf8'))
        print(body_dic['state_json'])
        state_json = body_dic['state_json']
        # state_json = json.loads(body_dic['state_json'])
        # state_json = body_dic['state_json']
        player_id = state_json['playerId']
        game_id = state_json['gameId']
        round_number = state_json['roundNumber']
        step = state_json['step']
        action = state_json['action']
        # players = state_json['players']

        # handle request for make or join the game
        if step == 'gameNotAvailable':
            if action == 'makeGame':  # todo check merge of addMeToGame with this. # do not think move this to snapshot, or request-reply patterns.
                game_name = state_json['newGameName']

                state_json = {}  # todo check not repeating...
                # if state_json['step'] == 'joinGame':
                state_json.update({'gameId': game_id})
                state_json.update({'step': 'beforeStart'})
                state_json.update({'action': 'prepareToStart'})
                state_json.update({'roundNumber': 0})
                state_json.update({'gameName': game_name})
                next_step_props = get_next_step_props('beforeStart', 0)  # todo make this gameNotAvailable

                game_start_time = int(time.time()) + game_steps_props[next_step_props['nextStep']]['duration']
                state_json.update({'stepEndTime': game_start_time})
                state_json.update({'gameStartTime': game_start_time})

                # add game to games of player.
                if player_id in self.player_games_kvmap:
                    self.player_games_kvmap[player_id].append(game_id)
                else:
                    self.player_games_kvmap[player_id] = [game_id]

                # add player to players
                self.game_players_kvmap[game_id] = [player_id]
                state_json.update({'players': [player_id]})

                # first send kvmsg because of not holding user for making form.
                kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
                # kvmsg.body = str(state_json).encode('utf8')
                # publish update
                self.sequence += 1
                kvmsg.sequence = self.sequence
                kvmsg.send(self.publisher)
                # store body
                kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
                kvmsg.store(self.kvmap)  # todo set ttl because this message has been stored.
                logging.info(f"I: publishing update from gameNotAvailable step if={self.sequence} kvmap={kvmsg}")


                # make form for this user and first round and store in form of rounds kvmap.
                letter, form = get_form_of_round([], classic_game_fields, '')

                rounds_dict = {'gameName': game_name, 'selectedLetters': [letter], 'rounds': {}}
                forms_of_round = {'formTemplate': form, 'playersForms': {}}
                forms_of_round['playersForms'][player_id] = {}
                forms_of_round['playersForms'][player_id]['form'] = form
                rounds_dict['rounds'][next_step_props['roundNumber']] = forms_of_round

                self.game_rounds_forms_kvmap[game_id] = rounds_dict
                return

            elif action == 'addMeToGame':
                # state_json = {}
                # if state_json['step'] == 'joinGame':
                # state_json.update({'gameId': game_id})
                state_json.update({'step': 'beforeStart'})
                state_json.update({'action': 'prepareToStart'})
                state_json.update({'roundNumber': 0})
                try:
                    players = self.game_players_kvmap[game_id]
                except:
                    # todo add sending a message to client so that client can know this gameId is
                    #  not available or is incorrect.
                    print('this gameId is not available.')
                    return

                # add game to games of player.
                if player_id in self.player_games_kvmap:
                    self.player_games_kvmap[player_id].append(game_id)
                else:
                    self.player_games_kvmap[player_id] = [game_id]

                # add player to players of game
                players.append(player_id)
                # self.game_players_kvmap[game_id] = players
                state_json.update({'players': players})
                next_step_props = get_next_step_props('beforeStart', 0)  # todo make this gameNotAvailable
                # state_json.update({'next_step_props': next_step_props})

                game_start_time = int(time.time()) + game_steps_props[next_step_props['nextStep']]['duration']
                # print(game_start_time)
                state_json.update({'gameName': self.game_rounds_forms_kvmap[game_id]['gameName']})
                state_json.update({'stepEndTime': game_start_time})
                state_json.update({'gameStartTime': game_start_time})

                # first send kvmsg because of not holding user for making form.
                kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
                # kvmsg.body = str(state_json).encode('utf8')
                # publish update
                self.sequence += 1
                kvmsg.sequence = self.sequence
                kvmsg.send(self.publisher)
                # store body
                kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
                kvmsg.store(self.kvmap)  # todo set ttl because this message has been stored.
                logging.info(f"I: publishing update from gameNotAvailable step if={self.sequence} kvmap={kvmsg}")

                # make form for this user and first round and store in form of rounds kvmap.
                form = self.game_rounds_forms_kvmap[game_id]['rounds'][next_step_props['roundNumber']]['formTemplate']
                self.game_rounds_forms_kvmap[game_id]['rounds'][next_step_props['roundNumber']]['playersForms'][player_id] = {}
                self.game_rounds_forms_kvmap[game_id]['rounds'][next_step_props['roundNumber']]['playersForms'][player_id]['form'] = form
                return

        # handle request for start round
        elif step == 'beforeStart':
            if action == 'startRound':
                # set step to round, set action to fillForm. todo change sending of forms to router-dealer in order to
                #  not send all forms to each player.(?)
                next_step_props = get_next_step_props(step, round_number)
                state_json.update({'step': next_step_props['nextStep']})  # todo combine this updates with each other.
                state_json.update({'action': 'fillForm'})  # todo move this to get_next_step_props function.
                state_json.update({'stepEndTime': next_step_props['endTime']})
                state_json.update({'roundNumber': next_step_props['roundNumber']})

                state_json['rounds'] = {}
                state_json['rounds'][next_step_props['roundNumber']] = self.game_rounds_forms_kvmap[game_id]['rounds'][next_step_props['roundNumber']]

                '''
                how do you know state_json['players'] are all of players till now?
                should we read this from kvmap i.e from server ram or database?
                yes state_json['players'] may not be last one so, we must read this from ram or database, so we use
                kvmsg_with_biggest_sequence_body['players'].
                
                if two players request for start round we can not make form at this time because of race condition
                in this case we send a kvmsg to clients with a letter i.e 'ف' and send a kvmsg after that with 
                letter 'الف'. although it is not a big problem because form of clients will be changed after 
                milliseconds.
                 but we have a better solution. we can make forms after sending kvmsg in previous 
                step so we keep form in kvmap(in ram of server) but we do not send them to clients. 
                if clients get snapshot they can extract form but they do not have enough time to abuse this info. 
                so we make form  when a user make a game or join the game for first round. in next rounds we make 
                form for users after roundResult step or some step like this before start filling form. 
                '''

        # handle request for referee
        elif step == 'round':
            # handle request for stop round
            if action == 'stopRound':
                state_json.update({'action': 'submitForm'})
                # shuffle players list to select referee randomly.
                shuffle(self.game_players_kvmap[game_id])

            # handle request for submit form
            elif action == 'submitForm':
                # todo implement semaphore for shared resources.
                # set step to referee, set action to refereeForm.
                next_step_props = get_next_step_props(step, round_number)
                state_json.update({'step': next_step_props['nextStep']})
                state_json.update({'action': 'refereeForm'})  # todo move this to get_next_step_props function.
                state_json.update({'stepEndTime': next_step_props['endTime']})
                state_json.update({'roundNumber': next_step_props['roundNumber']})

                # select referee
                players = self.game_players_kvmap[game_id]
                index_of_player_completed_form = players.index(player_id)
                # if player not last item of list selects item next to player as referee else if last item of list
                # selects first item of list as referee
                if index_of_player_completed_form != len(players) - 1:
                    referee = players[index_of_player_completed_form + 1]
                else:
                    referee = players[0]

                state_json.update({'referee': referee})
                state_json.update({'playerRefereed': player_id})

                self.game_rounds_forms_kvmap[game_id]['rounds'][round_number]['playersForms'][player_id]['form'] = state_json['rounds'][str(round_number)]['playersForms'][player_id]['form']
                # state_json['rounds'] = {}
                # state_json['rounds'][next_step_props['roundNumber']] = self.game_rounds_forms_kvmap[game_id]['rounds'][next_step_props['roundNumber']]
                # todo for this step and action:
                # save form and player sent form to a kvmap of previous kvmaps.
                # select a player from players sent form or wait...
                # use router-dealer socket for this because step of some users that do not send forms
                # should not be changed.

        elif step == 'referee':
            if action == 'submitReferee':
                next_step_props = get_next_step_props(step, round_number)
                state_json.update({'step': next_step_props['nextStep']})
                state_json.update({'action': 'showResult'})  # todo move this to get_next_step_props function.
                state_json.update({'stepEndTime': next_step_props['endTime']})
                state_json.update({'roundNumber': next_step_props['roundNumber']})
                state_json.update({'lastRound': next_step_props['lastRound']})
                player_refereed = state_json['playerRefereed']
                form_refereed = state_json['rounds'][str(round_number)]['playersForms'][player_refereed]['form']

                number_of_corrects = 0
                number_of_incorrects = 0
                number_of_not_filled = 0
                for field, fieldForm in form_refereed.items():
                    if fieldForm['w']:
                        if fieldForm['c']:
                            number_of_corrects += 1
                        else:
                            number_of_incorrects += 1
                    else:
                        number_of_not_filled += 1
                form_refereed['roundResult'] = {'nr_c': number_of_corrects, 'nr_inc': number_of_incorrects,
                                       'nr_nf': number_of_not_filled}


                self.game_rounds_forms_kvmap[game_id]['rounds'][round_number]['playersForms'][player_refereed].update({'refereed': True, 'form': form_refereed})

                # first send kvmsg because of not holding user for making form.
                kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
                # kvmsg.body = str(state_json).encode('utf8')
                # publish update
                self.sequence += 1
                kvmsg.sequence = self.sequence
                kvmsg.send(self.publisher)
                # store body
                kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
                kvmsg.store(self.kvmap)  # todo set ttl because this message has been stored.
                logging.info(f"I: publishing update from gameNotAvailable step if={self.sequence} kvmap={kvmsg}")

                # make form for this user and first round and store in form of rounds kvmap.
                # if forms have been made already, should not be made again. if last round should not
                # be made form for next round because there is no next round.
                if next_step_props['roundNumber'] + 1 in self.game_rounds_forms_kvmap[game_id]['rounds'] or round_number == next_step_props['lastRound']:
                    return
                letter, form = get_form_of_round(self.game_rounds_forms_kvmap[game_id], classic_game_fields, '')

                self.game_rounds_forms_kvmap[game_id]['selectedLetters'].append(letter)
                forms_of_round = {}
                # forms_of_round[next_step_props['roundNumber']] = {}
                forms_of_round['formTemplate'] = form
                forms_of_round['playersForms'] = {}
                for player in self.game_players_kvmap[game_id]:
                    forms_of_round['playersForms'][player] = {}
                    forms_of_round['playersForms'][player]['form'] = form
                # the plus 1 is because of we are making forms of next step in this step or roundResult is before start
                # round of round(vs beforestart of game for first round).
                self.game_rounds_forms_kvmap[game_id]['rounds'][next_step_props['roundNumber']+1] = forms_of_round
                return

        elif step == 'roundResult':
            if action == 'startRound':
                # set step to round, set action to fillForm. todo change sending of forms to router-dealer in order to
                #  not send all forms to each player.(?)
                next_step_props = get_next_step_props(step, round_number)
                state_json.update({'step': next_step_props['nextStep']})  # todo combine this updates with each other.
                state_json.update({'action': 'fillForm'})  # todo move this to get_next_step_props function.
                state_json.update({'stepEndTime': next_step_props['endTime']})
                state_json.update({'roundNumber': next_step_props['roundNumber']})

                state_json['rounds'] = {}
                state_json['rounds'][next_step_props['roundNumber']] = self.game_rounds_forms_kvmap[game_id]['rounds'][
                    next_step_props['roundNumber']] # todo put all of this ifs in try except for reliability.

            if action == 'giveResult':
                next_step_props = get_next_step_props(step, round_number)
                state_json.update({'step': next_step_props['nextStep']})  # todo combine this updates with each other.
                state_json.update({'action': 'showTotal'})  # todo move this to get_next_step_props function.
                state_json.update({'stepEndTime': next_step_props['endTime']})
                state_json.update({'roundNumber': next_step_props['roundNumber']})

                rounds = self.game_rounds_forms_kvmap[game_id]['rounds']
                if 'totalResult' in self.game_rounds_forms_kvmap[game_id]:  # if totalResult was calculated it should not be calculated again.
                    state_json['totalResult'] = self.game_rounds_forms_kvmap[game_id]['totalResult']
                else:
                    self.game_rounds_forms_kvmap[game_id]['totalResult'] = []
                    for player in self.game_players_kvmap[game_id]:
                        number_of_corrects = 0
                        number_of_incorrects = 0
                        number_of_not_filled = 0
                        # calculate total result of player
                        for roundNumber in range(1, game_steps_props['round']['number_of_rounds']+1):  # todo not dynamic. and calculate this with pandas.
                            '''
                            todo there is an error here, if one of players refereed a form and sent this to server, server 
                            starts to calculate total result, but other users did not send forms. so there is no 
                            roundResult for them and this part rais a key error. now we extends time to send referee 
                            but this error should be resolved completely. 
                            '''
                            if 'roundResult' in rounds[roundNumber]['playersForms'][player]['form']:
                                number_of_corrects += rounds[roundNumber]['playersForms'][player]['form']['roundResult']['nr_c']
                                number_of_incorrects += rounds[roundNumber]['playersForms'][player]['form']['roundResult']['nr_inc']
                                number_of_not_filled += rounds[roundNumber]['playersForms'][player]['form']['roundResult']['nr_nf']
                            else:
                                continue  # todo add logic of calculation of not participated rounds of user and show them to user.
                        self.game_rounds_forms_kvmap[game_id]['totalResult'].append({'playerId': player, 'nr_c': number_of_corrects, 'nr_inc': number_of_incorrects,
                                           'nr_nf': number_of_not_filled})
                    # save total result of player
                    state_json['totalResult'] = self.game_rounds_forms_kvmap[game_id]['totalResult']

                    # deletes game from active games of this player, in other word closes this game for this player.
                    try:
                        self.player_games_kvmap[player].remove(game_id)
                    except KeyError:
                        print('key not found')  # todo change this to log


        elif step == 'gameResult':
            if action == 'giveResult':
                print('total result.')

        kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
        # kvmsg.body = str(state_json).encode('utf8')
        # publish update
        self.sequence += 1
        kvmsg.sequence = self.sequence
        kvmsg.send(self.publisher)

        # set time to live if any
        try:
            # logging.info('%b',kvmsg.get(b'ttl', 0))
            # logging.info('%f',calcsize(kvmsg.get(b'ttl', 0)))

            ttl = struct.unpack('!f', kvmsg.get(b'ttl', 0))[0]
            # ttl = float(kvmsg.get(b'ttl', 0))
            # ttl = float(ord(kvmsg.get(b'ttl', 0)))
            # ttl = int.from_bytes(kvmsg.get(b'ttl', 0), byteorder='big')
        # except ValueError as e:
        except:
            # print("error", e,)
            ttl = int(kvmsg.get(b'ttl', 0))
            # logging.error(e)
        if ttl:
            # kvmsg[b'ttl'] = b'%f' % (time.time() + ttl)
            kvmsg[b'ttl'] = b'%f' % (time.time() + ttl)

        # save to key value map
        kvmsg.store(self.kvmap)
        # logging.info("I: publishing update=%d", self.sequence)
        logging.info(f"I: publishing update={self.sequence} kvmap={self.kvmap}")

    def flush_ttl(self):
        """Purge ephemeral values that have expired"""
        for key, kvmsg in list(self.kvmap.items()):
            # used list() to exhaust the iterator before deleting from the dict
            self.flush_single(kvmsg)

    def flush_single(self, kvmsg):
        """If key-value pair has expired, delete it and publish the fact
        to listening clients."""
        try:
            ttl = float(kvmsg.get(b'ttl', 0))  # todo can optimize.
        except:
            ttl = struct.unpack('!f', kvmsg.get(b'ttl', 0))[0]
        # ttl = struct.unpack('!d', kvmsg.get(b'ttl', 0))[0]

        # ttl = float(kvmsg.get(b'ttl', 0))
        # ttl = float(ord(kvmsg.get(b'ttl', 0)))
        # ttl = int.from_bytes(kvmsg.get(b'ttl', 0), byteorder='big')
        # print(ttl)
        # ttl = struct.unpack('=f', kvmsg.get(b'ttl', 0))
        if ttl and ttl <= time.time():
            kvmsg.body = b""
            self.sequence += 1
            kvmsg.sequence = self.sequence
            kvmsg.send(self.publisher)
            del self.kvmap[kvmsg.key]
            # logging.info("I: publishing delete=%d", self.sequence)
            logging.info(f"I: publishing delete={self.sequence} kvmap={self.kvmap}")


def clonesrv():
    # loop = asyncio.new_event_loop()
    clone = CloneServer()
    clone.start()


def main():
    # clone = CloneServer()
    # clone.start()
    gevent.joinall([
        gevent.spawn(clonesrv),
        # gevent.spawn(gr2),
        # gevent.spawn(gr3),
    ])


if __name__ == '__main__':
    main()

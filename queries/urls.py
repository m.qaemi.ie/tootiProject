from django.urls import path
# from creators.views import GameList, DeviceInstalledAppList, RoundList, PlayerFillsFormList
from rest_framework.urlpatterns import format_suffix_patterns

from queries.views import GameList

urlpatterns = [
    path('listOfActiveGames/<int:cursor>', GameList.as_view(), name='list-of-active-games'),
    # path('users/install', DeviceInstalledAppList.as_view(), name='install-app'),
    # path('games', GameList.as_view(), name='create-game-with-rounds'),

    # path('games/<uuid:game_id>/rounds', PlayerFillsFormList.as_view(), name='player-fills-forms'),
    # path('users/install', DeviceUserList.as_view(), name='install-app'),
    # path('rounds', RoundList.as_view(), name='save-round-of-game'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

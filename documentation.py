# todo check user is in players and then he/she can update state json for security.
# todo garbage collection of games if players would go and not continue game.
# todo you can receive a message after sending of referee to confirm receiving of form to referee and
# send it to all players to notify them about this user had started referee. this can be implemented for all
# messages and changing of state_json.
'''
the step end time must be set and not start number because maybe users get snapshot and remaining time until end of
step must be calculated client side.
'''
'''
playerId in published kvmsg means this player has requested from server to update state of game.
it can be used to notify to other users.
'''

'''
how do you know state_json['players'] are all of players till now?
should we read this from kvmap i.e from server ram or database?
yes state_json['players'] may not be last one so, we must read this from ram or database, so we use
kvmsg_with_biggest_sequence_body['players'].

if two players request for start round we can not make form at this time because of race condition
in this case we send a kvmsg to clients with a letter i.e 'ف' and send a kvmsg after that with
letter 'الف'. although it is not a big problem because form of clients will be changed after
milliseconds.
 but we have a better solution. we can make forms after sending kvmsg in previous
step so we keep form in kvmap(in ram of server) but we do not send them to clients.
if clients get snapshot they can extract form but they do not have enough time to abuse this info.
so we make form  when a user make a game or join the game for first round. in next rounds we make
form for users after roundResult step or some step like this before start filling form.
'''

''' 
we receive form of each user and save them and save player_id of users sent form. and server prepares 
response message in this "if" and save them to redis. spwan a call_later of 
a call back that publish each of these messages separately after 7 seconds and only set referee to each
message before sending them to users. and server sets not_participated for users which did not send forms
in this 7 seconds. because we allow users to send messages in those seconds we do not change server step
until that deadline
'''



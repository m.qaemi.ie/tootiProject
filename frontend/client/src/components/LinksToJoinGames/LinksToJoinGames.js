import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";
import ChipsList from "../MaterialUiComponents/ChipsList.js";
import Select from "../MaterialUiComponents/Select.js";
import styles from "../assets/jss/views/loginPage.js";

import image from "../assets/img/bg7.jpg";

const axios = require('axios');
const useStyles = makeStyles(styles);

export default function LinksToJoinGames() {
  const [linksToJoinGames, setlinksToJoinGames] = useState('');
  const classes = useStyles();
  useEffect(() => {
    // Make a request for get active games datas
    const urlForGetGameDatas = `http://${process.env.REACT_APP_IP_ADDRESS_OF_DJANGO_SERVER}:${process.env.REACT_APP_PORT_OF_DJANGO_SERVER}/listOfActiveGames/0`; // TODO cursor
    axios.get(urlForGetGameDatas)
    .then(function (response) {
      // handle success
      console.log(response);
      const games_datas = response.data.games_datas;
      let linksToJoinGames = games_datas.map((game_data) =>
        <div>
          <p className={classes.divider}>or join an existing game</p> 
          <CardFooter className={classes.cardFooter}>
            <Link to={{pathname:`/PlayGame/${game_data.gameId}`, state: { gameName: game_data.gameName }}}>
              <Button simple color="primary" size="lg">
                gameId: {game_data.gameId} gameName:{game_data.gameName}/players:{(Object.keys(game_data.playersData)).map((playerId)=>game_data.playersData[playerId].playerName)}
              </Button>
            </Link>
          </CardFooter>
        </div>
      );
      if (linksToJoinGames.length === 0) linksToJoinGames = <p>there is no game to join, make a game please.</p>;
      setlinksToJoinGames(linksToJoinGames);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
  }, []);

  return linksToJoinGames;
}

const zmq = require('zeromq');
const uuid = require('uuid');
const uuid4 = uuid.v4;
require('fast-text-encoding');
var binary = require('binary');
// var Buffer = require('buffer').Buffer;

// import { v4 as uuidv4 } from 'uuid';
// console.log(uuid4()); // ⇨ '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d'
// console.log(typeof(uuid4()));

const seedrandom = require('seedrandom');
// const rng = seedrandom('added entropy.', { entropy: true });
// console.log(rng.int32());

function bin2string(array){
    var result = '';
    for(var i = 0; i < array.length; ++i){
        result+= (String.fromCharCode(array[i]));
    }
    return result;
}

function encode_properties(properties_obj) {
    prop_s = '';
    for (let [key, value] of Object.entries(properties_obj)) {
        // console.log(`${key}: ${value}`);
        prop_s += `${key}=${value}\n`;
    }
    return prop_s;
}

function decode_properties(prop_s) {
    prop = {};
    line_array = prop_s.split("\n");
    for(let line in line_array){
        try{
            let [key, value] = line.split("=");
            prop[key] = value;
        }
        catch (e) {
            console.log("error occured in decode properties.");
            console.log(e);
        }
    }
    return prop;
}


class KVMsg{
    // Message is formatted on wire as 5 frames:
    // frame 0: key (0MQ string)
    // frame 1: sequence (8 bytes, network order)
    // // frame 2: uuid (blob, 16 bytes)
    // frame 2: uuid (blob, 8 bytes)
    // frame 3: properties (0MQ string)
    // frame 4: body (blob)

    // key = null;
    // sequence = 0;
    // uuid = null;
    // properties = null;
    // body = null;

    // constructor(sequence=0, uuid=null, key=null, properties=null, body=null) {
    constructor(sequence, uuid, key, properties, body) {
        //     assert isinstance(sequence, int)
        if(sequence){this.sequence=sequence;}else{this.sequence = 0;}
        if(uuid){
            this.uuid = uuid;
        }else {
            this.uuid = uuid4();
        }
        // if(uuid === null){
        //     this.uuid = uuid4();
        // }else {
        //     this.uuid = uuid;
        // }
        // this.key = key;
        if(key){this.key=key;}else{this.key = '';}
        if(properties){
            this.properties === properties;
        }else {
            this.properties = {};
        }
        // if(properties===null){
        //     this.properties === {};
        // }else {
        //     this.properties = properties;
        // }
        // this.body = body;
        if(body){this.body=body;}else{this.body = '';}
    }

    get_items(k){
        return  this.properties[k];
    }
    set_items(k , v){
        this.properties[k] = v;
    }
    get_prop(k, default_return=null){
        if(k){
            return this.properties[k];
        }else{
            return default_return;
        }
    }
    store(obj){  // todo correct this such that delete all previouse keys.
        // Store me in an object if I have anything to store
        // else delete me from the object.
        if( (this.key!==null) && (this.body!=='') ){
            obj[this.key] = this;
        }else if(this.key in obj){
            delete obj[this.key];
        }
    }
    send(socket){
        //Send key-value message to socket; any empty frames are sent as such.
        let key='';
        if(this.key===null){key = '';}else{key = this.key;}
        // const buf = Buffer.allocUnsafe(64);
        // buf.writeUInt16BE(this.sequence, 0);

        // let seq_s = `${buf}`;
        // let seq_s = new Blob([`${this.sequence}`], {size:8});
        // let seq_s = new Uint8Array([this.sequence]);
        // let seq_s = new Uint8Array([357]);
        // var buffer = new ArrayBuffer(8);
        // let seq_s = new Uint8Array([357], 1, 8);
        // let seq_s = BigUint64Array.from('376766');
        // seq_s = seq_s.from([359]);
        // seq_s[0] = 359;
        let seq_s = `${this.sequence}`; //todo optimization for struct not string
        let body='';
        if(this.body===null){body = '';}else{body = this.body;}
        let prop_s = encode_properties(this.properties);
        socket.send([key, seq_s, this.uuid, prop_s, body])
    }
    static from_msg(msg){
        //Construct key-value message from a multipart message.
        let [key, seq_s, uuid, prop_s, body] = msg;
        const text_decoder = new TextDecoder("utf-8");
        // const text_decoder16 = new TextDecoder("utf-16be");
        // let string_args = args.map((argument)=>{return text_decoder.decode(argument);});
        if(!key || key===''){key='';}else{key = text_decoder.decode(key);}
        // let seq = seq_s;
        let seq;
        // if(!seq_s || seq_s.length===0){seq=0;}else{seq=parseInt(seq_s);}
        // let seq = binary.parse(seq_s).word64bu('seq').vars.seq;
        // seq=parseInt(seq_s);
        if(!seq_s || seq_s.length===0){seq=0;}else{seq = binary.parse(seq_s).word64bu('seq').vars.seq;}
        if(!uuid || uuid.length===0){uuid=uuid4();}else{uuid = text_decoder.decode(uuid);}

        if(!body || body.length===0){body='';}else{body = text_decoder.decode(body);}
        let prop;
        if(!prop_s || prop_s===""){prop = {};}else{prop = decode_properties(text_decoder.decode(prop_s));}
        let kvmsg = new KVMsg(seq, uuid, key, prop, body);
        return kvmsg;
        // return new Promise(function (resolve, reject) {
        //     resolve(kvmsg);
        // });
    }
    static recv(socket){
        //Reads key-value message from socket, returns new kvmsg instance.
        // return new Promise(function (resolve, reject) {
        //     let kvmsg = {};
            socket.on('message', function(msg) {  //todo try catch
                // kvmsg = KVMsg.from_msg(msg);
                // return new Promise(function (resolve, reject) {
                    return msg;
                // });
                // return new Promise(function (resolve, reject) {
                //     resolve(msg);
                // })
            });
            // return kvmsg;
        // });
        // return this.from_msg(socket.recv_multipart())
    }
}

const send_update_to_server = (subtree, player_id) =>{
    // SUBTREE = str(subtree)
    const SUBTREE = subtree;

    // Prepare our context and sockets. in javascript we dont have context because of async...
    let snapshot = zmq.socket('dealer');

    snapshot.linger = 0;
    // snapshot.connect("tcp://localhost:5556");

    let subscriber = zmq.socket('sub');
    subscriber.linger = 0;
    // subscriber.connect("tcp://localhost:5557");
    // subscriber.subscribe(SUBTREE);
    // subscriber.setsockopt(zmq.SUBSCRIBE, SUBTREE.encode())

    let publisher = zmq.socket('push');
    publisher.linger = 0;
    publisher.connect("tcp://localhost:5558");

    const rng = seedrandom('added entropy.', { entropy: true });
    kvmap = {};

    // Get state snapshot
    var sequence = 0;
    snapshot.send(["ICANHAZ?", SUBTREE]);
    snapshot.on('message', function() {  //todo try catch
        let msg = Array.apply(null, arguments);
        kvmsg = KVMsg.from_msg(msg);
        if(kvmsg.key === "KTHXBAI"){
            sequence = kvmsg.sequence;
            console.log(`player id:${player_id}, Received snapshot=${sequence} key=${kvmsg.key} body=${kvmsg.body}`);
            // break;
            // flag = false;
        }
        kvmsg.store(kvmap);
        // console.log(kvmap);
        // console.log(kvmsg);
        // alarm = time.time()+13;
        var d = new Date();
        var alarm = d.getTime()/1000 + 13;
        var form_number = 0;
        subscriber.on("message", function() {
            // console.log('Received message: ', reply.toString());
            let msg = Array.apply(null, arguments);
            // b =binary.word64le('ar').vars;
            // b=binary.parse(arguments).word64bu('seq').vars.seq;
            const text_decoder = new TextDecoder("utf-8");
            b=text_decoder.decode(arguments);
            kvmsg = KVMsg.from_msg(msg);
            // console.log(kvmsg.sequence);
            if(kvmsg.sequence > sequence) {
                sequence = kvmsg.sequence;
                kvmsg.store(kvmap);
                // console.log(kvmap);
                // console.log(kvmsg);
                // action = "update" if kvmsg.body else "delete"
                let action;
                if(!kvmsg.body || kvmsg.body===''){action = 'delete';}else{action = 'update';}
                console.log(`player id:${player_id}, Received ${action}=${sequence} key=${kvmsg.key} body=${kvmsg.body} uuid=${kvmsg.uuid}`);
                // break;
                // flag = false;
            }
        });
        subscriber.connect("tcp://localhost:5557");
        subscriber.subscribe(SUBTREE);
        const send_update_if_timedout = ()=>{
            let date = new Date();
            let time_now =  date.getTime()/1000;
            if(time_now > alarm){
                let kvmsg = new KVMsg();
                rndm_4dig_number = Math.trunc(rng.quick() * (9999 - 1000) + 1000);
                kvmsg.key = SUBTREE + `${rndm_4dig_number}`;
                form = `form_number_${form_number}_node_player_${player_id}`;
                // form_json = `{"my_json_form": ${form}}`;
                form_object = {my_json_form: form};
                kvmsg.body = JSON.stringify(form_object);
                form_number++;
                rndm_btwn_0to30_number = Math.trunc(rng.quick() * (30 - 0) + 0);
                kvmsg.set_items('ttl', `${rndm_btwn_0to30_number}`);

                // kvmsg[b'ttl'] = struct.pack('!f', random.randint(0, 30))

                kvmsg.send(publisher);
                kvmsg.store(kvmap);
                alarm = time_now + 13;
                console.log(time_now);
                console.log(`player id:${player_id}, sent update=${sequence} key=${kvmsg.key} body=${kvmsg.body} uuid=${kvmsg.uuid}`);
            }
        };
        // setInterval(function(){ alert("Hello"); }, 3000);
        setInterval(send_update_if_timedout, 1000);



        // flag = false;
    });
    snapshot.connect("tcp://localhost:5556");
};

game_id ='e3175d25-949f-406a-af01-437fc6c1';
// game_id =uuid4();
console.log(game_id);
// send_update_to_server(game_id, 'player1');
module.exports = send_update_to_server;
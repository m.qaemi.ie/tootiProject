// frontend/src/App.js

// import React, { Component } from "react";
// import React from "react";
// import Modal from "./components/Modal";
// import axios from "axios";
// import "./App.css";
// eslint-disable-next-linenn
// import ReactPaginate from 'react-paginate';

// import {send_update_to_server} from 'socket_modules/cloneclient5';
// import './cloneclient5';
// const send_update = require('./cloneclient5');
// class FormOfSnapShotOfGame extends React.Component{
//     constructor(props) {
//         super(props);
//         // this.state = {value: ''};
//         this.state = {
//             form:{}
//         };
//         this.handleChange = this.handleChange.bind(this);
//         this.handleSubmit = this.handleSubmit.bind(this);
//     }
//
//     handleChange(event) {
//         this.setState({value: event.target.value});
//     }
//
//     handleSubmit(event) {
//         alert('A name was submitted: ' + this.state.value);
//         event.preventDefault();
//     }
//
//     render() {
//         return (
//             <form onSubmit={this.handleSubmit}>
//                 <label>
//                     Name:
//                     <input type="text" value={this.state.value} onChange={this.handleChange} />
//                 </label>
//                 <input type="submit" value="Submit" />
//             </form>
//         );
//     }
// }


const inputParsers = {
    date(input) {
        const [month, day, year] = input.split('/');
        return `${year}-${month}-${day}`;
    },
    uppercase(input) {
        return input.toUpperCase();
    },
    number(input) {
        return parseFloat(input);
    },
};

class FormOfSnapShotOfGame extends React.Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const form = event.target;
        const data = new FormData(form);

        // for (let name of data.keys()) {
        //     const input = form.elements[name];
        //     const parserName = input.dataset.parse;
        //
        //     if (parserName) {
        //         const parser = inputParsers[parserName];
        //         const parsedValue = parser(data.get(name));
        //         data.set(name, parsedValue);
        //     }
        // }

        // fetch('/api/form-submit-url', {
        //     method: 'POST',
        //     body: data,
        // });
        let form_completed = {};
        for(let [k, v] of data.entries()){
            console.log(k + '=' + v);
            form_completed[k] = v;
        }
        console.log(form_completed);
        console.log(JSON.stringify(form_completed));
        // console.log(data);
        // alert('A name was submitted: ' + JSON.stringify(data));
        // send_update('e3175d25-949f-406a-af01-437fc6c1', 'player1_node')
        // send_update_to_server('e3175d25-949f-406a-af01-437fc6c1', 'player_node1')
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input
                    name="username"
                    type="text"
                    data-parse="uppercase"
                />

                <input name="email" type="email" />

                <input
                    name="birthdate"
                    type="text"
                    data-parse="date"
                />

                <button>Send Esm Famils</button>
            </form>
        );
    }
}


// class FormCompletion extends React.Component {
//     render(){
//         const element = (
//             // <!DOCTYPE html>
//             <html lang="en">
//             <head>
//                 <meta charSet="utf-8"></meta>
//                 <meta httpEquiv="X-UA-Compatible" content="IE=edge"></meta>
//                 <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
//                 <title>Bootstrap Real Estate Page Template</title>
//                 {/*<link href="bootstrap-4.3.1.css" rel="stylesheet"></link>*/}
//                 <link href="js/bootstrap-4.3.1.css" rel="stylesheet" type="text/css"></link>
//                 <link href="./App.css" rel="stylesheet" type="text/css"></link>
//                 {/*<link href="css/ghalichin.css" rel="stylesheet" type="text/css"></link>*/}
//                 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
//                 <script src="js/jquery-3.3.1.min.js"></script>
//
//                 <script src="js/popper.min.js"></script>
//                 {/*<script src="js/bootstrap-4.3.1.js"></script>*/}
//
//             </head>
//             <body>
//             <div>hello esmfamil</div>
//             <FormOfSnapShotOfGame/>
//             {/*<script src="jquery-3.3.1.min.js"></script>*/}
//             {/*<script src="popper.min.js"></script>*/}
//             {/*<script src="bootstrap-4.3.1.js"></script>*/}
//
//             </body>
//             </html>
//         );
//
//         return element;
//     }
// }

// export default FormCompletion;

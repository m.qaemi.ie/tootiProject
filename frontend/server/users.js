const users = [];

//add user and his/her current socket to a game or change socket of that user.
const addUserToGameOrChangeSocketId = ({ socketId, name, gameId, playerId }) => {
  name = name.trim().toLowerCase();
  gameId = gameId.trim().toLowerCase();
  playerId = playerId.trim().toLowerCase();

  // const existingUser = users.find((user) => user.gameId === gameId && user.name === name);
  // find user with this specific socketId
  const existingUserWithThisSocketId = users.find((user) => user.gameId === gameId && user.playerId === playerId && user.socketId === socketId);

  // if (!playerId || !gameId || !name) return {error: 'Username and game are required.'};
  if (!playerId || !name) return {error: 'Username and game are required.'};
  // if(existingUser) return { error: 'Username is taken.' };
  // if(existingUserWithThisSocketId) return {userExisted : true};
  if(existingUserWithThisSocketId) {
    return {user: existingUserWithThisSocketId};
  } else {
    const user = { socketId, name, gameId, playerId };

    users.push(user);  // todo what happen if one user had disconnected but socket not deleted.
    // todo delete socket if one user would have existing socket but connect from another socket.
    // console.log(user);
    console.log(users);
  
    return { user };  
  }
  // if (existingUserWithThisSocketId) {
  //   return {userExisted : true};
  //   }
  // else {
  //   return {userExisted: false} ;
  // }
  // const user = { id, name, room };
};

const addUser = ({ id, name, room }) => {
  name = name.trim().toLowerCase();
  room = room.trim().toLowerCase();

  const existingUser = users.find((user) => user.room === room && user.name === name);

  if(!name || !room) return { error: 'Username and room are required.' };
  if(existingUser) return { error: 'Username is taken.' };

  const user = { id, name, room };

  users.push(user);

  return { user };
};

const removeUser = (id) => {
  const index = users.findIndex((user) => user.id === id);

  if(index !== -1) return users.splice(index, 1)[0];
};

const getUser = (id) => users.find((user) => user.id === id);

const getUserWithPlayerId = (playerId) => users.find((user) => user.playerId === playerId);

const getUsersInRoom = (room) => users.filter((user) => user.room === room);

module.exports = { addUser, removeUser, getUser, getUserWithPlayerId, getUsersInRoom, addUserToGameOrChangeSocketId };

json_state_template = {
    'gameId': '',
    'duration': '',
    'step': '',
    'action': '',
    'stepEndTime': '',
    'roundNumber': '',
    'gameStartTime': '',
    # 'next_step_props': '',
    'players': [],
    # 'after_next_step_props': '',
    'gameType': '',
    'rounds': {},  # each round number is key. round data template
    # 'players_data': {}  # each player data key is user uuid.
}

# player_data_template = {
#     'userName': '',
#     'left': None,
#     'rounds': {
#         1: {},
#     },
# }

round_data_template = {
    # 'duration': None,
    'endTime': None,  # duration of round plus start time calculated in server
    'startTime': None,  # when first user clicks on button I'm prepared. server set a time to start round.
    'finished': None,
    # 'roundNumber': None,
    'playersForms': {
        'playerId1': {
            'completedTime': None,  # client side time stamp
            'receivedTime': None,  # server side time stamp
            'participated': True,  # if not received on time.
            'formReceived': False,  # if form received.
            'referee': 'playerId2',
            'form': {}
        }
    }
}

form_template = {'l': None, 'f': None, 'w': None, 'r': None, 'c': None}
# l: letter,  f: field, w: word, r: referee, c: correctness

form_example = {
    'gameId': '',
    'duration': '',
    'step': '',
    'roundNumber': '',
    'gameStartTime': '',
    'next_step_props': '',
    'gameType': '',
    'playerId': {
        'userName': '',
        'left': True,
        'rounds': [
            {
                1: {
                    'duration': '',
                    'roundNumber': '',
                    'completedTime': '',
                    'receivedTime': '',
                    'participated': True,
                    'formReceived': True,
                    'form': [
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        # l: letter,  f: field, w: word, r: referee, c: correctness
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                    ]
                },
                2: {
                    'duration': '',
                    'roundNumber': '',
                    'completedTime': '',
                    'receivedTime': '',
                    'participated': True,
                    'formReceived': True,
                    'form': [
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        # l: letter,  f: field, w: word, r: referee, c: correctness
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                        {'l': 'الف', 'f': 'اسم شهر', 'w': 'اراک', 'r': '', 'c': True},
                    ]
                }
            }
        ]
    }
}
# step: 'joinGame' means client do not know which game he is in.

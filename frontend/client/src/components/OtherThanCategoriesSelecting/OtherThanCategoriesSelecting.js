import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";
import ChipsList from "../MaterialUiComponents/ChipsList.js";
import Select from "../MaterialUiComponents/Select.js";
import styles from "../assets/jss/views/loginPage.js";
import image from "../assets/img/bg7.jpg";
import { Redirect } from "react-router-dom";

const axios = require('axios');
const persian_alphabets = ['الف', 'ب', 'پ', 'ت', 'ث', 'ج', 'چ', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'ژ', 'س', 'ش', 'ص', 'ض',
                  'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ک', 'گ', 'ل', 'م', 'ن', 'و', 'ه', 'ی'];
function makeid(length) {  // todo do this server-side.
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const useStyles = makeStyles(styles);

export default function OtherThanCategoriesSelecting(props) {
// export default function MakeOrJoinGame(props) {
  
  const [cardAnimaton, setCardAnimation] = useState("cardHidden");
  const [gameId, setGameId] = useState('');
  const [newGameName, setNewGameName] = useState('');
  const [selectedAlphabets, setSelectedAlphabets] = useState(persian_alphabets);
  const [playersCount, setPlayersCount] = useState(2);
  const [roundsCount, setRoundsCount] = useState(3);
  const [redirect, setRedirect] = useState("");
  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  setTimeout(function() { // redirects page to home page after 5 minutes.
    setRedirect("/");
  }, 5 * 60 * 1000);
  
  const classes = useStyles();
  // const { ...rest } = props;
  const [linksToJoinGame, setlinksToJoinGame] = useState('');

  console.log('player_auth value:', Cookies.getJSON('player_auth') || {});
  let {access_token='', name='', playerId='',} = Cookies.getJSON('player_auth') || {};
  // if player auth was in cookies it will use them but if was not in cookies it will generate them.
  if (!name || !playerId) { //todo check expiration and refresh of token... expiration of playerId and name cookies should be equal to access token.
    let names = ['wasp', 'gaur', 'butterfly', 'bee', 'ant'];
    name = names[Math.floor(Math.random() * names.length)]; // select random from names
    playerId = makeid(6);
    const expireseconds= new Date(new Date().getTime() + 3 * 1000); // after 15 seconds! for test... todo
    Cookies.set('player_auth', { name , playerId }, { expires: expireseconds });
    console.log('random name and player id assigned.', name, playerId);
  } else {
    console.log('player id and name exists because access token exists.', );
  }


  useEffect(() => {
    // Make a request for get valid id for game id
    const urlForGetValidId = `http://${process.env.REACT_APP_IP_ADDRESS_OF_DJANGO_SERVER}:${process.env.REACT_APP_PORT_OF_DJANGO_SERVER}/validId`;
    axios.get(urlForGetValidId)
    .then(function (response) {
      // handle success
      setGameId(response.data.valid_id);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
  }, []);

  if (redirect) {
    return <Redirect to={redirect} />
  }
  let {selectedCategories} = props.location.state || {};  
  if (!selectedCategories) {
    setRedirect("/");
  }
  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="Esm Famil"
        rightLinks={<HeaderLinks />}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <h4>Login</h4>
            <h4>player id: {playerId}</h4>
            <h4>name: {name}</h4>
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Make Game Or Join One</h4>
                  </CardHeader>
                  {/* <p className={classes.divider}>Make Game</p> */}
                  <CardBody>
                    <ChipsList items={persian_alphabets} selectedItems={selectedAlphabets} setSelectedItems={setSelectedAlphabets} minOfSelectedItems={5} maxOfSelectedItems={32}/>
                    <Select options={[2, 3, 4, 5, 6]} value={playersCount} setValue={setPlayersCount} title="Players Count"/>
                    <Select options={[2, 3, 4, 5, 6]} value={roundsCount} setValue={setRoundsCount} title="Rounds Count"/>
                    <CustomInput
                      labelText="New Game Name..."
                      id="newGameName"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "text",
                        onChange:(e) => setNewGameName(e.target.value),
                        endAdornment: (
                          <InputAdornment position="end">
                            <People className={classes.inputIconsColor} />
                          </InputAdornment>
                        )
                      }}
                    />
                    <CardFooter className={classes.cardFooter}>
                      <Link to={{
                        pathname: `/PlayGame/${gameId}`,
                        // search: "?sort=name",
                        // hash: "#the-hash",
                        state: { gameName: newGameName, action: 'makeGame', configOfGame: {selectedCategories, selectedAlphabets, playersCount, roundsCount} }
                        }}
                      >
                        <Button simple color="primary" size="lg">
                          Make Game
                        </Button>
                      </Link>
                    </CardFooter>
                    <p className={classes.divider}>or join an existing game</p>
                    {linksToJoinGame}
                  </CardBody>
                </form>
                {linksToJoinGame}
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

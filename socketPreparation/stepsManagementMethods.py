import json
from socketPreparation.classicEFUtilities import steps_props, each_round_steps, classicef_game_steps_props
from socketPreparation.gameManagementMethods import calculate_store_total_result, close_game
from socketPreparation.generalUtilities import convert
from socketPreparation.prepareSendResponseMethods import prepare_publish_store_msg
from socketPreparation.pyredis import hgetall_name_dict, rounds_data_db, hset_name_dict, get_name_bytes, game_data_db


def player_not_in_game_flow(logic_obj, steps_allowed):
    """check if user is in flow of game return true else send a message to user to give snapshot"""
    if logic_obj.round_number == logic_obj.game_data['serverStep']['roundNumber']:
        if logic_obj.game_data['serverStep'] in steps_allowed:
            return False
    else:
        # update state_json
        state_json_update = {'step': 'notInFlow', 'action': 'giveSnapshot', 'roundNumber': 0, }
        logic_obj.state_json.update(state_json_update)

        # send reply kvmsg
        logic_obj.send_snapshot_kvmsg(logic_obj.state_json, logic_obj.step, logic_obj.kvmsg_received, logic_obj.dealer_socket,
                                 logic_obj.identity_of_requester)
        return True


def push_game_to_next_stage(logic_obj):
    current_step = logic_obj.step
    if current_step == 'beforeStart':  # todo you can cache this in a function
        current_step_index = -1
        next_step = each_round_steps[current_step_index + 1]
    elif current_step == each_round_steps[-1]:
        if logic_obj.round_number == str(logic_obj.game_data['configOfGame']['roundsCount']):
            next_step = 'gameResultShowing'
        else:
            logic_obj.round_number = str(int(logic_obj.round_number) + 1)
            next_step = each_round_steps[0]
    else:
        current_step_index = each_round_steps.index(current_step)
        next_step = each_round_steps[current_step_index + 1]
    logic_obj.step = next_step  # steps just use for check if player is in flow of game or not  and nothing else.
    logic_obj.game_data['serverStage'] = {'roundNumber': logic_obj.round_number, 'step': next_step, }


def go_to_next_stage_if_necessary(logic_obj):
    """"""
    server_side_stage = logic_obj.game_data['serverStage']
    action = logic_obj.action
    action_changing_step = steps_props['steps'][server_side_stage['step']]['action_changing_step']
    if action == action_changing_step:
        push_game_to_next_stage(logic_obj)


def player_is_in_flow_of_game(logic_obj):  # todo change logic_obj to request_obj
    """check if player is in flow of game if is in game flow will update server side stage and set step to next
    step(go to next step) and return true otherwise return false."""
    server_side_stage = logic_obj.game_data['serverStage']
    action = logic_obj.action
    if server_side_stage['roundNumber'] == logic_obj.round_number:
        if server_side_stage['step'] == logic_obj.step:
            actions_allowed_in_stage = steps_props['steps'][server_side_stage['step']]['actions_allowed']
            # if server_side_stage is in actions allowed to send request in this step...
            if action in actions_allowed_in_stage:
                return True
    else:
        # update state_json
        state_json_update = {'step': 'notInFlow', 'action': 'giveSnapshot', 'roundNumber': 0, }
        logic_obj.state_json.update(state_json_update)
        return False


def go_to_next_round(logic_obj):
    logic_obj.game_data = get_name_bytes(game_data_db, logic_obj.game_id)
    logic_obj.action = 'startRound'
    logic_obj.player_is_in_flow = player_is_in_flow_of_game(logic_obj)
    if not logic_obj.player_is_in_flow: return
    push_game_to_next_stage(logic_obj)
    # logic_obj.round_number = str(int(logic_obj.round_number) + 1)
    # logic_obj.game_data['serverStage']['roundNumber'] = logic_obj.round_number
    prepare_publish_store_msg(logic_obj)


def go_to_give_total(logic_obj):
    logic_obj.action = 'giveTotal'
    logic_obj.step = each_round_steps[-1]
    push_game_to_next_stage(logic_obj)
    calculate_store_total_result(logic_obj)


def confirm_results(logic_obj):
    """"""
    all_players_confirmed_results = False
    name_round_data = 'gameId:' + logic_obj.game_id + 'roundNumber:' + logic_obj.round_number + 'roundData'
    logic_obj.round_data = convert(hgetall_name_dict(rounds_data_db, name_round_data))
    players_confirmed_results = json.loads(logic_obj.round_data['playersConfirmedResults'])
    players_confirmed_results.append(logic_obj.player_id)
    players_participated_in_round = json.loads(logic_obj.round_data['playersParticipatedInRound'])
    if set(players_participated_in_round) == set(players_confirmed_results):
        all_players_confirmed_results = True
    hset_name_dict(rounds_data_db, name_round_data, {'playersConfirmedResults': json.dumps(players_confirmed_results).encode('utf-8'), 'playersParticipatedInRound': json.dumps(logic_obj.game_data['gamePlayers']).encode('utf-8'), })
    return all_players_confirmed_results

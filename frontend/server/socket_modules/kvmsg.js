const uuid = require('uuid');
const uuid4 = uuid.v4;
var binary = require('binary');
require('fast-text-encoding');
const text_decoder = new TextDecoder("utf-8");

var kvmap = {};

function bin2string(array){
    var result = '';
    for(var i = 0; i < array.length; ++i){
        result+= (String.fromCharCode(array[i]));
    }
    return result;
}

function encode_properties(properties_obj) {
    prop_s = '';
    for (let [key, value] of Object.entries(properties_obj)) {
        // console.log(`${key}: ${value}`);
        prop_s += `${key}=${value}\n`;
    }
    return prop_s;
}

function decode_properties(prop_s) {
    prop = {};
    line_array = prop_s.split("\n");
    for(let line in line_array){
        try{
            let [key, value] = line.split("=");
            prop[key] = value;
        }
        catch (e) {
            console.log("error occured in decode properties.");
            console.log(e);
        }
    }
    return prop;
}


class KVMsg{
    constructor(sequence, uuid, key, properties, body) {
        if(sequence){this.sequence=sequence;}else{this.sequence = 0;}
        if(uuid){
            this.uuid = uuid;
        }else {
            this.uuid = uuid4();
        }
        if(key){this.key=key;}else{this.key = '';}
        if(properties){
            this.properties === properties;
        }else {
            this.properties = {};
        }
        if(body){this.body=body;}else{this.body = '';}
    }

    get_items(k){
        return  this.properties[k];
    }
    set_items(k , v){
        this.properties[k] = v;
    }
    get_prop(k, default_return=null){
        if(k){
            return this.properties[k];
        }else{
            return default_return;
        }
    }
    store(obj){  // todo correct this such that delete all previouse keys.
        // Store me in an object if I have anything to store
        // else delete me from the object.
        if( (this.key!==null) && (this.body!=='') ){
            obj[this.key] = this;
        }else if(this.key in obj){
            delete obj[this.key];
        }
    }
    send(socket){
        //Send key-value message to socket; any empty frames are sent as such.
        let key='';
        if(this.key===null){key = '';}else{key = this.key;}
        let seq_s = `${this.sequence}`; //todo optimization for struct not string
        let body='';
        if(this.body===null){body = '';}else{body = this.body;}
        let prop_s = encode_properties(this.properties);
        socket.send([key, seq_s, this.uuid, prop_s, body])
    }
    static from_msg(msg){
        //Construct key-value message from a multipart message.
        let [key, seq_s, uuid, prop_s, body] = msg;
        if(!key || key===''){key='';}else{key = text_decoder.decode(key);}
        let seq;
        if(!seq_s || seq_s.length===0){seq=0;}else{seq = binary.parse(seq_s).word64bu('seq').vars.seq;}
        if(!uuid || uuid.length===0){uuid=uuid4();}else{uuid = text_decoder.decode(uuid);}

        if(!body || body.length===0){body='';}else{body = text_decoder.decode(body);}
        let prop;
        if(!prop_s || prop_s===""){prop = {};}else{prop = decode_properties(text_decoder.decode(prop_s));}
        let kvmsg = new KVMsg(seq, uuid, key, prop, body);
        return kvmsg;
    }
    static recv(socket){
        //Reads key-value message from socket, returns new kvmsg instance.
        // return new Promise(function (resolve, reject) {
            socket.on('message', function(msg) {  //todo try catch
                    return msg;
            });
    }
}

module.exports = {
    KVMsg,
    kvmap,
};

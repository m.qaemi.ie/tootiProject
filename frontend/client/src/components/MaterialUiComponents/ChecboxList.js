import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import CommentIcon from '@material-ui/icons/Comment';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CheckboxList({checked, setChecked, formFields, disabledCheckbox}) {
  const classes = useStyles();
// const [checked, setChecked] = React.useState([0]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(checked => newChecked);
    console.log(checked);
  };

  return (
    <List className={classes.root}>
      {Object.keys(formFields).map((key) => {
        const field = formFields[key];
        const labelId = `checkbox-list-label-${field.f}`;

        return (
          <ListItem disabled={disabledCheckbox} key={field.f} role={undefined} dense button onClick={field.w?handleToggle(field.f):null}>
            <ListItemIcon>
              <Checkbox
                disabled={field.w?false:true}
                edge="start"
                checked={field.w?checked.indexOf(field.f) !== -1:false}
                tabIndex={-1}
                disableRipple
                // color="secondary"
                inputProps={{ 
                    'aria-labelledby': labelId,
                    // 'aria-label': 'disabled checked checkbox',
                    // 'aria-label': 'secondary checkbox',
                }}
              />
            </ListItemIcon>
            <ListItemText id={labelId} primary={field.w?field.w:"نزده"} />
            <ListItemText id={labelId} primary={field.f} />
            {/* <ListItemSecondaryAction>
              <IconButton edge="end" aria-label="comments">
                <CommentIcon />
              </IconButton>
            </ListItemSecondaryAction> */}
          </ListItem>
        );
      })}
    </List>
  );
}

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function NativeSelects({value, setValue, options, title}) {
  const classes = useStyles();

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const optionsComponents = options.map((option) =>
    <option value={option} id={option}>{option}</option>
  );

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="age-native-simple">{title}</InputLabel>
        <Select
          native
          value={value}
          onChange={handleChange}
          label={title}
          inputProps={{
            name: title,
            id: title,
          }}
        >
          {/* <option aria-label="None" value="" /> */}
          {optionsComponents}
        </Select>
      </FormControl>
    </div>
  );
}

import json
import logging
import logging.handlers

class StructuredMessage(object):
    def __init__(self, message, **kwargs):
        self.message = message
        self.kwargs = kwargs

    def __str__(self):
        # return '%s = %s' % (self.message, json.dumps(self.kwargs))
        # return '%s : %s,' % (self.message, self.kwargs)
        return '{} : {},'.format(self.message, self.kwargs)

# _ = StructuredMessage  # optional, to improve readability

# logging.basicConfig(level=logging.INFO, format='%(message)s')
# logging.info(_('message 1', foo='bar', bar='baz', num=123, fnum=123.456))



# Set up logger 1
LOG_FILENAME_FOR_KVMSG = 'logsOfLoggers/kvmsg_logs.out'

kvmsg_logger = logging.getLogger('kvmsg_logger')
kvmsg_logger.setLevel(logging.INFO)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(
              LOG_FILENAME_FOR_KVMSG, backupCount=1)
              # LOG_FILENAME_FOR_KVMSG, maxBytes=20000 , backupCount=1)
formatter = logging.Formatter(fmt="'%(levelname)s_%(filename).8s_%(lineno)d_%(asctime)s_%(process)d_%(processName)s_%(thread)d_%(threadName)s_%(message)s", datefmt="%Y_%m_%d_%H_%M_%S")
handler.setFormatter(formatter)
kvmsg_logger.addHandler(handler)



# Set up logger 2
# recieved state_json will be logged in this file.
LOG_FILE_FOR_CLASSICEFLOGIC = 'logsOfLoggers/classicEF_logs.out'
# LOG_FILE_FOR_CLASSICEFLOGIC_COLLECTED = 'logsOfLoggers/classicEF_collected_logs.out'

classicEF_collected_logger = logging.getLogger('classicEF_collected_logger')
classicEF_collected_logger.setLevel(logging.INFO)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(
              LOG_FILE_FOR_CLASSICEFLOGIC, backupCount=1)
              # LOG_FILENAME_FOR_KVMSG, maxBytes=20000 , backupCount=1)

formatter = logging.Formatter(fmt="'%(levelname)s_collected_state_%(filename).8s_%(lineno)d_%(asctime)s_%(process)d_%(processName)s_%(thread)d_%(threadName)s_%(message)s", datefmt="%Y_%m_%d_%H_%M_%S")
handler.setFormatter(formatter)

classicEF_collected_logger.addHandler(handler)


# Set up logger 3
# recieved state_json will be logged in this file.
# LOG_FILE_FOR_CLASSICEFLOGIC_PUBLISHED = 'logsOfLoggers/classicEF_published_logs.out'

classicEF_published_logger = logging.getLogger('classicEF_published_logger')
classicEF_published_logger.setLevel(logging.INFO)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(
              LOG_FILE_FOR_CLASSICEFLOGIC, backupCount=1)
              # LOG_FILENAME_FOR_KVMSG, maxBytes=20000 , backupCount=1)

formatter = logging.Formatter(fmt="'%(levelname)s_published_state_%(filename).8s_%(lineno)d_%(asctime)s_%(process)d_%(processName)s_%(thread)d_%(threadName)s_%(message)s", datefmt="%Y_%m_%d_%H_%M_%S")
handler.setFormatter(formatter)

classicEF_published_logger.addHandler(handler)

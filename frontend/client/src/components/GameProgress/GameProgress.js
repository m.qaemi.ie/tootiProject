import React, { useState } from 'react';
// import { Link } from 'react-router-dom';

// import CounterDown from '../CounterDown/CounterDown';
// import TextContainer from '../TextContainer/TextContainer';
// import Messages from '../Messages/Messages';
// import InfoBar from '../InfoBar/InfoBar';
import Form from '../Form/Form';
import JoinGame from '../JoinGame/JoinGame';
import BeforeStart from '../BeforeStart/BeforeStart';
// import RoundResult from '../RoundResult/RoundResult';
import GameResult from '../GameResult/GameResult';
import Referee from '../Referee/Referee';
// import StartOrResumeGame from '../StartOrResumeGame/StartOrResumeGame';

import './GameProgress.css';

// eslint-disable-next-line react/prop-types
const GameProgress = ({ state, sendMessage, changeStep,}) => {
  let {step, action} = state; 
  // const [name, setName] = useState('');
  // const [room, setRoom] = useState('');
  const [isActive, setIsActive] = useState(true);
  console.log('in GameProgress root:', state, sendMessage);

  let stepComponent = <div></div>;
  // var interval;
  if (step) { // todo convert to switch.
    if (step === 'joinGame') { // todo sockets must be deactivated if not used.
      stepComponent = <JoinGame state={state} />;
      return stepComponent;
    } else if (step === 'beforeStart') {
      stepComponent = <BeforeStart state={state} sendMessage={sendMessage}/>;
      // if (isActive) {
      //   let state_json = { playerId, gameId, step: 'beforeStart', action: 'startRound', roundNumber};
      //   console.log(state_json);
      //   const sendData = async () => {
      //     interval = await setTimeout(() => socket.emit('sendUpdate', {
      //       name,
      //       gameId,
      //       playerId,
      //       message: JSON.stringify(state_json),
      //     },
      //     () => setIsActive(false)),
      //     startNumber * 1000);
      //     // setData(result.data);
      //   };
      //   try {
      //     sendData();
      //   } catch (e) {
      //     console.log(e);
      //   }
      // }  
      return stepComponent;

    } else if (step === 'formFilling' || step === 'formSubmitting') {
      if (action === 'waitToReceiveFormToReferee') return <h1>wait until send form to referee.</h1>;
      stepComponent = <Form state={state} sendMessage={sendMessage} />;
      return stepComponent;

    } else if (step === 'formRefereeing') {
      // if (action === 'waitToReceiveFormToReferee') return <h1>wait until send form to referee.</h1>;
      if (action === 'notifyPlayersSentForm') return <h1>you have not sent form of this round in time.</h1>;
      stepComponent = <Referee state={state} sendMessage={sendMessage} changeStep={changeStep} />;
      return stepComponent;

    } else if (step === 'gameResultShowing') {
      stepComponent = <GameResult state={state}/>;
      // stepComponent = <h1>hello game result.</h1>;
      return stepComponent;

    } else if (step === 'notInFlow') {
      // stepComponent = <GameResult startNumber={startNumber} totalResult={totalResult}/>;
      stepComponent = <h1>you are not in flow, is giving snapshot from server.</h1>;
      return stepComponent;

    } else {
      stepComponent = <JoinGame state={state} />;
      return stepComponent;

    }
  }
  // return null;
  return <div></div>;

  // return (
  //   <div className="joinOuterContainer">
  //     <div className="joinInnerContainer">
  //       <h1 className="heading">progress of Game</h1>
  //       <CounterDown startNumber={startNumber} />
  //     {/*  <div>*/}
  //     {/*    <input placeholder="Name" className="joinInput" type="text" onChange={(event) => setName(event.target.value)} />*/}
  //     {/*  </div>*/}
  //     {/*  <div>*/}
  //     {/*    <input placeholder="Room" className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)} />*/}
  //     {/*  </div>*/}
  //     {/*  <Link onClick={(e) => ((!name || !room) ? e.preventDefault() : null)} to={`/chat?name=${name}&room=${room}`}>*/}
  //     {/*    <button className="button mt-20" type="submit">Sign In</button>*/}
  //     {/*  </Link>*/}
  //     </div>
  //   </div>
  // );
};

export default GameProgress;

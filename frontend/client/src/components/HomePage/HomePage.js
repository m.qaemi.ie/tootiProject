import React, { useState } from "react";
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";
import LinksToJoinGames from "../LinksToJoinGames/LinksToJoinGames";

import styles from "../assets/jss/views/loginPage.js";

import image from "../assets/img/bg7.jpg";

import { GoogleLogin } from 'react-google-login';
import axios from 'axios';

const axios_rest_auth = axios.create({
  baseURL: 'http://130.185.76.61:8000/auth', // todo move this to env file
  timeout: 4000,
  // headers: {'X-Custom-Header': 'foobar'}
});

const useStyles = makeStyles(styles);

export default function LoginPage(props) {
  const [cardAnimaton, setCardAnimation] = useState("cardHidden");
  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();
  const { ...rest } = props;
  const {access_token='', name='', playerId='',} = Cookies.getJSON('player_auth') || {};

  // const handleClick = (e) => {
  //   if (!name || !playerId) {
  //     e.preventDefault();
  //     alert('name and email is required.');
  //     console.log(name, playerId);
  //   } else {
  //     console.log(playerId, name);
  //   }
  // };
  const responseGoogle = (response) => { // todo enlonge expire time of cookies.
    const { accessToken, profileObj } = response;
    axios_rest_auth.post('/convert-token', { //todo save data of user in server specially nickname of user. email will be saved automatically but ... check you can use python auth for this. 
      grant_type:'convert_token',
      client_id:'FwmaL0eqaUugozoaALfPvMda1YgJXpM4cp4WzBZg',
      client_secret:'bhqe4xcFNtTJ6eQEE9DkmcJwXCfA5JDWRLp25XdveinBRlVVi5QaJz1QYNjdnr4nSQJYZuEGIUzP8jA73rKi7GS2FgXFBXBQgQ5Ct9rpnwJZLYVK5yGdPEQ6dXRBpo2Q',
      backend:'google-oauth2',
      token:accessToken,
    })
    .then( (response) => { //todo implement logout and handle errors of login.
      console.log(response);
      const {data} = response;
      Cookies.set('player_auth', { ...profileObj , ...data, playerId:profileObj.email });
    })
    .catch( (error) => {
      console.log(error);
    });
    console.log(response);
  }

  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="Esm Famil"
        rightLinks={<HeaderLinks />}
        {...rest}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Login</h4>
                    <h4>player id: {playerId}</h4>
                    <h4>name: {name}</h4>
                    <div className={classes.socialLine}>
                      <Button
                        justIcon
                        href="#pablo"
                        target="_blank"
                        color="transparent"
                        onClick={e => e.preventDefault()}
                      >
                        <i className={"fab fa-twitter"} />
                      </Button>
                      <Button
                        justIcon
                        href="#pablo"
                        target="_blank"
                        color="transparent"
                        onClick={e => e.preventDefault()}
                      >
                        <i className={"fab fa-facebook"} />
                      </Button>
                      {/* <Button
                        justIcon
                        href="#pablo"
                        target="_blank"
                        color="transparent"
                        children={                        
                            <GoogleLogin
                            clientId="889674341411-e60g60ostlq4h0d86aqkokodn3evl5q2.apps.googleusercontent.com"
                            buttonText=""
                            onSuccess={responseGoogle}
                            onFailure={responseGoogle}
                            cookiePolicy={'single_host_origin'}
                            className="fab fa-google-plus-g"
                            />
                          }
                        onClick={e => e.preventDefault()}
                      >
                      </Button> */}
                      <GoogleLogin
                        clientId="889674341411-e60g60ostlq4h0d86aqkokodn3evl5q2.apps.googleusercontent.com"
                        buttonText=""
                        onSuccess={responseGoogle}
                        onFailure={responseGoogle}
                        cookiePolicy={'single_host_origin'}
                        className=''                            
                      />
                    </div>
                  </CardHeader>
                  <CardFooter className={classes.cardFooter}>
                    <Link to={'/create-new-game'}>
                      <Button simple color="primary" size="lg">
                        Play Game
                      </Button>
                    </Link>
                  </CardFooter>
                  <LinksToJoinGames/>
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

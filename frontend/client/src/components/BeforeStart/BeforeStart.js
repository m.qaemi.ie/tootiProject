import React, { useState } from "react";
import { Link } from 'react-router-dom';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";
import Small from "../MaterialKitComponents/Small.js";
import SimpleLists from "../MaterialUiComponents/SimpleLists";

import styles from "../assets/jss/views/loginPage.js";

import image from "../assets/img/bg7.jpg";

const useStyles = makeStyles(styles);

export default function BeforeStart({ state, sendMessage, ...rest }) {
  let { name, playerId, gameId, gameName, creator, players, playersData, step, action, roundNumber, startNumber, activeGames,} = state;
  const [cardAnimaton, setCardAnimation] = useState("cardHidden");
  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();

  const handleSubmit = (event) => {
    event.preventDefault();
    let message = { playerId, gameId, roundNumber:1, step:'beforeStart' ,action: 'startRound', };
    sendMessage('sendUpdate', message);
  };

  return (
    <div>
      <h4>creator of game:{creator}</h4>
      <Header
        absolute
        color="transparent"
        brand="Esm Famil"
        rightLinks={<HeaderLinks />}
        {...rest}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Prepare To Start</h4>
                    {/* <Small>Use {'"Small"'} tag for the headers</Small> */}
                  </CardHeader>
                  {/* <p className={classes.divider}>Make Game</p> */}
                  <CardBody>
                  <SimpleLists items={players.map((player)=>playersData[player].playerName)}/>
                    <p className={classes.divider}>Game Name: {gameName}</p>
                    <p className={classes.divider}>Game Id: {gameId}</p>
                    <p className={classes.divider}>Give link of game to others to join game.</p>
                    <p className={classes.divider}>only creator of game can start game.</p>
                  </CardBody>
                  <CardFooter className={classes.cardFooter}>
                    {creator===playerId?<Button simple color="primary" size="lg" onClick={handleSubmit}>
                        Start Game
                    </Button>:null}
                  </CardFooter>
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

from django.apps import AppConfig


class DataminingsConfig(AppConfig):
    name = 'dataMinings'

import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";

import styles from "../assets/jss/views/loginPage.js";

import image from "../assets/img/bg7.jpg";

const useStyles = makeStyles(styles);

export default function Form({state, sendMessage, ...rest }) {
  let { name, playerId, gameId, gameName, step, action, roundNumber, startNumber, rounds, } = state;
  const [cardAnimaton, setCardAnimation] = useState("cardHidden");
  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();

  const [isActive, setIsActive] = useState(true);
  const [inputs, setInputs] = useState({});
  const [inputDisabled, setInputDisabled] = useState(false);
  const formFields = rounds[roundNumber].playersForms[playerId].form;

  const handleInputChange = (event) => {
    event.persist();
    setInputs(inputs => ({ ...inputs, [event.target.name]: event.target.value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setInputDisabled(inputDisabled => true)
    let message = { playerId, gameId, roundNumber, step, action: 'stopRound', }
    sendMessage('sendUpdate', message);
  };

  const submitInputs = () => {
    // todo implement a mechanism for resent forms if not recieved from server. but this tries repeat until sometime.
    setInputDisabled(inputDisabled => true)
    let form_completed = inputs;
    let formFieldsCompleted = formFields;
    for (var key of Object.keys(formFieldsCompleted)) {
      // console.log(key + " -> " + p[key])
      if(form_completed[key]){
        formFieldsCompleted[key].w = form_completed[key];
      }
    }
    console.log(form_completed);
    console.log(formFieldsCompleted);
    let rounds_with_completed_form = {};
    rounds_with_completed_form[roundNumber] = {};
    rounds_with_completed_form[roundNumber].playersForms = {};
    rounds_with_completed_form[roundNumber].playersForms[playerId] = {};
    rounds_with_completed_form[roundNumber].playersForms[playerId].form = formFieldsCompleted;
    let message = { playerId, gameId, roundNumber, step, action: 'receiveForm', rounds: rounds_with_completed_form, }
    sendMessage('sendUpdate', message);
  };

  useEffect(() => {
    if (action === 'submitForm') {
      submitInputs();
    }  
  }, [action]);

  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="Esm Famil"
        rightLinks={<HeaderLinks />}
        {...rest}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Fill Form</h4>
                  </CardHeader>
                  <CardBody>
                    {
                      Object.keys(formFields).map((field) =>
                        <CustomInput
                          key={field}
                          labelText={`${formFields[field].f} با ${formFields[field].l}`}
                          id={formFields[field].f}
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            placeholder: `${formFields[field].f} با ${formFields[field].l}`,
                            onChange:handleInputChange,
                            name:formFields[field].f,
                            type: "text",
                            disabled : (inputDisabled)? "disabled" : "",
                            endAdornment: (
                              <InputAdornment position="end">
                                <People className={classes.inputIconsColor} />
                              </InputAdornment>
                            )
                          }}
                        />
                      )
                    }
                  </CardBody>
                  <p className={classes.divider}>First user sends Form, the form for this user and all other users will be disabled.</p>
                  <CardFooter className={classes.cardFooter}>
                    <Button simple color="primary" size="lg" onClick={handleSubmit}>
                        Send Form
                    </Button>
                  </CardFooter>
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

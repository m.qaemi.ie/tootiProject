import React, { useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';


const GreenRadio = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

export default function RadioButtons({setInputsValues, name, radioOptions, question }) {
  const [selectedValue, setSelectedValue] = React.useState(radioOptions[0].value);

  const handleChange = (event) => {
    // event.preventDefault();
    event.persist() //todo check
    setSelectedValue(event.target.value);
    setInputsValues(inputsValues => ({ ...inputsValues, [event.target.name]: event.target.value }));
  };
  useEffect(()=>{
    setInputsValues(inputsValues => ({ ...inputsValues, [name]: radioOptions[0].value }));
  },[]);

  const radioComponents = radioOptions.map((radioOption, index) => {
    return (<FormControlLabel
    value={radioOption.value}
    control={<Radio color="primary" checked={selectedValue === radioOption.value} onChange={handleChange} name={name} inputProps={{ 'aria-label': 'F' }} size="small"
/>}
    label={radioOption.label}
    labelPlacement="top"
    key={index}
    />);
  });

  return (
    <div>
    <FormControl component="fieldset">
    {/* <FormLabel component="legend">{question}</FormLabel> */}
    <RadioGroup row aria-label="position" name="position" defaultValue="top">
      {radioComponents}
    </RadioGroup>
  </FormControl>
  </div>
    // <div>
    //   <h3>{name}</h3>
    //   <label style={{verticalAlign: "bottom"}}>
    //     very low
    //   <Radio
    //     checked={selectedValue === '1'}
    //     onChange={handleChange}
    //     value={1}
    //     name={name}
    //     inputProps={{ 'aria-label': 'A' }}
    //     // label='very low'
    //     // labelPlacement='start'
    //   />
    //   </label>
    //   <Radio
    //     checked={selectedValue === '2'}
    //     onChange={handleChange}
    //     value={2}
    //     name={name}
    //     inputProps={{ 'aria-label': 'B' }}
    //   />
    //   {/* <GreenRadio
    //     checked={selectedValue === 'c'}
    //     onChange={handleChange}
    //     value="c"
    //     name={name}
    //     inputProps={{ 'aria-label': 'C' }}
    //   /> */}
    //   <Radio
    //     checked={selectedValue === '3'}
    //     onChange={handleChange}
    //     value={3}
    //     color="default"
    //     name={name}
    //     inputProps={{ 'aria-label': 'D' }}
    //   />
    //   <Radio
    //     checked={selectedValue === '4'}
    //     onChange={handleChange}
    //     value={4}
    //     color="default"
    //     name={name}
    //     inputProps={{ 'aria-label': 'E' }}
    //     // size="small"
    //   />
    //   <label style={{verticalAlign: "bottom"}}>
    //   <Radio
    //     checked={selectedValue === '5'}
    //     onChange={handleChange}
    //     value={5}
    //     color="default"
    //     name={name}
    //     inputProps={{ 'aria-label': 'F' }}
    //     // label='very important'
    //     // labelPlacement='end'
    //     // size="small"
    //   />
    //   very high
    //   </label>
    // </div>
  );
}

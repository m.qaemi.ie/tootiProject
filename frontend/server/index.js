const http = require('http');
const express = require('express');
const cors = require('cors');
const {
    prepare_dealer_socket,
    prepare_subscriber_socket,
    send_message_to_python_server,} = require('./socket_modules/cloneclient5');

const app = express();
const server = http.createServer(app);

const socketio = require('socket.io');
const io = socketio(server, 
    {
    path: '/',
    serveClient: false,
    // below are engine.IO options
    pingInterval: 15 * 60 * 1000, // todo you must disconnect users manually server side and client side.
    pingTimeout: 5000,
    // cookie: false,
  }
  );
io.set('transports', ['websocket']);
const redis = require('socket.io-redis');
// var Redis = require("ioredis");
// var redis = new Redis();
// io.adapter(redis);
io.adapter(redis({  // todo wrap touch of reis in try catch... . or async?
    host: process.env.IP_ADDRESS_OF_REDIS_SERVER,
    port: process.env.PORT_OF_REDIS_SERVER,
    requestsTimeout: 10000,
 }));
const { KVMsg, kvmap } = require('./socket_modules/kvmsg');

// var io = require('socket.io-emitter')(redis);

// var sequence = 0;
var ip_address = process.env.IP_ADDRESS_OF_SERVER;
console.log('ip address of python server:', ip_address);

app.use(cors());

app.use(express.static('public'));

// prepare sockets with python server
var snapshot = prepare_dealer_socket(ip_address, process.env.SNAPSHOT_PORT);
var updater = prepare_dealer_socket(ip_address, process.env.UPDATER_PORT);
var subscriber = prepare_subscriber_socket(ip_address, process.env.SUBSCRIBER_PORT, '');

// listen to snapshots from python server and send them to client
snapshot.on('message', function() {  //todo try catch
    let mergedSnapshots = {};
    let snapshots_kvmsgs = [];
    let msg = Array.apply(null, arguments);
    let kvmsg = KVMsg.from_msg(msg);
    if (kvmsg.key === "NOSUBTREE"){
        console.log(kvmsg.body);
        playerId = (JSON.parse(kvmsg.body)).playerId;
        io.of('/').to(playerId).emit('snapshot', JSON.stringify(kvmsg)); // todo msg or kvmsg.
    } else if(kvmsg.key === "KTHXBAI"){
        sequence = kvmsg.sequence; // todo do we need this line?
        console.log(`player id:${player_id}, Received snapshot=${sequence} key=${kvmsg.key} body=${kvmsg.body}`);
        console.log('thanks buy.' , kvmsg);
        console.log('thanks buy.' , snapshots_kvmsgs);
        snapshots_kvmsgs.sort((a, b) => (a.sequence < b.sequence) ? 1 : -1);
        // mergedSnapshots = numbers.reduce((total, n) => total + n, 0);
        mergedSnapshots = merge(mergedSnapshots, snapshots_kvmsgs);
        // break;
        // flag = false;
        client_socket.emit('snapshot', JSON.stringify(mergedSnapshots[0]));
        // client_socket.emit('snapshot', JSON.stringify(kvmsg));
    } else {
        snapshots_kvmsgs.push(kvmsg);
    }

    kvmsg.store(kvmap);  // todo do we require this?
});

// listen to snapshots from python server and send them to client
updater.on('message', function() {  //todo try catch
    let mergedSnapshots = {};
    let snapshots_kvmsgs = [];
    let msg = Array.apply(null, arguments);
    let kvmsg = KVMsg.from_msg(msg);
    if (kvmsg.key === "NOSUBTREE"){
        console.log(kvmsg.body);
        playerId = (JSON.parse(kvmsg.body)).playerId;
        io.of('/').to(playerId).emit('snapshot', JSON.stringify(kvmsg)); // todo msg or kvmsg.
    } else if(kvmsg.key === "KTHXBAI"){
        sequence = kvmsg.sequence; // todo do we need this line?
        console.log(`player id:${player_id}, Received snapshot=${sequence} key=${kvmsg.key} body=${kvmsg.body}`);
        console.log('thanks buy.' , kvmsg);
        console.log('thanks buy.' , snapshots_kvmsgs);
        snapshots_kvmsgs.sort((a, b) => (a.sequence < b.sequence) ? 1 : -1);
        // mergedSnapshots = numbers.reduce((total, n) => total + n, 0);
        mergedSnapshots = merge(mergedSnapshots, snapshots_kvmsgs);
        // break;
        // flag = false;
        client_socket.emit('snapshot', JSON.stringify(mergedSnapshots[0]));
        // client_socket.emit('snapshot', JSON.stringify(kvmsg));
    } else {
        snapshots_kvmsgs.push(kvmsg);
    }

    kvmsg.store(kvmap);  // todo do we require this?
});

// listen to updates published from python server and publish them.
subscriber.on("message", function() {
    io.of('/').adapter.allRooms((err, rooms) => {
        console.log('all rooms:', rooms); // an array containing all rooms (accross every node)
      });

      io.of('/').adapter.clients((err, clients) => {
        console.log('all connected clients:', clients) // an array containing all connected socket ids
      });
    let msg = Array.apply(null, arguments);
    kvmsg = KVMsg.from_msg(msg);
    console.log(kvmsg);
    sequence = kvmsg.sequence;
    kvmsg.store(kvmap); //todo is this necessary?
    let action;
    if(!kvmsg.body || kvmsg.body===''){
        action = 'delete';
    }else{
        action = 'update';
    }
    game_id = kvmsg.key.substring(0, 8); // todo make it reliable, now when we change length of uuid it will not be changed automatically.
    console.log(game_id, 'this is game id.', typeof(game_id));
    try{
        io.of('/').to(game_id).emit('update', JSON.stringify(kvmsg));
    }catch (e) {
        console.log(e)
    }
});


io.of('/').on('connect', (socket) => {
    io.of('/').adapter.allRooms((err, rooms) => {
        console.log('all rooms:', rooms); // an array containing all rooms (accross every node)
      });

      io.of('/').adapter.clients((err, clients) => {
        console.log('all connected clients:', clients) // an array containing all connected socket ids
      });

    socket.on('addMeToRoom', ({name, gameId, playerId, message}, callback) => {
        try{
            if (gameId) { // todo is this if else necessary?
                // socket.join(gameId);
                // Makes the socket with the given id join the room. The callback will be called once the socket has joined the room, or with an err argument if the socket was not found.
                io.of('/').adapter.remoteJoin(socket.id, gameId, (err) => {
                    if (err) { console.log(err);/* unknown id */ }
                    // success
                  });
                io.of('/').adapter.remoteJoin(socket.id, playerId, (err) => {
                    if (err) { console.log(err);/* unknown id */ }
                    // success
                  });
                io.of('/').adapter.clients([gameId, playerId], (err, clients) => {
                    console.log('clients of 2 rooms', clients) // an array containing socket ids in 'room1' and/or 'room2'
                });
                io.in(gameId).clients((err, clients) => {
                    console.log('clients of room', clients) // an array containing socket ids in 'room3'
                  });
                  io.of('/').adapter.clientRooms(socket.id, (err, rooms) => {
                    if (err) { /* unknown id */ }
                    console.log('rooms',playerId, rooms); // an array containing every room a given id has joined.
                  });
                  
                  // send_message_to_python_server(snapshot, gameId, playerId, message,);
            } else if (!gameId) {
                // if user not joined to a game yet and messages should be sent to him individually, we make for him
                // a room with his playerId to send messages for him privately. if he connect with multi device with his playerId all of them
                // saved in this room and send messages for all of them.
                // todo garbage collection of this sockets needed?
                // socket.join(playerId);
                io.of('/').adapter.remoteJoin(socket.id, playerId, (err) => {
                    if (err) { console.log(err);/* unknown id */ }
                    // success
                  });
                // send_message_to_python_server(snapshot, gameId, playerId, message,);
            } else {
                console.log('name or playerId not sent.');
            }
        } catch (e) {
            console.log(e); //todo
        }
        callback();
    });

    // request snapshot from python server and add user to a game(room) if not added yet.
    socket.on('sendSnapshot', ({name, gameId, playerId, message}, callback) => {
        try{
            if (gameId) { // todo is this if else necessary?
                // socket.join(gameId);
                // Makes the socket with the given id join the room. The callback will be called once the socket has joined the room, or with an err argument if the socket was not found.
                io.of('/').adapter.remoteJoin(socket.id, gameId, (err) => {
                    if (err) { console.log(err);/* unknown id */ }
                    // success
                  });
                io.of('/').adapter.remoteJoin(socket.id, playerId, (err) => {
                    if (err) { console.log(err);/* unknown id */ }
                    // success
                  });
                io.of('/').adapter.clients([gameId, playerId], (err, clients) => {
                    console.log('clients of 2 rooms', clients) // an array containing socket ids in 'room1' and/or 'room2'
                });
                io.in(gameId).clients((err, clients) => {
                    console.log('clients of room', clients) // an array containing socket ids in 'room3'
                  });
                  io.of('/').adapter.clientRooms(socket.id, (err, rooms) => {
                    if (err) { /* unknown id */ }
                    console.log('rooms',playerId, rooms); // an array containing every room a given id has joined.
                  });


                send_message_to_python_server(snapshot, gameId, playerId, message,);
            } else if (!gameId) {
                // if user not joined to a game yet and messages should be sent to him individually, we make for him
                // a room with his playerId to send messages for him privately. if he connect with multi device with his playerId all of them
                // saved in this room and send messages for all of them.
                // todo garbage collection of this sockets needed?
                // socket.join(playerId);
                io.of('/').adapter.remoteJoin(socket.id, playerId, (err) => {
                    if (err) { console.log(err);/* unknown id */ }
                    // success
                  });
                send_message_to_python_server(snapshot, gameId, playerId, message,);
            } else {
                console.log('name or playerId not sent.');
            }
        } catch (e) {
            console.log(e); //todo
        }
        callback();
    });

    // listens to clients and sends their updates to python server.
    socket.on('sendUpdate', ({name, gameId, playerId, message}) => {
        console.log('from socket.on sendUpdate:',name, gameId, playerId, message);
        try{
            if (name && playerId && gameId) { // todo is this if else necessary?
                // socket.join(gameId);
                io.of('/').adapter.remoteJoin(socket.id, gameId, (err) => {
                    if (err) { console.log(err);/* unknown id */ }
                    // success
                  });
                io.of('/').adapter.remoteJoin(socket.id, playerId, (err) => {
                    if (err) { console.log(err);/* unknown id */ }
                    // success
                });
                io.of('/').adapter.clients([gameId, playerId], (err, clients) => {
                    console.log('clients of 2 rooms', clients) // an array containing socket ids in 'room1' and/or 'room2'
                });
                io.in(gameId).clients((err, clients) => {
                    console.log('clients of room', clients) // an array containing socket ids in 'room3'
                  });
                io.of('/').adapter.clientRooms(socket.id, (err, rooms) => {
                    if (err) { /* unknown id */ }
                    console.log('rooms', playerId, rooms); // an array containing every room a given id has joined.
                });

                send_message_to_python_server(updater, gameId, playerId, message);
            } else {
                console.log('error: name or playerId or gameId not sent.')
            }
        } catch (e) {
            console.log(e); //todo
        }
        // callback();
    });

    socket.on('disconnect', (reason) => { //TODO implement adapter and remove removeUser.
        console.log(`node socket disconnectd.${socket.id}`, reason);
        // const user = removeUser(socket.id);
        // console.log(gameId, playerId);
        console.log('socket disconnected?', socket.disconnected);
        socket.removeAllListeners('send message'); //todo
        socket.removeAllListeners('disconnect'); // todo
        io.removeAllListeners('connection'); // todo

        /*
        io.of('/').adapter.remoteDisconnect(socket.id, true, (err) => {
            if (err) { console.log(err); } // todo throw unknown id
            // success
            console.log(socket.id, 'has successfully remoteDisconnected with redis.');
          });
        // Returns the list of rooms the client with the given ID has joined (even on another node).
        io.of('/').adapter.clientRooms(socket.id, (err, rooms) => {
            if (err) { console.log(err);} // todo throw unknown id
            console.log(rooms, 'reached successfully.'); // an array containing every room a given id has joined.
            rooms.map( room => {
                // Makes the socket with the given id leave the room. The callback will be called once the socket has left the room, or with an err argument if the socket was not found.
                io.of('/').adapter.remoteLeave(socket.id, room, (err) => {
                    if (err) { console.log(err); } // todo throw unknown id 
                    // success
                    console.log(socket.id, 'from', room, 'removed');
                  });
            });
          });
        */
        // if(user) { // todo
        //     io.to(user.gameId).emit('message', { user: 'Admin', text: `${user.name} has left.` });
        //     // io.to(user.gameId).emit('roomData', { gameId: user.gameId, users: getUsersInRoom(user.gameId)});
        //     io.to(user.gameId).emit('roomData', { gameId: user.gameId, users: getUsersInGame(user.gameId)});
        //     socket.leave(user.gameId);
        // }
    });
});

console.log(process.env.PORT);
server.listen(process.env.PORT || 5000, process.env.HOST, () => console.log(`Server has started.`));

from django.apps import AppConfig


class SocketpreparationConfig(AppConfig):
    name = 'socketPreparation'

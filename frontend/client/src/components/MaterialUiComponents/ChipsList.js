import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import CommentIcon from '@material-ui/icons/Comment';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

export default function ChipsList({selectedItems, setSelectedItems, items, minOfSelectedItems, maxOfSelectedItems}) {
  const classes = useStyles();

  const handleToggle = (value) => () => {
    const currentIndex = selectedItems.indexOf(value);
    const newSelcteds = [...selectedItems];
    if (currentIndex === -1) {
      if (selectedItems.length < maxOfSelectedItems) {
        newSelcteds.push(value);
      } else {
        alert(`number of selected categories must be between ${minOfSelectedItems} and ${maxOfSelectedItems}`);
        return;
      }
    } else {
      if (selectedItems.length > minOfSelectedItems) {
        newSelcteds.splice(currentIndex, 1);
      } else {
        alert(`number of selected categories must be between ${minOfSelectedItems} and ${maxOfSelectedItems}`);
        return;
      }
    }
    setSelectedItems(selectedItems => newSelcteds);
  };
  console.log(selectedItems);

  return (
    <div className={classes.root}>
      {items.map((category) => {
          const color = selectedItems.indexOf(category) === -1 ? "" : "primary" ;
          return  <Chip clickable color={color} label={category} id={category} onClick={handleToggle(category)} />
        }
        )
      }
    </div>
  );
};

from django.urls import path
from creators.views import GameList, DeviceInstalledAppList, RoundList, PlayerFillsFormList, ValidIdList

urlpatterns = [
    path('validId', ValidIdList.as_view(), name='valid-id'),
    path('users/install', DeviceInstalledAppList.as_view(), name='install-app'),
    path('games', GameList.as_view(), name='create-game-with-rounds'),

    path('games/<uuid:game_id>/rounds', PlayerFillsFormList.as_view(), name='player-fills-forms'),
    # path('users/install', DeviceUserList.as_view(), name='install-app'),
    path('rounds', RoundList.as_view(), name='save-round-of-game'),
]


package com.testapplication.androidadvanced.recyclerView;

/**
 * Created by A.mehdizadeh on 3/14/2020.
 * mehdizadeh7250@gmail.com
 */
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;

import io.reactivex.Observable;
import io.reactivex.processors.PublishProcessor;

public class conecil5 {
        //  This client is identical to clonecli3 except for where we
        //  handles subtrees.
    Observable<String> observable =null;
        private final static String SUBTREE = "/client/";
        PublishProcessor<Long> publishProcessor = PublishProcessor.create();
        public Observable<Long> run()
        {
            try (ZContext ctx = new ZContext()) {
                Socket snapshot = ctx.createSocket(SocketType.DEALER);
                snapshot.connect("tcp://130.185.76.61:5556");

                Socket subscriber = ctx.createSocket(SocketType.SUB);
                subscriber.connect("tcp://130.185.76.61:5558");
                subscriber.subscribe(SUBTREE.getBytes(ZMQ.CHARSET));

                Socket publisher = ctx.createSocket(SocketType.DEALER);
                publisher.connect("tcp://130.185.76.61:5557");

                Map<String, kvmsg> kvMap = new HashMap<String, kvmsg>();

                // get state snapshot

//                snapshot.sendMore("ICANHAZ?");
//                snapshot.send(SUBTREE);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("gameId","");
                jsonObject.addProperty("playerId","m.qame@sample.com");
                jsonObject.addProperty("roundNumber","");
                jsonObject.addProperty("step","gameNotAvailable");
                jsonObject.addProperty("action","giveActiveGames");
                JsonObject jsonObject1 = new JsonObject();
                jsonObject1.add("state_json",jsonObject);
                long sequence = 0;
                kvmsg kvMsg = new kvmsg(0);
                kvMsg.fmtKey("%s%d", SUBTREE, 10000);
                kvMsg.fmtBody("%s",jsonObject1.toString());
                kvMsg.setProp("ttl", "%d",30);
                kvMsg.send(snapshot);
//                kvMsg.destroy();
//                snapshot.send(kvmsg)
                while (true) {
                    kvmsg kvMsg1 = kvmsg.recv(snapshot);
                    if (kvMsg1 == null)
                        break; //  Interrupted

                    sequence = kvMsg1.getSequence();
                    if ("KTHXBAI".equalsIgnoreCase(kvMsg1.getKey())) {
//                        System.out.println(
//                                "Received snapshot = " + kvMsg.getSequence()
//                        );
//                        publishProcessor.onNext(kvMsg.getSequence());
                        kvMsg1.destroy();
//                        break; // done
                        return Observable.just(kvMsg1.getSequence());
                    }

                    System.out.println("receiving " + kvMsg1.getSequence());
                    kvMsg1.store(kvMap);
                }

//                Poller poller = ctx.createPoller(1);
//                poller.register(subscriber);

//                Random random = new Random();

                // now apply pending updates, discard out-of-getSequence messages
//                long alarm = System.currentTimeMillis() + 5000;
//                while (true) {
//                    int rc = poller.poll(
//                            Math.max(0, alarm - System.currentTimeMillis())
//                    );
//                    if (rc == -1)
//                        break; //  Context has been shut down
//
//                    if (poller.pollin(0)) {
//                        kvmsg kvMsg = kvmsg.recv(subscriber);
//                        if (kvMsg == null)
//                            break; //  Interrupted
//
//                        if (kvMsg.getSequence() > sequence) {
//                            sequence = kvMsg.getSequence();
//                            System.out.println("receiving " + sequence);
//                            kvMsg.store(kvMap);
//                        }
//                        else kvMsg.destroy();
//                    }

//                    if (System.currentTimeMillis() >= alarm) {

//                        alarm = System.currentTimeMillis() + 1000;
//                    }
//                }
            }
            return null;
        }
}

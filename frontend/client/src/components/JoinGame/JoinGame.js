import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
import CounterDown from '../CounterDown/CounterDown';
import './JoinGame.css';

// eslint-disable-next-line react/prop-types
const JoinGame = ({ state }) => {
  let {startNumber} = state; 
  // const [name, setName] = useState('');
  // const [room, setRoom] = useState('');

  return (
    <div className="joinOuterContainer">
      <div className="joinInnerContainer">
        <h1 className="heading">Wait until other users join game.</h1>
        <CounterDown startNumber={startNumber} />
      {/*  <div>*/}
      {/*    <input placeholder="Name" className="joinInput" type="text" onChange={(event) => setName(event.target.value)} />*/}
      {/*  </div>*/}
      {/*  <div>*/}
      {/*    <input placeholder="Room" className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)} />*/}
      {/*  </div>*/}
      {/*  <Link onClick={(e) => ((!name || !room) ? e.preventDefault() : null)} to={`/chat?name=${name}&room=${room}`}>*/}
      {/*    <button className="button mt-20" type="submit">Sign In</button>*/}
      {/*  </Link>*/}
      </div>
    </div>
  );
};

export default JoinGame;

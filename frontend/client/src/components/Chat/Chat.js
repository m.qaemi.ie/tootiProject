import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';

// import TextContainer from '../TextContainer/TextContainer';
// import Messages from '../Messages/Messages';
// import InfoBar from '../InfoBar/InfoBar';
// import Input from '../Input/Input';

import './Chat.css';

let socket;

// eslint-disable-next-line react/prop-types
const Chat = ({ location }) => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const [users, setUsers] = useState('');
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const ENDPOINT = 'https://project-chat-application.herokuapp.com/';

  useEffect(() => {
    // eslint-disable-next-line no-shadow
    const { name, room } = queryString.parse(location.search);

    // socket = io(ENDPOINT);
    socket = io();

    setRoom(room);
    setName(name);

    // socket.emit('join', { name, room }, (error) => {
    //   if (error) {
    //     // eslint-disable-next-line no-alert
    //     alert(error);
    //   }
    // });
    // eslint-disable-next-line react/prop-types
  }, [ENDPOINT, location.search]);

  useEffect(() => {
    // eslint-disable-next-line no-shadow
    socket.on('message', (message) => {
      setMessages([...messages, message]);
    });

    // eslint-disable-next-line no-shadow
    socket.on('roomData', ({ users }) => {
      setUsers(users);
    });

    return () => {
      socket.emit('disconnect');

      socket.off();
    };
  }, [messages]);

  const sendMessage = (event) => {
    event.preventDefault();

    if (message) {
      socket.emit('sendMessage', message, () => setMessage(''));
    }
  };

  return (
    <div className="outerContainer">
      <div className="container">
        {/*<InfoBar room={room} />*/}
        {/*<Messages messages={messages} name={name} />*/}
        <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
      </div>
      {/*<TextContainer users={users} />*/}
    </div>
  );
};

export default Chat;

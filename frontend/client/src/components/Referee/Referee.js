import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";
import CheckboxList from '../MaterialUiComponents/ChecboxList';
import styles from "../assets/jss/views/loginPage.js";
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

// import List from '@material-ui/core/List';
// import ListItem from '@material-ui/core/ListItem';
// import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
// import ListItemText from '@material-ui/core/ListItemText';
// import ListItemAvatar from '@material-ui/core/ListItemAvatar';
// import Checkbox from '@material-ui/core/Checkbox';
// import Avatar from '@material-ui/core/Avatar';
// import CheckboxListSecondary from '../MaterialUiComponents/chec';
import MaterialTable from "material-table";

import image from "../assets/img/bg7.jpg";
// import { Category } from "@material-ui/icons";

styles.imageIcon = {
  height: '100%'
}
styles.iconRoot = {
  textAlign: 'center'
}
// styles.icon = {
//   backgroundImage: "url('/icons8-delete-48 (1).png')",
//   backgroundSize: 'inherit',
//   // textAlign: 'center'
// }

const useStyles = makeStyles(styles);

export default function Referee({ state, sendMessage, changeStep, ...rest }) {
  let { name, playerId, gameId, gameName, step, action, roundNumber, rounds, playerRefereed, roundData, selectedCategories, resultsConfirmed,} = state;
  const [cardAnimaton, setCardAnimation] = useState("cardHidden");
  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();

  console.log(roundData);
  const [playersParticipatedInRound, setPlayersParticipatedInRound] = useState(roundData.playersParticipatedInRound);
  // const [resultsConfirmed, setResultsConfirmed] = useState(false);
  // const playersParticipatedInRound = JSON.parse(roundData.playersParticipatedInRound);
  // const playersForms = {};
  // playersParticipatedInRound.map((playerId)=>{
  //   Object.assign(playersForms, {[playerId]: JSON.parse(roundData[playerId])});
  // });

  const [isActive, setIsActive] = useState(true);  
  // const [inputs, setInputs] = useState(formFields);
  const [inputDisabled, setInputDisabled] = useState(false);
  // const [submitForm, setSubmitForm] = useState(false);
  const [checked, setChecked] = useState([]);

  const handleClick = (event, rowData) => {
    event.preventDefault();
    console.log(rowData);
    console.log(event);
    rowData.referees[playerId] = false;
  };
  const handleConfirmResults = (event) => {
    event.preventDefault();
    let message = { playerId, gameId, roundNumber, step, action: 'confirmResults', }
    sendMessage('sendUpdate', message);
    // setResultsConfirmed(true);
  };
/*
  const handleSubmit = (event) => {
    event.preventDefault();
    setInputDisabled(inputDisabled => true)
    let inputs = formFields;
    checked.map((field=>inputs[field].c=true));
    // setInputs(inputs => checked.map((field=>inputs)))
    let rounds_with_completed_form = {};
    rounds_with_completed_form[roundNumber] = {};
    rounds_with_completed_form[roundNumber].playersForms = {};
    rounds_with_completed_form[roundNumber].playersForms[playerRefereed] = {};
    rounds_with_completed_form[roundNumber].playersForms[playerRefereed].form = inputs;
    console.log(rounds_with_completed_form);
    let message = { playerId, gameId, roundNumber, step, action: 'receiveReferee', rounds: rounds_with_completed_form, playerRefereed }
    sendMessage('sendUpdate', message);
    roundsRefereed? changeStep('roundResult'): changeStep('waitUntilReferee');
  };
*/
  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="Esm Famil"
        rightLinks={<HeaderLinks />}
        {...rest}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Referee Form</h4>
                  </CardHeader>
                  <CardBody>
                    {/* <CheckboxList checked={checked} setChecked={setChecked} formFields={formFields} disabledCheckbox={inputDisabled}/> */}
                    {selectedCategories.map(category =>
                      <div style={{ maxWidth: "100%" }}>
                        <MaterialTable
                          id={category}
                          columns={[
                            { title: "", field: "player" },
                            { title: "", field: "word" },
                            // { title: "", field: "birthYear", type: "numeric" },
                            // {
                            //   title: "",
                            //   field: "correctness",
                            //   // lookup: { 34: "İstanbul", 63: "Şanlıurfa" },
                            // },
                            {
                              field: 'url',
                              title: 'checkbox',
                              render: rowData =>
                                // <ListItemIcon key={field.f} role={undefined} dense button onClick={field.w?handleToggle(field.f):null}>
                                  <Checkbox
                                    id={category}
                                    name={rowData.player}
                                    onClick={(event) =>
                                      {
                                        // event.preventDefault();
                                        console.log(rowData);
                                        console.log(event);
                                        let message = { playerId, gameId, roundNumber, step , action: 'receiveReferee', playerRefereed: rowData.player, category, judgmentAboutWord:!rowData.referees[playerId]};
                                        sendMessage('sendUpdate', message);                                    
                                        // rowData.referees[playerId] = false;
                                      }
                                    }
                                    // onClick={(event, rowData) => handleClick(event, rowData)}
                                    className={classes.icon}
                                    disabled={rowData.word === 'نزده'}
                                    edge="start"
                                    checked={rowData.referees[playerId] === true?true:false}
                                    tabIndex={-1}
                                    disableRipple
                                    // color="secondary"
                                    inputProps={{ 
                                        // 'aria-labelledby': labelId,
                                        // 'aria-label': 'disabled checked checkbox',
                                        // 'aria-label': 'secondary checkbox',
                                    }}
                                  />
                                // </ListItemIcon>
                                //<img src={rowData.url} style={{width: 50, borderRadius: '50%'}}/>
                            },
                            {
                              field: 'url',
                              title: 'judjments',
                              render: rowData =>
                                <ListItemIcon>
                                  {playersParticipatedInRound.map((player) =>
                                    rowData.referees[player] === true?
                                    (<Tooltip title={player} placement="top-start">
                                      <Icon classes={{root: classes.iconRoot}}>
                                        <img className={classes.imageIcon} src="/icons8-approval-48.png"/>
                                      </Icon>
                                    </Tooltip>):
                                    <Tooltip title={player} placement="top-start">
                                      <Icon classes={{root: classes.iconRoot}}>
                                        <img className={classes.imageIcon} src="/icons8-delete-48 (1).png"/>
                                      </Icon>
                                    </Tooltip>
                                  )}
                                      {/* <SvgIcon >
                                          <textPath d="/icons8-approval-48.png" />
                                      </SvgIcon> */}
                                  </ListItemIcon>
                                  //<img src={rowData.url} style={{width: 50, borderRadius: '50%'}}/>
                              }
                          ]}
                          data={playersParticipatedInRound.map((player)=> {
                            let word = roundData[player][category].word;
                            return {
                              player,
                              word:word?word:'نزده',
                              referees: roundData[player][category],
                            };
                          })}
                          // data={[
                          //   {
                          //     playerId: "rkjwekr",
                          //     word: "Baran",
                          //     correctness: true,
                          //     birthCity: 63,
                          //   },
                          //   {
                          //     playerId: "rkjwekr",
                          //     word: "Baran",
                          //     correctness: true,
                          //     birthCity: 63,
                          //   },
                          // ]}
                          // actions={[
                            // {
                            //   icon: 'save',
                            //   tooltip: 'Save User',
                            //   // onClick: (event, rowData) => alert("You saved " + rowData.name)
                            // },
                            // rowData => ({
                              // icon: () =><Checkbox/>,
                              // tooltip: '',
                              // onClick: handleClick,
                              // onClick: (event, rowData) => confirm("You want to delete " + rowData.name),
                              // disabled: rowData.word === 'نزده',
                              // disabled: rowData.birthYear < 2000
                            // })
                          // ]}
                          title={category}
                          options={{
                            actionsColumnIndex: 2,
                            // selection: true,
                            paging: false,
                            search: false,
                            padding: 'dense'
                          }}
                        />
                      </div>  
                    )}
                  </CardBody>
                  <p className={classes.divider}>First user sends Form, the form for this user and all other users will be disabled.</p>
                  <CardFooter className={classes.cardFooter}>
                  {resultsConfirmed?null:<Button simple color="primary" size="lg" onClick={handleConfirmResults}>
                        Confirm Results
                    </Button>}
                  </CardFooter>
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

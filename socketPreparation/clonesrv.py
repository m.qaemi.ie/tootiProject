"""
Clone server Model Five

Author: Min RK <benjaminrk@gmail.com
"""
import json
import os
import sys
from pathlib import Path
from uuid import uuid4

import django
# from django.conf import settings

import gevent

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'esmFamilProject.settings')

# from django.conf import settings
# settings.configure(DEBUG=True)
# django.setup()

import logging
import struct
import time
# from struct import calcsize

import zmq
# from zmq.eventloop.ioloop import IOLoop, PeriodicCallback
from tornado.ioloop import IOLoop, PeriodicCallback
import tornado.web

from zmq.eventloop.zmqstream import ZMQStream

path_to_cwd = Path(os.getcwd())
print(path_to_cwd.parent)
# path_to_cwd = path_to_cwd.parent

sys.path.append(os.path.join(path_to_cwd.parent))
# sys.path.insert(0, os.path.join(os.getcwd(), 'models.py'))
# print(sys.path)

# Ensure settings are read
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

# from creators.models import Game, Player
from kvmsg import KVMsg
from zhelpers import dump
# from constants import json_state_template
# from classicEFLogic import ClassicEFLogic
from socketPreparation.classicEFLogic import ClassicEFLogic
from loggingConfig import kvmsg_logger, classicEF_collected_logger, classicEF_published_logger, StructuredMessage


# simple struct for routing information for a key-value snapshot
class Route:
    def __init__(self, socket, identity, subtree):
        self.socket = socket  # ROUTER socket to send to
        self.identity = identity  # Identity of peer who requested state
        self.subtree = subtree  # Client subtree specification


# slice dict based on an string, return all key values in which keys start with an string.
def slicedict(d, s):
    return {k: v for k, v in d.items() if k.startswith(s)}


def send_single(key, kvmsg, route):
    """Send one state snapshot key-value pair to a socket"""
    # check front of key against subscription subtree:
    if kvmsg.key.startswith(route.subtree):
        # Send identity of recipient first
        route.socket.send(route.identity, zmq.SNDMORE)
        kvmsg.send(route.socket)


def get_latest_kvmsg_related_to_game(last_updates):
    biggest_sequence = 0
    key_with_biggest_sequence = ''
    kvmsg_with_biggest_sequence = {}
    for k, v in last_updates.items():
        if v.sequence > biggest_sequence:
            biggest_sequence = v.sequence
            key_with_biggest_sequence = k
            kvmsg_with_biggest_sequence = v
    return key_with_biggest_sequence, kvmsg_with_biggest_sequence


class CloneServer(object):
    # Our server is defined by these properties
    ip_address = '127.0.0.1'
    ctx = None  # Context wrapper
    kvmap = None  # Key-value store(for messages)
    player_games_kvmap = None  # key-value store(for games of each player)
    game_players_kvmap = None  # key-value store(for players of games)
    game_rounds_forms_kvmap = None  # key-value store(for forms of rounds of games)
    # game_name_kvmap = None  # key-value store(for name of games)
    loop = None  # IOLoop reactor
    port = None  # Main port we're working on
    sequence = 0  # How many updates we're at
    assign_game = None  # Handle snapshot requests
    snapshot = None  # Handle snapshot requests
    updater = None  # Handle updates requests
    publisher = None  # Publish updates to clients
    collector = None  # Collect updates from clients

    def __init__(self, port=5555):
        self.port = port
        self.ctx = zmq.Context()
        self.kvmap = {}
        self.player_games_kvmap = {}
        self.game_players_kvmap = {}
        self.game_rounds_forms_kvmap = {}  # todo change this to default tree.
        self.game_name_kvmap = {}
        self.loop = IOLoop.instance()

        # Set up our clone server sockets
        self.snapshot = self.ctx.socket(zmq.ROUTER)
        self.updater = self.ctx.socket(zmq.ROUTER)
        self.publisher = self.ctx.socket(zmq.PUB)
        # self.collector = self.ctx.socket(zmq.PULL)
        self.snapshot.bind("tcp://{}:{}".format(self.ip_address, self.port + 1))
        self.publisher.bind("tcp://{}:{}".format(self.ip_address, self.port + 2))
        self.updater.bind("tcp://{}:{}".format(self.ip_address, self.port + 3))
        # self.collector.bind("tcp://{}:{}".format(self.ip_address, self.port + 3))

        # Wrap sockets in ZMQStreams for IOLoop handlers
        self.snapshot = ZMQStream(self.snapshot)
        self.publisher = ZMQStream(self.publisher)
        self.updater = ZMQStream(self.updater)
        # self.collector = ZMQStream(self.collector)

        # Register our handlers with reactor
        self.snapshot.on_recv(self.handle_snapshot)
        self.updater.on_recv(self.handle_update)
        # self.collector.on_recv(self.handle_collect)
        self.flush_callback = PeriodicCallback(self.flush_ttl, 1000)

        # basic log formatting:
        logging.basicConfig(format="%(filename).8s_%(lineno)d_%(asctime)s_%(message)s", datefmt="%Y_%m_%d_%H_%M_%S",
                            level=logging.DEBUG)

    def start(self):
        # Run reactor until process interrupted
        # self.flush_callback.start()  # todo active this after experiment.
        try:
            self.loop.start()
        except KeyboardInterrupt:
            pass

    def send_snapshot_kvmsg(self, state_json_to_send, log_step, kvmsg_received, dealer_socket, identity_of_requester):  # todo rename this to publish.
        kvmsg_received.body = json.dumps(state_json_to_send, ensure_ascii=False).encode('utf8')  # check without ensure ascii
        # kvmsg_received.body = str(state_json).encode('utf8')
        dealer_socket.send(identity_of_requester, zmq.SNDMORE)
        # publish update
        self.sequence += 1
        kvmsg_received.sequence = self.sequence
        kvmsg_received.key = b"NOSUBTREE"  # todo
        kvmsg_received.send(dealer_socket)
        # store body
        kvmsg_received.store(self.kvmap)  # todo set ttl because this message has been stored.
        classicEF_published_logger.info(StructuredMessage("_state_json_step' ",
                                                          state_json=str(state_json_to_send).encode(sys.stdout.encoding,
                                                                                            errors='replace')))

        kvmsg_logger.info(StructuredMessage(f"published_update_step_{log_step}_sequence_{self.sequence}_kvmsg' ", kvmsg=str(kvmsg_received)))
        # kvmsg_logger.info(f"'published_update_step_{log_step}_sequence_{self.sequence}_kvmap' : r'{kvmsg_received}',")

    def publish_kvmsg(self, state_json_to_send, log_step, kvmsg_received):  # todo rename this to publish.
        kvmsg_received.body = json.dumps(state_json_to_send, ensure_ascii=False).encode('utf8')  # check without ensure ascii
        # kvmsg.body = str(state_json).encode('utf8')
        # publish update
        self.sequence += 1
        kvmsg_received.sequence = self.sequence
        kvmsg_received.send(self.publisher)
        # store body
        # kvmsg.body = json.dumps(state_json, ensure_ascii=False).encode('utf8')  # check without ensure ascii
        kvmsg_received.store(self.kvmap)  # todo set ttl because this message has been stored.
        classicEF_published_logger.info(StructuredMessage("_state_json_step' ",
                                                          state_json=str(state_json_to_send).encode(sys.stdout.encoding,
                                                                                            errors='replace')))

        kvmsg_logger.info(StructuredMessage(f"published_update_step_{log_step}_sequence_{self.sequence}_kvmsg' ", kvmsg=str(kvmsg_received)))
        # kvmsg_logger.info(f"'published_update_step_{log_step}_sequence_{self.sequence}_kvmap' : r'{kvmsg}',")

    def handle_snapshot(self, msg):
        """snapshot requests"""
        identity, *msg = msg
        kvmsg = KVMsg.from_msg(msg)

        # convert json to dic
        body_dic = json.loads(kvmsg.body.decode('utf8'))
        state_json = body_dic['state_json']
        # state_json = json.loads(body_dic['state_json'])
        classicEF_collected_logger.info(StructuredMessage("_state_json_step' ", state_json=str(state_json).encode(sys.stdout.encoding, errors='replace')))

        # prepare state_json of response according to logic
        try:
            classicEF_logic = ClassicEFLogic(state_json, kvmsg, identity, self.snapshot, self)
            classicEF_logic.reply_player()
        except Exception as e:
            classicEF_collected_logger.exception(StructuredMessage("_state_json_step' ",
                                                              error=e,
                                                              state_json=str(state_json).encode(sys.stdout.encoding,
                                                                                                errors='replace')),)

    # todo check player_id, if player_id is not unique send error message. this is not necessary for mobile.
    # because we have id of installation.

    def handle_update(self, msg):  # todo node receive "" updates.
        """Collect updates from clients"""
        # todo add players and join of players to back and front.
        # transform frames to key value message
        identity, *msg = msg
        kvmsg = KVMsg.from_msg(msg)

        # convert json to dic
        body_dic = json.loads(kvmsg.body.decode('utf8'))
        state_json = body_dic['state_json']
        # state_json = json.loads(body_dic['state_json'])
        classicEF_collected_logger.info(StructuredMessage("_state_json_step' ", state_json=str(state_json).encode(sys.stdout.encoding, errors='replace')))

        # prepare state_json of response according to logic
        try:
            classicEF_logic = ClassicEFLogic(state_json, kvmsg, identity, self.updater, self)
            classicEF_logic.update_game()
        except Exception as e:
            classicEF_collected_logger.exception(StructuredMessage("_state_json_step' ",
                                                              error=e,
                                                              state_json=str(state_json).encode(sys.stdout.encoding,
                                                                                                errors='replace')),)

        # set time to live if any
        try:
            # logging.info('%b',kvmsg.get(b'ttl', 0))
            # logging.info('%f',calcsize(kvmsg.get(b'ttl', 0)))

            ttl = struct.unpack('!f', kvmsg.get(b'ttl', 0))[0]
            # ttl = float(kvmsg.get(b'ttl', 0))
            # ttl = float(ord(kvmsg.get(b'ttl', 0)))
            # ttl = int.from_bytes(kvmsg.get(b'ttl', 0), byteorder='big')
        # except ValueError as e:
        except:
            # print("error", e,)
            ttl = int(kvmsg.get(b'ttl', 0))
            # logging.error(e)
        if ttl:
            # kvmsg[b'ttl'] = b'%f' % (time.time() + ttl)
            kvmsg[b'ttl'] = b'%f' % (time.time() + ttl)

        # save to key value map
        kvmsg.store(self.kvmap)
        # logging.info("I: publishing update=%d", self.sequence)
        logging.info(f"I: publishing update={self.sequence} kvmap={self.kvmap}")

    def flush_ttl(self):
        """Purge ephemeral values that have expired"""
        for key, kvmsg in list(self.kvmap.items()):
            # used list() to exhaust the iterator before deleting from the dict
            self.flush_single(kvmsg)

    def flush_single(self, kvmsg):
        """If key-value pair has expired, delete it and publish the fact
        to listening clients."""
        try:
            ttl = float(kvmsg.get(b'ttl', 0))  # todo can optimize.
        except:
            ttl = struct.unpack('!f', kvmsg.get(b'ttl', 0))[0]
        # ttl = struct.unpack('!d', kvmsg.get(b'ttl', 0))[0]

        # ttl = float(kvmsg.get(b'ttl', 0))
        # ttl = float(ord(kvmsg.get(b'ttl', 0)))
        # ttl = int.from_bytes(kvmsg.get(b'ttl', 0), byteorder='big')
        # print(ttl)
        # ttl = struct.unpack('=f', kvmsg.get(b'ttl', 0))
        if ttl and ttl <= time.time():
            kvmsg.body = b""
            self.sequence += 1
            kvmsg.sequence = self.sequence
            kvmsg.send(self.publisher)
            del self.kvmap[kvmsg.key]
            # logging.info("I: publishing delete=%d", self.sequence)
            logging.info(f"I: publishing delete={self.sequence} kvmap={self.kvmap}")


def clonesrv():
    # loop = asyncio.new_event_loop()
    clone = CloneServer()
    clone.start()


# def main():
#     # clone = CloneServer()
#     # clone.start()
#     gevent.joinall([
#         gevent.spawn(clonesrv),
#         # gevent.spawn(gr2),
#         # gevent.spawn(gr3),
#     ])

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(" Hello, world")


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ], debug=True)


if __name__ == "__main__":
    gevent.joinall([
        gevent.spawn(clonesrv),
        # gevent.spawn(gr2),
        # gevent.spawn(gr3),
    ])
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()


# if __name__ == '__main__':
#     main()

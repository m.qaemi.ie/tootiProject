# todo make a logger for redis
# todo we can remove try except from these functions because pyredis has a try except in execution of commands.
import json
import redis
from socketPreparation.loggingConfig import classicEF_collected_logger, StructuredMessage

host = '127.0.0.1'
port = 6379
player_data_db = redis.Redis(host=host, port=port, db=0) # todo move to env file.
game_data_db = redis.Redis(host=host, port=port, db=1)
rounds_data_db = redis.Redis(host=host, port=port, db=2)
# db to save prepared messages specially for step referee
prepared_messages_db = redis.Redis(host=host, port=port, db=3)

game_data_db_pipe = game_data_db.pipeline()


def transaction_game_data_db(pipe):
    new_string1 = pipe.get('string1') + pipe.get('string2')
    pipe.set('string1', new_string1)


def log_error(e, db, name):
    classicEF_collected_logger.exception(StructuredMessage("redis_db' ", error=e, db=db, name=name,))


def set_name_value(db, name, value):
    """set a value in redis with a redis key(value must be string or byte or number and any other redis data type)"""
    return db.set(name, value)


def delete_names(db, *names):
    """delete some keys from a db, a key is ignored if it does not exist."""
    return db.delete(*names)


def set_name_bytes(db, name, value):
    """give any value in term of python data type convert them to bytes and set in redis with a redis key(name)"""
    return db.set(name, json.dumps(value))


def get_name_bytes(db, name):
    """get value from redis in term of bytes and convert them to python dict."""
    result = db.get(name)
    if result:
        return json.loads(result)
    else:
        return {}


def get_multiple_name_bytes_of_json(db, keys, *args):
    """Returns a list of values ordered identically to keys."""
    list_of_byte_values = db.mget(keys, *args)
    if list_of_byte_values:
        list_of_strings = convert_list_of_bytes_to_list_of_strings(list_of_byte_values)
        list_of_dicts = convert_list_of_strings_of_json_to_list_of_dicts(list_of_strings)
        return list_of_dicts
    else:
        return []


def hset_name_dict(db, name, dict):
    """set a dict in redis with a redis key(name) and added each key value in dict to hash and return number of added fields"""
    result = db.hset(name, mapping=dict)
    return result


def hset_name_dict_lock(db, name, dict):
    """set a dict in redis with a redis key(name) with lock"""
    with db.lock('hset_lock'): # todo must name of lock be changed? move lock to other file.
        result = db.hset(name, mapping=dict)
    return result


def hgetall_name_dict(db, name):
    """get all of a dict from redis with a redis key(name)"""
    return db.hgetall(name)


def hget_name_key_value(db, name, key):
    """get value of a key of a dict from redis with a python dict key and redis key(name)"""
    return db.hget(name, key)


def lpush_name_values(db, name, *values):
    """push some values to a list on a redis db"""
    db.lpush(name, *values)  # todo do if true ... needed? number of attempt or say to requester try again.


def convert_list_of_bytes_to_list_of_strings(list_of_bytes_item):
    list_of_strings = [list_of_bytes_item[i].decode('utf8') for i in range(len(list_of_bytes_item))]
    return list_of_strings


def convert_list_of_strings_of_json_to_list_of_dicts(list_of_strings_of_jsons):
    list_of_dicts = [json.loads(list_of_strings_of_jsons[i]) for i in range(len(list_of_strings_of_jsons))]
    return list_of_dicts


def lrange_get_list(db, name):
    """get a list that its items type are bytes and converts them to string."""
    # retrun list of bytes
    list_of_bytes_item = db.lrange(name, 0, -1)
    # convert to list of strings
    list_of_strings = convert_list_of_bytes_to_list_of_strings(list_of_bytes_item)
    return list_of_strings


def list_remove(db, name, count, value):
    """Remove the first count occurrences of elements equal to value from the list stored at name.
    The count argument influences the operation in the following ways:
    count > 0: Remove elements equal to value moving from head to tail.
    count < 0: Remove elements equal to value moving from tail to head.
    count = 0: Remove all elements equal to value."""
    db.lrem(name, count, value)


def sadd_name_values(db, name, *values):
    """Add value(s) to set name on a redis db"""
    return db.sadd(name, *values)  # return value: integer reply: the number of elements that were added to the set, not including all the elements already present into the set.


def string_set_scan(db, name, cursor=0, match=None, count=None):
    """Incrementally return lists of elements in a set. Also return a cursor indicating the scan position. iterates
     elements of Sets types. Since these commands allow for incremental iteration, returning only a small number of
     elements per call. SCAN is a cursor based iterator. This means that at every call of the command, the server
     returns an updated cursor that the user needs to use as the cursor argument in the next call. An iteration starts
     when the cursor is set to 0, and terminates when the cursor returned by the server is 0"""
    cursor, list_of_bytes_item = db.sscan(name, cursor, match, count)
    list_of_strings = convert_list_of_bytes_to_list_of_strings(list_of_bytes_item)
    return cursor, list_of_strings


def set_remove(db, name, *values):
    """Remove values from set name"""
    return db.srem(name, *values)


def sorted_set_add(db, name, mapping):
    """Set any number of element-name, score pairs to the key name. Pairs are specified as a dict of element-names keys
    to score values."""
    db.zadd(name, mapping, nx=False, xx=False, ch=False, incr=False)


def sorted_string_set_scan(db, name, cursor=0, match=None, count=None):
    """Incrementally return lists of elements in a sorted set. Also return a cursor indicating the scan position. match
     allows for filtering the keys by pattern. count allows for hint the minimum number of returns.
     score_cast_func a callable used to cast the score return value. Since these commands allow for incremental
     iteration, returning only a small number of elements per call. SCAN is a cursor based iterator. This means that
     at every call of the command, the server returns an updated cursor that the user needs to use as the cursor
     argument in the next call. An iteration starts when the cursor is set to 0, and terminates when the cursor
     returned by the server is 0"""
    cursor, list_of_tuples_of_value_score = db.zscan(name, cursor, match, count)
    list_of_bytes_item = [i[0] for i in list_of_tuples_of_value_score]
    list_of_strings = convert_list_of_bytes_to_list_of_strings(list_of_bytes_item)
    return cursor, list_of_strings


def sorted_set_remove(db, name, *values):
    """Remove member values from sorted set name"""
    return db.zrem(name, *values)


def sorted_set_remrangebyscore(db, name, min, max):
    """Remove all elements in the sorted set name with scores between min and max. Returns the number of elements
    removed."""
    num_of_elements_removed = db.zremrangebyscore(name, min, max)
    return num_of_elements_removed

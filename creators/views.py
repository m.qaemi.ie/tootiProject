# from django.http import JsonResponse
from datetime import datetime
from uuid import uuid4

from rest_framework.response import Response
from rest_framework.views import APIView

from creators.models import Game, Round
from creators.serializers import GameSerializer, DeviceInstalledAppSerializer, RoundSerializer, \
    PlayerFillsFormSerializer, ValidIdSerializer
from rest_framework import mixins
from rest_framework import generics

from django.contrib.auth import get_user_model

from interactions.models import PlayerFillsForm
from socketPreparation.pyredis import sorted_set_add, game_data_db

ProjectUser = get_user_model()


class ValidId():
    def __init__(self, valid_id):
        self.valid_id = valid_id


class ValidIdList(APIView):
    """"""
    def get(self, request, format=None):
        # snippets = Snippet.objects.all()
        # serializer = SnippetSerializer(snippets, many=True)
        nr_of_added_valid_id = 0
        while nr_of_added_valid_id == 0:  # while is to ensure unique id.
            valid_id = str(uuid4())[0:8]
            now_timestamp = datetime.timestamp(datetime.now())
            nr_of_added_valid_id = sorted_set_add(game_data_db, 'validIds', {valid_id: now_timestamp})
            # todo implement a function to run every 30 minutes and deletes expired ids.
        valid_id = ValidId(valid_id)
        serializer = ValidIdSerializer(valid_id)
        return Response(serializer.data)


class VersionInfo():  # todo admin send info of version to server and get all versions query.
    pass


class DeviceInstalledAppList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = Game.objects.all()   # todo this must retrieve all games of a playowner.(device_id or logined_user)
    serializer_class = DeviceInstalledAppSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):  # todo
        return self.list(request, *args, **kwargs)

    def perform_create(self, serializer):
        # queryset = ProjectUser.objects.filter(user=self.request.user)
        # queryset = ProjectUser.objects.get(email=self.request.)
        # if queryset.exists():
        #     raise ValidationError('You have already signed up')
        # serializer.save(user=self.request.user)
        serializer.save()

    def post(self, request, *args, **kwargs):  # todo if app VersionCode == app_version_installed send 305 and check that device
        # todo and codeVersion recorded or not. if versionCode != app_version_installed send info of last version.
        obj = self.create(request, *args, **kwargs) # todo
        return obj
        # return 'all things good.'


# class DeviceUserList(mixins.ListModelMixin,
#                   mixins.CreateModelMixin,
#                   generics.GenericAPIView):
#     queryset = ProjectUser.objects.all()
#     serializer_class = DeviceUserSerializer
#     # parser_classes = [JSONParser]
#
#     def get(self, request, *args, **kwargs):  # todo remove this? or correct this.
#         return self.list(request, *args, **kwargs)
#
#     def perform_create(self, serializer):
#         # queryset = ProjectUser.objects.filter(user=self.request.user)
#         # queryset = ProjectUser.objects.get(email=self.request.)
#         # if queryset.exists():
#         #     raise ValidationError('You have already signed up')
#         # serializer.save(user=self.request.user)
#         serializer.save()
#
#     def post(self, request, *args, **kwargs):
#         user_installed = self.create(request, *args, **kwargs)
#         return Response(user_installed, status=201)
#         # return 'all things good.'


class GameList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = Game.objects.all()   # todo this must retrieve all games of a playowner.(device_id or logined_user)
    serializer_class = GameSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):  # todo
        return self.list(request, *args, **kwargs)

    def perform_create(self, serializer):
        # queryset = ProjectUser.objects.filter(user=self.request.user)
        # queryset = ProjectUser.objects.get(email=self.request.)
        # if queryset.exists():
        #     raise ValidationError('You have already signed up')
        # serializer.save(user=self.request.user)
        serializer.save()

    def post(self, request, *args, **kwargs):
        game_created = self.create(request, *args, **kwargs)
        # return Response(game_created, status=201)
        return game_created
        # return 'all things good.'


class RoundList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = Round.objects.all()   # todo this must retrieve all rounds of ... .(device_id or logined_user)
    serializer_class = RoundSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):  # todo
        return self.list(request, *args, **kwargs)

    def perform_create(self, serializer):
        # queryset = ProjectUser.objects.filter(user=self.request.user)
        # queryset = ProjectUser.objects.get(email=self.request.)
        # if queryset.exists():
        #     raise ValidationError('You have already signed up')
        # serializer.save(user=self.request.user)
        serializer.save()

    def post(self, request, *args, **kwargs):
        round_saved = self.create(request, *args, **kwargs)
        # return Response(game_created, status=201)
        return round_saved
        # return 'all things good.'


class PlayerFillsFormList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = PlayerFillsForm.objects.all()   # todo this must retrieve all rounds of ... .(device_id or logined_user)
    serializer_class = PlayerFillsFormSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):  # todo
        return self.list(request, *args, **kwargs)

    def perform_create(self, serializer):
        # queryset = ProjectUser.objects.filter(user=self.request.user)
        # queryset = ProjectUser.objects.get(email=self.request.)
        # if queryset.exists():
        #     raise ValidationError('You have already signed up')
        # serializer.save(user=self.request.user)
        serializer.save()

    def post(self, request, *args, **kwargs):
        game_id = kwargs['game_id']
        game = Game.objects.get_or_create(id=game_id, device_id="52ab6165-c0f6-4163-aa25-c223d55a00d6")
        play_saved = self.create(request, *args, **kwargs)
        # return Response(game_created, status=201)
        return play_saved
        # return 'all things good.'




'''
class AnonymousUserList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = ProjectUser.objects.all()  # todo list of anonymous user not all users.
    serializer_class = AnonymousUserSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class GameList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
    
'''

'''
from rest_framework.exceptions import ValidationError

from creators.serializers import ProviderCreatorSerializer, ProviderSerializer
from rest_framework import mixins
from rest_framework import generics
# from rest_framework.parsers import JSONParser
from creators.models import Provider, Service


from django.contrib.auth import get_user_model
ProjectUser = get_user_model()


class ProviderCreatorList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = ProjectUser.objects.all()
    serializer_class = ProviderCreatorSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def perform_create(self, serializer):
        # queryset = ProjectUser.objects.filter(user=self.request.user)
        # queryset = ProjectUser.objects.get(email=self.request.)
        # if queryset.exists():
        #     raise ValidationError('You have already signed up')
        # serializer.save(user=self.request.user)
        serializer.save()

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


# provider creator makes a provider and a service at the same time at the sign up steps.
class ProviderList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def perform_create(self, serializer):
        name_queryset = Service.objects.filter(name=self.request.data['name'])
        # queryset = ProjectUser.objects.get(email=self.request.)
        if name_queryset.exists():
            raise ValidationError('This name have already signed up.')

        url_address_queryset = Service.objects.filter(urlAddress=self.request.data['urlAddress'])
        if url_address_queryset.exists():
            raise ValidationError('This site have already signed up.')
        # serializer.save(user=self.request.user)
        serializer.save()

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


'''
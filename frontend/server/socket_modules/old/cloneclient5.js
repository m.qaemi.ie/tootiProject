const zmq = require('zeromq');
const uuid = require('uuid');
const uuid4 = uuid.v4;
// import { v4 as uuidv4 } from 'uuid';
// console.log(uuid4()); // ⇨ '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d'
// console.log(typeof(uuid4()));

const seedrandom = require('seedrandom');
// const rng = seedrandom('added entropy.', { entropy: true });
// console.log(rng.int32());

function bin2string(array){
    var result = "";
    for(var i = 0; i < array.length; ++i){
        result+= (String.fromCharCode(array[i]));
    }
    return result;
}

function encode_properties(properties_obj) {
    prop_s = '';
    for (let [key, value] of Object.entries(properties_obj)) {
        // console.log(`${key}: ${value}`);
        prop_s += `${key}=${value}\n`;
    }
    return prop_s;
}

function decode_properties(prop_s) {
    prop = {};
    line_array = prop_s.split("\n");
    for(let line in line_array){
        try{
            let key, value = line.split("=");
            prop[key] = value;
        }
        catch (e) {
            print("error occured.")
        }
    }
    return prop;
}


class KVMsg{
    // Message is formatted on wire as 5 frames:
    // frame 0: key (0MQ string)
    // frame 1: sequence (8 bytes, network order)
    // frame 2: uuid (blob, 16 bytes)
    // frame 3: properties (0MQ string)
    // frame 4: body (blob)

    // key = null;
    // sequence = 0;
    // uuid = null;
    // properties = null;
    // body = null;

    // constructor(sequence=0, uuid=null, key=null, properties=null, body=null) {
    constructor(sequence, uuid, key, properties, body) {
        //     assert isinstance(sequence, int)
        if(sequence){this.sequence=sequence;}else{this.sequence = 0;}
        if(uuid){
            this.uuid = uuid;
        }else {
            this.uuid = uuid4();
        }
        // if(uuid === null){
        //     this.uuid = uuid4();
        // }else {
        //     this.uuid = uuid;
        // }
        // this.key = key;
        if(key){this.key=key;}else{this.key = null;}
        if(properties){
            this.properties === properties;
        }else {
            this.properties = {};
        }
        // if(properties===null){
        //     this.properties === {};
        // }else {
        //     this.properties = properties;
        // }
        // this.body = body;
        if(body){this.body=body;}else{this.body = null;}
    }

    static get_items(k){
        return  this.properties[k];
    }
    static set_items(k , v){
        this.properties[k] = v;
    }
    static get_prop(k, default_return=null){
        if(k){
            return this.properties[k];
        }else{
            return default_return;
        }
    }
    static store(obj){
        // Store me in an object if I have anything to store
        // else delete me from the object.
        if( (this.key!==null) && (this.body!==null) ){
            obj[this.key] = this;
        }else if(this.key in obj){
            delete obj[this.key];
        }
    }
    static send(socket){
        //Send key-value message to socket; any empty frames are sent as such.
        let key;
        if(this.key===null){key = '';}else{key = this.key;}
        let seq_s = this.sequence;
        let body;
        if(this.body===null){body = '';}else{body = this.body;}
        let prop_s = encode_properties(this.properties);
        socket.send([key, seq_s, this.uuid, prop_s, body])
    }
    static from_msg(msg){
        //Construct key-value message from a multipart message.
        let key, seq_s, uuid, prop_s, body = msg;
        if(!key){key=null;}
        let seq = seq_s;  //todo
        if(!body){body=null;}
        let prop = decode_properties(prop_s);
        let kvmsg = new KVMsg(sequence=seq, uuid=uuid, key=key, properties=prop, body=body);
        return new Promise(function (resolve, reject) {
            resolve(kvmsg);
        });
    }
    static recv(socket){
        //Reads key-value message from socket, returns new kvmsg instance.
        // return new Promise(function (resolve, reject) {
        //     let kvmsg = {};
            socket.on('message', function(msg) {  //todo try catch
                // kvmsg = KVMsg.from_msg(msg);
                // return new Promise(function (resolve, reject) {
                    return msg;
                // });
                // return new Promise(function (resolve, reject) {
                //     resolve(msg);
                // })
            });
            // return kvmsg;
        // });
        // return this.from_msg(socket.recv_multipart())
    }
    // @classmethod
    // def recv(cls, socket):
    //     """Reads key-value message from socket, returns new kvmsg instance."""
    // return cls.from_msg(socket.recv_multipart())
    //
    // @classmethod
    // def from_msg(cls, msg):
    //     """Construct key-value message from a multipart message"""
    // key, seq_s, uuid, prop_s, body = msg
    // key = key if key else None
    // seq = struct.unpack('!q', seq_s)[0]
    // body = body if body else None
    // prop = decode_properties(prop_s)
    // return cls(seq, uuid=uuid, key=key, properties=prop, body=body)
}

const send_update_to_server = (subtree, player_id) =>{
    // SUBTREE = str(subtree)
    const SUBTREE = subtree;

    // Prepare our context and sockets. in javascript we dont have context because of async...
    let snapshot = zmq.socket('dealer');
    snapshot.linger = 0;
    snapshot.connect("tcp://localhost:5556");

    let subscriber = zmq.socket('sub');
    subscriber.linger = 0;
    subscriber.connect("tcp://localhost:5557");
    subscriber.subscribe(SUBTREE);
    // subscriber.setsockopt(zmq.SUBSCRIBE, SUBTREE.encode())

    // const publisher = zmq.socket('dealer');
    let publisher = zmq.socket('push');
    publisher.linger = 0;
    publisher.connect("tcp://localhost:5558")

    const rng = seedrandom('added entropy.', { entropy: true });
    kvmap = {};

    // Get state snapshot
    let sequence = 0;
    snapshot.send(["ICANHAZ?", SUBTREE]);
    flag = true;
    while(flag){
        try{
            recieve_snapshot = new Promise(function (relolve, reject) {
                return KVMsg.recv(snapshot);
            });
            // recieve_snapshot = function () {
            //     return KVMsg.recv(snapshot);
            // };
            // recieve_snapshot()
            recieve_snapshot
                .then(function (msg) {
                    return KVMsg.from_msg(mgs);
                })
                .then(function (kvmsg) {
                    if(kvmsg.key == "KTHXBAI"){
                        sequence = kvmsg.sequence;
                        console.log(`player id:${player_id}, Received snapshot=${sequence} key=${kvmsg.key} body=${kvmsg.body}`);
                        // break;
                        flag = false;
                    }
                    kvmsg.store(kvmap);
                })
                .catch(function (e) {
                    console.log(e);
                })
            ;
            // recieve_snapshot = new Promise(function (resolve, reject) {
            //     return KVMsg.recv(snapshot);
            // });
            // KVMsg.recv(snapshot)
            //     .then(function (kvmsg) {
            //         if(kvmsg.key == "KTHXBAI"){
            //             sequence = kvmsg.sequence;
            //             console.log(`player id:${player_id}, Received snapshot=${sequence} key=${kvmsg.key} body=${kvmsg.body}`);
            //             // break;
            //         }
            //         kvmsg.store(kvmap);
            //     })
            //     .catch(function (e) {
            //     console.log(e);
            // });
        }
        catch (e) {
            // throw('cant receive kvmsg.');
            console.log(e);
        }
    }
    // while(true){
    //     try{
    //         kvmsg = KVMsg.recv(snapshot);
    //         // snapshot.on('message', function() {
    //         //     // Note that separate message parts come as function arguments.
    //         //     var args = Array.apply(null, arguments);
    //         //     // Pass array of strings/buffers to send multipart messages.
    //         //     backend.send(args);
    //         // });
    //     }
    //     catch (e) {
    //         // throw('cant receive kvmsg.');
    //         console.log(e);
    //     }
    //     if(kvmsg.key == "KTHXBAI"){
    //         sequence = kvmsg.sequence;
    //         // logging.info(f'{player_id}: Received snapshot={sequence} key={kvmsg.key} body={kvmsg.body}')
    //         console.log(`player id:${player_id}, Received snapshot=${sequence} key=${kvmsg.key} body=${kvmsg.body}`);
    //         break;
    //     }
    //     kvmsg.store(kvmap);
    // }
};

game_id =uuid4();
console.log(game_id);
send_update_to_server(game_id, 'player1');

from datetime import timedelta, datetime
from rest_framework.response import Response
from rest_framework.views import APIView
from queries.serializers import ActiveGamesListSerializer
from socketPreparation.pyredis import game_data_db, sorted_string_set_scan, sorted_set_remrangebyscore, get_multiple_name_bytes_of_json


class GameListData():
    def __init__(self, cursor, games_list, games_datas):
        self.cursor = cursor
        self.games_list = games_list
        self.games_datas = games_datas
        # self.created = created or datetime.now()


class GameList(APIView):
    """"""
    def get(self, request, cursor, format=None):
        # snippets = Snippet.objects.all()
        # serializer = SnippetSerializer(snippets, many=True)
        # removes games expired
        min_timestamp = 0
        max_timestamp = datetime.timestamp(datetime.now() - timedelta(minutes=30))
        sorted_set_remrangebyscore(game_data_db, 'activeGames', min=min_timestamp, max=max_timestamp)
        cursor, games_list = sorted_string_set_scan(game_data_db, 'activeGames', cursor=cursor, count=10)
        list_of_game_datas_of_active_games = get_multiple_name_bytes_of_json(game_data_db, games_list)
        for i in range(len(list_of_game_datas_of_active_games)):
            del list_of_game_datas_of_active_games[i]['rounds']
        game_list_data = GameListData(cursor, games_list, list_of_game_datas_of_active_games)
        serializer = ActiveGamesListSerializer(game_list_data)
        # self.state_json['cursorOfActiveGamesList'] = cursor
        # self.state_json['activeGamesList'] = games_list
        return Response(serializer.data)

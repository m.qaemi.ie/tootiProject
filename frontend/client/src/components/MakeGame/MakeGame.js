import React, { useState } from "react";
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";

import styles from "../assets/jss/views/loginPage.js";

import image from "../assets/img/bg7.jpg";

const useStyles = makeStyles(styles);

function makeid(length) {  // todo do this server-side.
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export default function LoginPage(props) {
  const [cardAnimaton, setCardAnimation] = useState("cardHidden");
  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();
  const { ...rest } = props;
  console.log('player_auth value:', Cookies.getJSON('player_auth') || {});
  let {access_token='', name='', playerId='',} = Cookies.getJSON('player_auth') || {};
  // if player auth was in cookies it will use them but if was not in cookies it will generate them.
  if (!name || !playerId) { //todo check expiration and refresh of token... expiration of playerId and name cookies should be equal to access token.
    let names = ['wasp', 'gaur', 'butterfly', 'bee', 'ant'];
    name = names[Math.floor(Math.random() * names.length)]; // select random from names
    playerId = makeid(6);
    const expireseconds= new Date(new Date().getTime() + 7 * 1000); // after 15 seconds! for test... todo
    Cookies.set('player_auth', { name , playerId }, { expires: expireseconds });
    console.log('random name and player id assigned.', name, playerId);
  } else {
    console.log('player id and name exists because access token exists.', );
  }

  let gameId = makeid(8); // todo any need to give this from server?

  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="Esm Famil"
        rightLinks={<HeaderLinks />}
        {...rest}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Login</h4>
                    <h4>player id: {playerId}</h4>
                    <h4>name: {name}</h4>
                    <div className={classes.socialLine}>
                      <Button
                        justIcon
                        href="#pablo"
                        target="_blank"
                        color="transparent"
                        onClick={e => e.preventDefault()}
                      >
                        <i className={"fab fa-twitter"} />
                      </Button>
                      <Button
                        justIcon
                        href="#pablo"
                        target="_blank"
                        color="transparent"
                        onClick={e => e.preventDefault()}
                      >
                        <i className={"fab fa-facebook"} />
                      </Button>
                      <Button
                        justIcon
                        href="#pablo"
                        target="_blank"
                        color="transparent"
                        onClick={e => e.preventDefault()}
                      >
                        <i className={"fab fa-google-plus-g"} />
                      </Button>
                    </div>
                  </CardHeader>
                  <CardFooter className={classes.cardFooter}>
                    <Link to={`/PlayGame/${gameId}`}>
                      <Button simple color="primary" size="lg">
                        Make Game
                      </Button>
                    </Link>
                  </CardFooter>
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

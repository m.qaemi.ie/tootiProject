import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";
import CheckboxList from '../MaterialUiComponents/ChecboxList';
import styles from "../assets/jss/views/loginPage.js";

import image from "../assets/img/bg7.jpg";
import CounterDown from '../CounterDown/CounterDown';

const useStyles = makeStyles(styles);

export default function RoundResult({ name, playerId, gameId, gameName, step, action, roundNumber, startNumber, roundsRefereed, sendMessage, lastRound, ...rest }) {
  const classes = useStyles();
  const [cardAnimaton, setCardAnimation] = useState("cardHidden");
  const [cardOfFooter, setCardOfFooter] = useState(<CardFooter className={classes.cardFooter}>
    <CounterDown startNumber={startNumber}/>
  </CardFooter>);

  setTimeout(function() {
    setCardAnimation("");
  }, 700);

  setTimeout(function() {
    if (roundNumber!==lastRound) {
      setCardOfFooter(<CardFooter className={classes.cardFooter}>
        <Button simple color="primary" size="lg" onClick={StartNextRound}>
            Start Next Round
        </Button>
      </CardFooter>);
    } else if (roundNumber===lastRound) {
      setCardOfFooter(<CardFooter className={classes.cardFooter}>
        <Button simple color="primary" size="lg" onClick={ShowUsGameResult}>
            Show Us Game Result
        </Button>
      </CardFooter>);  
    }
  }, startNumber * 1000);


  const formFields = roundsRefereed[roundNumber].playersForms[playerId].form;
  // const [inputs, setInputs] = useState(formFields);
  const [inputDisabled, setInputDisabled] = useState(true);
  // const [submitForm, setSubmitForm] = useState(false);
  // make list of fields that correctness of them is true.
  const initialChecked = Object.keys(formFields).map((key) => {if (formFields[key].c) return key})
  const [checked, setChecked] = useState(initialChecked);

  const StartNextRound = (event) => {
    event.preventDefault();
    // setInputDisabled(inputDisabled => true)
    let message = { playerId, gameId, roundNumber, step, action: 'startRound', }
    sendMessage('sendUpdate', message);
  };

  const ShowUsGameResult = (event) => {
    event.preventDefault();
    // setInputDisabled(inputDisabled => true)
    let message = { playerId, gameId, roundNumber, step:'roundResult', action: 'giveTotal', }
    sendMessage('sendUpdate', message);
  };

  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="Esm Famil"
        rightLinks={<HeaderLinks />}
        {...rest}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Round Result</h4>
                  </CardHeader>
                  <CardBody>
                    <CheckboxList checked={checked} setChecked={setChecked} formFields={formFields} disabledCheckbox={inputDisabled}/>
                  </CardBody>
                  <p className={classes.divider}>First user sends Form, the form for this user and all other users will be disabled.</p>
                  {cardOfFooter}
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

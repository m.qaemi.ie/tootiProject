"""
Clone client Model Five

Author: Min RK <benjaminrk@gmail.com
"""
import json
import logging
import pathlib
import random
import struct
import time
from uuid import uuid4

# import zmq
import gevent
import zmq.green as zmq
# from gevent import spawn, spawn_later

import sys
import os

# from gevent._compat import xrange

current_directory = pathlib.Path(__file__).parent.absolute()
sys.path.append(os.path.join(current_directory))
from kvmsg import KVMsg

# SUBTREE = "/client/"
# SUBTREE = bytes(str(uuid4()), 'utf8')
# SUBTREE = bytes('76ded477-bbd6-4d21-b06b-b13fbddb3a40', 'utf8')

# basic log formatting:
logging.basicConfig(format="%(asctime)s [%(filename)s:%(lineno)d] %(message)s ", datefmt="%Y-%m-%d %H:%M:%S",
                    level=logging.INFO)


def send_update_to_server(subtree, player_id):
    # SUBTREE = bytes(str(subtree), 'utf8')
    SUBTREE = str(subtree)

    # Prepare our context and subscriber
    ctx = zmq.Context()
    # ip_address = "130.185.76.61"
    ip_address = "localhost"
    port = 5555
    # assign_game = ctx.socket(zmq.DEALER)
    # assign_game.linger = 0
    # assign_game.connect("tcp://localhost:5555")
    snapshot = ctx.socket(zmq.DEALER)
    snapshot.linger = 0
    # snapshot.connect("tcp://localhost:5556")
    snapshot.connect("tcp://{}:{}".format(ip_address, port+1))
    subscriber = ctx.socket(zmq.SUB)
    subscriber.linger = 0
    subscriber.setsockopt(zmq.SUBSCRIBE, SUBTREE.encode())
    # subscriber.setsockopt(zmq.SUBSCRIBE, SUBTREE)
    # subscriber.connect("tcp://localhost:5557")
    subscriber.connect("tcp://{}:{}".format(ip_address, port+2))
    publisher = ctx.socket(zmq.PUSH)
    publisher.linger = 0
    # publisher.connect("tcp://localhost:5558")
    publisher.connect("tcp://{}:{}".format(ip_address, port+3))

    random.seed(time.time())
    kvmap = {}

    '''
    # request for participate in a game and server will assign this player to game and return an id of game.
    # sequence = 0
    device_id = bytes(str(uuid4()), 'utf8')
    # logging.info("I create device id= %s", device_id)
    print(device_id)
    assign_game.send_multipart([b"IWANTTOPLAY?", SUBTREE.encode(), device_id])
    while True:
        try:
            kvmsg = KVMsg.recv(assign_game)
        except:
            raise
            return          # Interrupted

        if kvmsg.key == b"GAMEIDSENT":
            sequence = kvmsg.sequence
            print("I: Received game id=%d %b", sequence, kvmsg.body)
            break          # Done
        kvmsg.store(kvmap)
    '''

    # Get state snapshot
    sequence = 0
    snapshot.send_multipart([b"ICANHAZ?", SUBTREE.encode()])
    # SUBTREE = bytes(str(uuid4()), 'utf8')
    # snapshot.send_multipart([b"ICANHAZ?", SUBTREE])
    while True:
        try:
            kvmsg = KVMsg.recv(snapshot)
        except:
            raise
            return          # Interrupted

        if kvmsg.key == b"KTHXBAI":
            sequence = kvmsg.sequence
            # print("I: Received snapshot=%d" % sequence)
            logging.info(f'{player_id}: Received snapshot={sequence} key={kvmsg.key} body={kvmsg.body}')
            break          # Done
        kvmsg.store(kvmap)

    poller = zmq.Poller()
    poller.register(subscriber, zmq.POLLIN)

    # alarm = time.time()+1.
    alarm = time.time()+13.
    form_number = 0
    while True:
        tickless = 1000*max(0, alarm - time.time())
        logging.info(str(alarm-time.time()))
        try:
            items = dict(poller.poll(tickless))
        except:
            break           # Interrupted

        if subscriber in items:
            kvmsg = KVMsg.recv(subscriber)

            # Discard out-of-sequence kvmsgs, incl. heartbeats
            if kvmsg.sequence > sequence:
                sequence = kvmsg.sequence
                kvmsg.store(kvmap)
                action = "update" if kvmsg.body else "delete"
                # print("I: received %s=%d" % (action, sequence))
                logging.info(f'{player_id}: received {action}={sequence} key={kvmsg.key} body={kvmsg.body} uuid={kvmsg.uuid}')

        # If we timed-out, generate a random kvmsg
        if time.time() >= alarm:
            kvmsg = KVMsg(0)
            kvmsg.key = SUBTREE.encode() + b'%d' % random.randint(1, 10000)
            # kvmsg.body = b"%d" % random.randint(1, 1000000)
            # kvmsg.body = f'form_number_{form_number}_{player_id}'.encode()
            form = f'form_number_{form_number}_{player_id}'
            form_json = json.dumps({"my_json_form": form})
            kvmsg.body = form_json.encode()
            form_number += 1
            # kvmsg[b'ttl'] = random.randint(0, 30)
            kvmsg[b'ttl'] = struct.pack('!f', random.randint(0, 30))
            kvmsg.send(publisher)
            kvmsg.store(kvmap)
            alarm = time.time() + 13.
            # logging.info(f'{player_id}: kvmap={kvmap}')
            # time.sleep(3)

    print(" Interrupted\n%d messages in" % sequence)


# spawn(send_update_to_server)

def main():
    # game_id1 = uuid4()
    game_id1 = 'e3175d25-949f-406a-af01-437fc6c1' # todo
    game_id2 = uuid4()
    # send_update_to_server()
    gevent.joinall([
        gevent.spawn(send_update_to_server, game_id1, 'player1'),
        # gevent.spawn(send_update_to_server, game_id2, 'player2'),
        gevent.spawn_later(5, send_update_to_server, game_id2, 'player2'),

        # gevent.spawn(send_update_to_server, game_id2, 'player3'),
        # gevent.spawn_later(5, send_update_to_server, game_id2, 'player4'),
        # gevent.spawn(send_update_to_server),
        # gevent.spawn(send_update_to_server),

        # gevent.spawn(gr2),
        # gevent.spawn(gr3),
    ])
    # threads = [gevent.spawn(task, i) for i in xrange(10)]
    # threads = [gevent.spawn(send_update_to_server) for i in xrange(10)]
    # gevent.joinall(threads)


if __name__ == '__main__':
    main()

// stringifies json and parse value that not parsed...
const parse_and_stringify = (data) => JSON.parse(JSON.stringify(data), (key, value) => // todo redundant
            typeof value === 'string'
              ? JSON.parse(value) // return parsed json
              : value     // return everything else unchanged
          );
// todo set default or an error that shows cases do not match any of these cases.
export default function handleUpdate(update_message, state, sendMessage) {
    let json_state = update_message;
    let json_state_body;
    json_state_body = JSON.parse(json_state.body);
    let { gameId='', gameName='', step='joinGame', action='', roundNumber=1, startNumber=5, creator='', players=[], playersData={}, rounds={}, roundData='', lastRound='', totalResult='', playerId='', playerRefereed='', playerRoundData={}, selectedCategories=[], resultsConfirmed=false, } = json_state_body || {};
    roundNumber=parseInt(roundNumber);
    // let startNumber = stepEndTime - Math.floor((new Date()).getTime() / 1000); // todo
    //states should be set in all receive of update.
    let states_to_set = {gameId, gameName, step, action, roundNumber, startNumber, creator, players, playersData, lastRound, playerRequestedUpdate: playerId, }
    switch(step) {
      // case 'gameNotAvailable': Object.assign(states_to_set, { uuidForGameId, activeGames, }); break;
      case 'beforeStart': break;
      case 'formFilling':
        if (roundData) {
          roundData = parse_and_stringify(roundData);
        } else if (state.roundData) {
          playerRoundData = parse_and_stringify(playerRoundData);
        }
        switch(action) {
          case 'fillForm': Object.assign(states_to_set, { rounds, }); break;
        }
        break;
      case 'formSubmitting': 
        switch(action) {
          // case 'refereeForm': Object.assign(states_to_set, { roundData, }); break;
          // case 'updatePlayerRoundData':
          //   roundData = state.roundData;
          //   roundData[playerRefereed] = playerRoundData;
          //   Object.assign(states_to_set, { roundData, });
          //   break;
          case 'submitForm': break;
          case 'waitToReceiveFormToReferee': break;
        }
      case 'formRefereeing':
        if (roundData) {
          roundData = parse_and_stringify(roundData);
        } else if (state.roundData) {
          playerRoundData = parse_and_stringify(playerRoundData);
        }
        switch(action) {
          case 'refereeForm': Object.assign(states_to_set, { roundData, selectedCategories, resultsConfirmed, }); break;
          case 'updatePlayerRoundData':
            roundData = state.roundData;
            roundData[playerRefereed] = playerRoundData;
            Object.assign(states_to_set, { roundData, });
            break;
          case 'waitToReceiveFormToReferee': break;
          case 'notifyPlayerConfirmedResults':
            Object.assign(states_to_set, { resultsConfirmed, }) // todo convert these to ...states_to_set
            break;
        }
        break;
      case 'gameResultShowing': if (action === 'showTotal') Object.assign(states_to_set, { totalResult, }); break;
      case 'notInFlow':
        let message = {playerId:state.playerId, gameId, roundNumber:0, step:'gameAvailable', action:'giveSnapshot',};
        sendMessage('sendSnapshot', message);
    }
    return states_to_set;
  }

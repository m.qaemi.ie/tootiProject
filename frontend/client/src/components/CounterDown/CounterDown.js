import React from 'react';

import './CounterDown.css';

// eslint-disable-next-line react/prop-types
const CounterDown = ({ startNumber }) => {
  const [counter, setCounter] = React.useState(startNumber);
  React.useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    // const interval = counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
    if (counter > 0) {
      var interval = setTimeout(() => setCounter(counter - 1), 1000);
    }
    return () => {
      clearInterval(interval);
    };
  }, [counter]);

  return (
    <div className="CounterDownContainer">
      <div className="CounterDownContainer">Countdown: {counter}</div>
    </div>
  );
};

export default CounterDown;

const zmq = require('zeromq');
const seedrandom = require('seedrandom');
const merge = require('lodash.merge');

var io = require('socket.io-emitter')({ host: process.env.IP_ADDRESS_OF_REDIS_SERVER, port: process.env.PORT_OF_REDIS_SERVER });
const { KVMsg, kvmap } = require('./kvmsg');

const rng = seedrandom('added entropy.', { entropy: true });
var sequence = 0;

const prepare_dealer_socket = (ip_address, port)=>{  //todo make a function for prepare socket
    let dealer = zmq.socket('dealer');
    dealer.linger = 0;
    dealer.connect(`tcp://${ip_address}:${port}`);
    return dealer;
};

const prepare_subscriber_socket = (ip_address, port, subtree)=>{  //todo make a function for prepare socket
    let subscriber = zmq.socket('sub');
    subscriber.linger = 0;
    subscriber.connect(`tcp://${ip_address}:${port}`);
    subscriber.subscribe(subtree);
    return subscriber;
};

const prepare_pusher_socket = (ip_address, port)=>{
    let pusher = zmq.socket('push');
    pusher.linger = 0;
    pusher.connect(`tcp://${ip_address}:${port}`);
    console.log(`successfully connected to tcp://${ip_address}:${port}`);
    return pusher;
};

// push or send message to python server
const send_message_to_python_server = (socket, subtree, player_id, message) => {
    let SUBTREE = subtree;
    let kvmsg = new KVMsg();
    let rndm_4dig_number = Math.trunc(rng.quick() * (9999 - 1000) + 1000);
    kvmsg.key = SUBTREE + `${rndm_4dig_number}`;
    kvmsg.body = JSON.stringify({"state_json": message});
    let rndm_btwn_0to30_number = Math.trunc(rng.quick() * (30 - 0) + 0);
    kvmsg.set_items('ttl', `${rndm_btwn_0to30_number}`);

    // socket.send(["ICANHAZ?", SUBTREE, player_id, message]);
    kvmsg.send(socket);
    kvmsg.store(kvmap);
    console.log(`player id:${player_id}, sent request=${sequence} key=${kvmsg.key} body=${kvmsg.body} uuid=${kvmsg.uuid}`);
};


module.exports = {
    prepare_dealer_socket,
    prepare_subscriber_socket,
    prepare_pusher_socket,
    send_message_to_python_server,
};

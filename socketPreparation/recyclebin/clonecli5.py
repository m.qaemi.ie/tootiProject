"""
Clone client Model Five

Author: Min RK <benjaminrk@gmail.com
"""

import random
import struct
import time

import zmq

import sys
import os

# Create a hi module in your home directory.
home_dir = os.path.expanduser("~")
my_module_file = os.path.join(home_dir, "kvmsg.py")
my_module_file = os.path.join(home_dir,  "zhelpers.py")

# Add the home directory to sys.path
sys.path.append(home_dir)

# from socketPreparation.kvmsg import KVMsg
# from socketPreparation import kvmsg
# from ./kvmsg import KVMsg
# import kvmsg
from kvmsg import KVMsg

SUBTREE = "/client/"


def main():

    # Prepare our context and subscriber
    ctx = zmq.Context()
    snapshot = ctx.socket(zmq.DEALER)
    snapshot.linger = 0
    snapshot.connect("tcp://localhost:5556")
    subscriber = ctx.socket(zmq.SUB)
    subscriber.linger = 0
    subscriber.setsockopt(zmq.SUBSCRIBE, SUBTREE.encode())
    subscriber.connect("tcp://localhost:5557")
    publisher = ctx.socket(zmq.PUSH)
    publisher.linger = 0
    publisher.connect("tcp://localhost:5558")

    random.seed(time.time())
    kvmap = {}

    # Get state snapshot
    sequence = 0
    snapshot.send_multipart([b"ICANHAZ?", SUBTREE.encode()])
    while True:
        try:
            kvmsg = KVMsg.recv(snapshot)
        except:
            raise
            return          # Interrupted

        if kvmsg.key == b"KTHXBAI":
            sequence = kvmsg.sequence
            print("I: Received snapshot=%d" % sequence)
            break          # Done
        kvmsg.store(kvmap)

    poller = zmq.Poller()
    poller.register(subscriber, zmq.POLLIN)

    alarm = time.time()+1.
    while True:
        tickless = 1000*max(0, alarm - time.time())
        try:
            items = dict(poller.poll(tickless))
        except:
            break           # Interrupted

        if subscriber in items:
            kvmsg = KVMsg.recv(subscriber)

            # Discard out-of-sequence kvmsgs, incl. heartbeats
            if kvmsg.sequence > sequence:
                sequence = kvmsg.sequence
                kvmsg.store(kvmap)
                action = "update" if kvmsg.body else "delete"
                print("I: received %s=%d" % (action, sequence))

        # If we timed-out, generate a random kvmsg
        if time.time() >= alarm:
            kvmsg = KVMsg(0)
            kvmsg.key = SUBTREE.encode() + b"%d" % random.randint(1, 10000)
            # kvmsg.key = (SUBTREE + "%d" % random.randint(1, 10000)).encode()
            kvmsg.body = b"%d" % random.randint(1, 1000000)
            # kvmsg.body = ("%d" % random.randint(1, 1000000)).encode()
            # kvmsg[b'ttl'] = random.randint(0, 30)
            kvmsg[b'ttl'] = struct.pack('!f', random.randint(0, 30))
            # kvmsg[b'ttl'] = struct.pack('!f', 15)
            kvmsg.send(publisher)
            kvmsg.store(kvmap)
            alarm = time.time() + 1.

    print(" Interrupted\n%d messages in" % sequence)


if __name__ == '__main__':
     main()

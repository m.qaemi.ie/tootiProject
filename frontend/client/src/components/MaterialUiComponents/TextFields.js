import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import { makeStyles } from '@material-ui/core/styles';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     '& .MuiTextField-root': {
//       margin: theme.spacing(1),
//       width: '25ch',
//     },
//   },
// }));

export default function TextFields({field, setInputsValues}) {
  // const classes = useStyles();
  const [inputValue, setInputValue] = useState('');
  const handleChange = (event) => {
    event.persist()
    // setSelectedValue(event.target.value);
    setInputValue(event.target.value);
    // console.log(event.target);
    setInputsValues(inputsValues => ({ ...inputsValues, [event.target.name]: event.target.value }));
  };
  // useEffect(()=>{
  //   setInputsValues(inputsValues => ({ ...inputsValues, [field.name]: '' }));
  // },[]);

  // const MadeComponents = fields.map((field, index) => {
  //   return (<FormControlLabel
  //     value={field.value}
  //     name={field.name}
  //     control={        <TextField
  //       // id="standard-number"
  //       name={field.name}
  //       label={field.label}
  //       onChange={handleChange}
  //       type="number"
  //       InputLabelProps={{
  //         shrink: true,
  //         }}
  //       />
  //     }
  //     label={field.label}
  //     labelPlacement="top"
  //     key={index}
  //     />);
  // });

  return (
    // <form className={classes.root} noValidate autoComplete="off">
      <div>
        {/* {MadeComponents} */}
        <TextField
          // id="standard-number"
          value={inputValue}
          name={field.name}
          label={field.label}
          onChange={handleChange}
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
        />
      </div>
    // </form>
  );
}

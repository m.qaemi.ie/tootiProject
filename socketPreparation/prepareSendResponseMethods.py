import time
from socketPreparation.classicEFUtilities import classicef_game_steps_props
from socketPreparation.pyredis import prepared_messages_db, get_name_bytes, set_name_bytes, game_data_db, rounds_data_db


def get_next_stage_props(logic_obj):
    """"""
    duration = classicef_game_steps_props[logic_obj.step]['duration']
    next_stage_props = {
        'step': logic_obj.step,
        'roundNumber': logic_obj.round_number,
        'stepEndTime': int(time.time()) + duration,
        'lastRound': logic_obj.game_data['configOfGame']['roundsCount']
    }
    return next_stage_props


def prepare_state_json(logic_obj):
    """make state json related to this step and action and update state_json"""
    # todo change get next step props to give action.
    # game_start_time = int(time.time()) + game_steps_props[next_step_props['nextStep']]['duration']
    state_json_update = {
        # 'gameId': logic_obj.gameId,
        'gameName': logic_obj.game_data['gameName'],  # todo game name must be added.
        'players': logic_obj.game_data['gamePlayers'],
        'playersData': logic_obj.game_data['playersData'],
    }
    next_stage_props_update = get_next_stage_props(logic_obj)
    state_json_update.update(next_stage_props_update)
    update_dict = logic_obj.get_action_state_json_update_dict()  # can this be moved to classicEFLogic.py
    update_related_to_action = update_dict[logic_obj.action]
    state_json_update.update(update_related_to_action)
    logic_obj.state_json.update(state_json_update)


def prepare_snapshot_state_json(logic_obj):
    """get snapshot from redis"""
    server_step = logic_obj.game_data['serverStage']
    step, action, round_number = server_step
    redis_name = logic_obj.game_id + ''.join(server_step)
    if [step, action] in (['referee', 'refereeForm'], ['referee', 'notifyPlayersSentForm'], ['roundResult', 'showResult']):
        players_sent_form = logic_obj.game_data['rounds'][round_number]['playersSentForm']
        if logic_obj.player_id in players_sent_form:
            redis_name += logic_obj.player_id
        else:
            logic_obj.step = 'referee'
            logic_obj.action = 'notifyPlayersSentForm'
            redis_name = logic_obj.game_id + 'referee' + 'notifyPlayersSentForm' + round_number
    logic_obj.state_json = get_name_bytes(prepared_messages_db, redis_name)
    logic_obj.state_json.update({'playerId': logic_obj.player_id, })


def change_server_side_step(logic_obj, server_side_step_to_change_to):
    """change server side step of game in game data"""
    logic_obj.game_data['serverStage'] = server_side_step_to_change_to


def prepare_send_store_msg(logic_obj):
    name_in_redis = logic_obj.game_id + ''.join(logic_obj.game_data['serverStage'])
    prepare_state_json(logic_obj)
    logic_obj.send_snapshot_kvmsg(logic_obj.state_json, logic_obj.step, logic_obj.kvmsg_received, logic_obj.dealer_socket, logic_obj.identity_of_requester)
    set_name_bytes(game_data_db, logic_obj.game_id, logic_obj.game_data)
    set_name_bytes(rounds_data_db, logic_obj.game_id + logic_obj.player_id, logic_obj.rounds_data)
    set_name_bytes(prepared_messages_db, name_in_redis, logic_obj.state_json)  # todo ttl


def prepare_publish_store_msg(logic_obj):
    name_in_redis = logic_obj.game_id + ''.join(logic_obj.game_data['serverStage'])
    prepare_state_json(logic_obj)
    logic_obj.publish_kvmsg(logic_obj.state_json, logic_obj.step, logic_obj.kvmsg_received)
    set_name_bytes(game_data_db, logic_obj.game_id, logic_obj.game_data)
    # set_name_bytes(rounds_data_db, logic_obj.game_id + logic_obj.player_id, logic_obj.rounds_data)
    set_name_bytes(prepared_messages_db, name_in_redis, logic_obj.state_json)  # todo ttl


# def publish_notification_msg(logic_obj):
#     """prepare and publish notification msg without storing them."""
#     prepare_state_json(logic_obj)
#     logic_obj.publish_kvmsg(logic_obj.state_json, logic_obj.step, logic_obj.kvmsg_received)

# todo add transaction pipline.
# todo check user is in flow of game with decorator.
# todo check if we can flatten game_data in future
# todo move action to get_next_step_props function.
# todo change playerId in messages to player and include features of player in it.
# todo add some logic to django to change and get player name and player id.
# todo and publish a message to show joining a player to game.
# todo use msgpack instead of json.dumps or struct.dumps loads and convert,etc. or use msgpack in redis.
# todo check in which one you can use python cache
# todo for security game id must be generated server side and every requst do not have valid game id will be banned. not possible every request because of overhead of chek in redis. instead check in some stages in each game!
# todo add sending a message to client so that client can know this gameId is not available or is incorrect and return in this point.
# todo check player_id, if player_id is not unique send error message. this is not necessary for mobile. because we have id of installation.
# todo we want a system to assure all players got last update of game. if not request snapshot. this is other than stages we want a number to control this.
# todo logging with decorator.
# todo try exception with decorator.
# todo seperate this function from clonesrv.
# todo confirm result in last round ....
from operator import itemgetter
# todo messeges that return not store in kvmss...?
from tornado import ioloop
from socketPreparation.classicEFUtilities import classicef_action_related_update, make_classicef_player_round_data, classicef_game_steps_props
from socketPreparation.pyredis import game_data_db, get_name_bytes, rounds_data_db, string_set_scan, sorted_set_remove
from socketPreparation.stepsManagementMethods import go_to_next_round, go_to_give_total, player_is_in_flow_of_game, \
    go_to_next_stage_if_necessary, confirm_results, push_game_to_next_stage
from socketPreparation.gameManagementMethods import store_round_data, calculate_store_total_result, \
    make_game_if_has_valid_id, join_player_to_game_if_not_joined, close_game
from socketPreparation.formProcessingMethods import store_form, change_judgment_about_word, publish_forms_completed
from socketPreparation.prepareSendResponseMethods import prepare_snapshot_state_json, prepare_send_store_msg, prepare_publish_store_msg


class ClassicEFLogic(object):  # todo change state_json to request json and pass it to an object or ... .
    get_action_state_json_update_dict = classicef_action_related_update
    make_player_round_data = make_classicef_player_round_data
    game_steps_props = classicef_game_steps_props
    # get_next_step_props = classicef_get_next_step_props

    def __init__(self, state_json, kvmsg_received, identity_of_requester, dealer_socket, clone_server):
        # assign instance attribute
        self.publish_kvmsg = clone_server.publish_kvmsg
        self.send_snapshot_kvmsg = clone_server.send_snapshot_kvmsg
        self.kvmsg_received = kvmsg_received
        self.identity_of_requester = identity_of_requester
        self.dealer_socket = dealer_socket
        # extract state_json
        self.player_id, self.game_id, self.step, self.action, self.round_number,\
            = itemgetter('playerId', 'gameId', 'step', 'action', 'roundNumber',)(state_json)
        self.state_json = state_json
        # json.loads converts key of int to string.
        self.round_number = str(self.round_number)
        self.active_games = []
        # self.next_stage_props = get_next_stage_props(self)
        self.last_round = ''
        self.form_template = {}
        self.form_completed = {}
        # self.round_forms_received = {}
        self.round_data = {}
        self.player_round_data = {}
        self.players_participated_in_round = []
        self.player_refereed = ''
        # get data of game from redis db
        self.game_data = get_name_bytes(game_data_db, self.game_id)
        self.rounds_data = get_name_bytes(rounds_data_db, self.game_id + self.player_id)

        self.player_is_in_flow = True

    def reply_player(self):
        """update user related data this includes add to a game, reply to requests that not related to other players"""
        if self.step == 'selectGameToJoin':  # todo move this to django or another class
            if self.action == 'giveActiveGames':
                cursor, games_list = string_set_scan(game_data_db, 'activeGames', cursor=self.state_json['cursor'], count=10)
                self.state_json['cursorOfActiveGamesList'] = cursor
                self.state_json['activeGamesList'] = games_list
                self.send_snapshot_kvmsg(self.state_json, self.step, self.kvmsg_received, self.dealer_socket, self.identity_of_requester)
                return
        elif self.step == 'beforeStart':
            if self.action == 'makeGame':
                nr_of_removed_valid_id = sorted_set_remove(game_data_db, 'validIds', self.game_id)
                if nr_of_removed_valid_id == 0:
                    return  # todo implement sending an error said this game id not valid and make a game again.
                make_game_if_has_valid_id(self)
                join_player_to_game_if_not_joined(self)
                prepare_send_store_msg(self)
                return
        elif self.step == 'gameAvailable':  # todo add me to game is give snapshot system can you delete this?
            if self.action == 'giveSnapshot':
                prepare_snapshot_state_json(self)
                self.send_snapshot_kvmsg(self.state_json, self.step, self.kvmsg_received, self.dealer_socket, self.identity_of_requester)

    def update_game(self):  # todo change this to dict
        """update game related data"""
        self.player_is_in_flow = player_is_in_flow_of_game(self) # todo move this to init function?
        if not self.player_is_in_flow:
            # send reply kvmsg
            self.send_snapshot_kvmsg(self.state_json, self.step, self.kvmsg_received, self.dealer_socket, self.identity_of_requester)
            return
        go_to_next_stage_if_necessary(self)
        if self.action == 'joinMeToGame':
            join_player_to_game_if_not_joined(self)
        elif self.action == 'startRound':  # todo change it to startGame? or make it dynamic for all rounds not just round one.
            if self.player_id != self.game_data['creator']:
                return  # todo implement sending an error and said only creator can start game.
            store_round_data(self)
        elif self.action == 'stopRound':
            delay_to_send_forms = 4
            ioloop.IOLoop.current().call_later(delay=delay_to_send_forms, callback=publish_forms_completed, logic_obj=self)
            store_round_data(self)
            delay_to_change_step = 60
            if self.round_number != str(self.game_data['configOfGame']['roundsCount']):
                ioloop.IOLoop.current().call_later(delay=delay_to_change_step, callback=go_to_next_round, logic_obj=self)
            else:
                ioloop.IOLoop.current().call_later(delay=delay_to_change_step, callback=go_to_give_total, logic_obj=self)
        elif self.action == 'receiveForm':
            store_form(self)
        elif self.action == 'receiveReferee':  # receive judgement about a word and update player round data.
            self.player_refereed = self.state_json['playerRefereed']
            change_judgment_about_word(self)
        elif self.action == 'confirmResults': # todo implement sending confirmed result to disable button not client side disabling.
            all_players_confirmed_results = confirm_results(self)
            if all_players_confirmed_results:
                if self.round_number != str(self.game_data['configOfGame']['roundsCount']):
                    self.action = 'startRound'
                    push_game_to_next_stage(self)
                else:
                    go_to_give_total(self)
                    delay_to_close_game = 5
                    ioloop.IOLoop.current().call_later(delay=delay_to_close_game, callback=close_game, logic_obj=self)
            else:
                prepare_send_store_msg(self)
                return
        elif self.action == 'giveTotal':
            if 'totalResult' in self.game_data:  # if totalResult was calculated it should not be calculated again.
                self.state_json['totalResult'] = self.game_data['totalResult']
            else:
                calculate_store_total_result(self)
        prepare_publish_store_msg(self)  # todo change this to dealer and ack we can send this message with dealer but publish this to say to other users that this user had sent form of this round.
        return

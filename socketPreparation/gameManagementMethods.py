import json
from datetime import datetime
from socketPreparation.formProcessingMethods import prepare_rounds_form_templates, prepare_rounds_forms_of_user
from socketPreparation.generalUtilities import convert
from socketPreparation.pyredis import sadd_name_values, hset_name_dict, player_data_db, rounds_data_db, \
    hgetall_name_dict, game_data_db, list_remove, sorted_set_add, sorted_set_remove


def add_player_to_game_players(logic_obj):
    """add player to players of game in game data"""
    logic_obj.game_data['gamePlayers'].append(logic_obj.player_id)
    player_name = logic_obj.state_json['playerName']
    logic_obj.game_data['playersData'][logic_obj.player_id] = {'playerName': player_name}


def add_game_to_games_of_player(logic_obj):
    """add game to games of player in redis db"""
    sadd_name_values(player_data_db, 'player:' + logic_obj.player_id + 'games', logic_obj.game_id)


def make_game_data_dict(logic_obj):
    """make dict for storing game data and store it in game data"""
    game_name = logic_obj.state_json['gameName']
    config_of_game = logic_obj.state_json['configOfGame']
    logic_obj.game_data = {'gameId': logic_obj.game_id, 'gameName': game_name, 'creator': logic_obj.player_id, 'gamePlayers': [], 'playersData': {}, 'selectedLetters': [], 'rounds': {}, 'serverStage': {'roundNumber': '1', 'step': 'beforeStart', }, 'configOfGame': config_of_game}


def prepare_game_data(logic_obj):
    """"""
    make_game_data_dict(logic_obj)
    prepare_rounds_form_templates(logic_obj)  # todo cache will decrease time of prepare round data.


def make_game_if_has_valid_id(logic_obj):
    """add game id to active games set. if not added yet prepare game data."""
    now_timestamp = datetime.timestamp(datetime.now())
    nr_of_added_game = sorted_set_add(game_data_db, 'activeGames', {logic_obj.game_id: now_timestamp})
    if nr_of_added_game != 0:  # if not equals 0 means this game had not been made before
        prepare_game_data(logic_obj)
        # todo store player id as creator of game somewhere in redis.


def join_player_to_game_if_not_joined(logic_obj):
    """"""
    nr_of_added_player = sadd_name_values(game_data_db, 'gameId:' + logic_obj.game_id + 'players',
                                          logic_obj.player_id)  # add player to players of game
    if nr_of_added_player != 0:  # if not equals 0 means this player had not been added to game before
        add_game_to_games_of_player(logic_obj)
        add_player_to_game_players(logic_obj)
        prepare_rounds_forms_of_user(logic_obj, logic_obj.player_id)


def store_round_data(logic_obj): # todo store next round data not store round data.
    """store players joined before until this round and categories of round to roundData in round data db"""
    name = 'gameId:' + logic_obj.game_id + 'roundNumber:' + logic_obj.round_number + 'roundData'
    hset_name_dict(rounds_data_db, name, {'playersConfirmedResults': json.dumps([]).encode('utf-8'), 'playersParticipatedInRound': json.dumps(logic_obj.game_data['gamePlayers']).encode('utf-8'), })


def close_game(logic_obj):
    """deletes game from active games of this player, and from active games, in other word closes this game for this
    player"""
    sorted_set_remove(game_data_db, 'activeGames', logic_obj.game_id)
    for playerId in logic_obj.game_data['gamePlayers']:
        list_remove(player_data_db, playerId, 0, logic_obj.game_id)


def get_player_round_form(logic_obj, round_num, player_id):
    """"""
    name = 'gameId:' + logic_obj.game_id + 'roundNumber:' + round_num + 'playerId:' + player_id + 'playerRoundData'
    player_round_form = convert(hgetall_name_dict(rounds_data_db, name))
    return player_round_form


def correct_form(player_round_form, number_of_players):
    """"""
    number_of_corrects = 0
    number_of_incorrects = 0
    number_of_not_filled = 0
    # move this calculation after each round to decrease use of resources
    for key in player_round_form.keys():
        player_word_judgments = json.loads(
            player_round_form[key]).values()  # response of player for a category and players judgments.
        if None in player_word_judgments:
            number_of_not_filled += 1
            continue
        else:
            number_of_corrects_judgments = sum(x is True for x in player_word_judgments)
            number_of_corrects += number_of_corrects_judgments
            number_of_incorrects += (number_of_players - number_of_corrects_judgments)
    return number_of_corrects, number_of_incorrects, number_of_not_filled


def calculate_store_total_result(logic_obj): # todo check you can use pandas or numpy for this?
    """calculate total result for each user"""
    name_round_data = 'gameId:' + logic_obj.game_id + 'roundNumber:' + logic_obj.round_number + 'roundData'
    logic_obj.round_data = convert(hgetall_name_dict(rounds_data_db, name_round_data))
    game_players = json.loads(logic_obj.round_data['playersParticipatedInRound'])
    number_of_players = len(game_players)
    logic_obj.game_data['totalResult'] = []
    for playerId in game_players:
        number_of_corrects = 0
        number_of_incorrects = 0
        number_of_not_filled = 0
        # calculate total result of player
        for roundNum in range(1, int(logic_obj.game_data['configOfGame']['roundsCount']) + 1):  # todo move game steps props to ef logic , ...
            # todo not dynamic. and calculate this with pandas.
            roundNum = str(roundNum)
            player_round_form = get_player_round_form(logic_obj, roundNum, playerId)
            form_number_of_corrects, form_number_of_incorrects, form_number_of_not_filled = correct_form(player_round_form, number_of_players)
            number_of_corrects += form_number_of_corrects
            number_of_incorrects += form_number_of_incorrects
            number_of_not_filled += form_number_of_not_filled

        logic_obj.game_data['totalResult'].append(
            {'playerId': playerId, 'nr_c': number_of_corrects, 'nr_inc': number_of_incorrects,
             'nr_nf': number_of_not_filled})
    # save total result of player
    logic_obj.state_json['totalResult'] = logic_obj.game_data['totalResult']

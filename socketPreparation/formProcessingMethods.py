from socketPreparation.classicEFUtilities import classic_game_fields, get_form_of_round
import json
from socketPreparation.generalUtilities import convert
from socketPreparation.prepareSendResponseMethods import prepare_publish_store_msg
from socketPreparation.pyredis import hset_name_dict, rounds_data_db, hgetall_name_dict


def prepare_rounds_form_templates(logic_obj):
    """make forms for all rounds and store in game data """
    random_selected_letters = []
    number_of_rounds = int(logic_obj.game_data['configOfGame']['roundsCount'])
    for round_to_make_data_of in range(1, number_of_rounds + 1):
        round_to_make_data_of = str(round_to_make_data_of)
        # make form for this user and round
        letter, form = get_form_of_round(random_selected_letters, logic_obj.game_data['configOfGame']['selectedAlphabets'], logic_obj.game_data['configOfGame']['selectedCategories'], )
        # add letter to letters selected
        random_selected_letters.append(letter)
        # make dict for storing round data
        logic_obj.game_data['rounds'][round_to_make_data_of] = {'formTemplate': form, 'playersForms': {}, 'playersSentForm': [], }


def get_form_template_of_round(logic_obj, round_number):
    """get form template of a specific round"""
    return logic_obj.game_data['rounds'][round_number]['formTemplate']


def prepare_rounds_forms_of_user(logic_obj, player_id):
    """add forms of a specific user for all rounds to rounds_data"""
    logic_obj.rounds_data = logic_obj.game_data['rounds']
    number_of_rounds = int(logic_obj.game_data['configOfGame']['roundsCount'])
    for roundNum in range(1, number_of_rounds + 1):
        roundNum = str(roundNum)
        form = get_form_template_of_round(logic_obj, roundNum)
        logic_obj.rounds_data[roundNum]['playersForms'][player_id] = {'form': form, } # todo add other elements. is this necessary.


def store_form(logic_obj):
    """get form from state_json and set game players initial referee and store it in rounds data db"""
    logic_obj.form_completed = logic_obj.state_json['rounds'][logic_obj.round_number]['playersForms'][logic_obj.player_id]['form']
    logic_obj.make_player_round_data()
    name = 'gameId:' + logic_obj.game_id + 'roundNumber:' + logic_obj.round_number + 'playerId:' + logic_obj.player_id + 'playerRoundData'
    hset_name_dict(rounds_data_db, name, logic_obj.player_round_data)


def change_judgment_about_word(logic_obj):
    """get judgment from state_json and change judgment of player about a word and store it in rounds data db"""
    judgment_about_word = logic_obj.state_json['judgmentAboutWord']
    category = logic_obj.state_json['category']
    name = 'gameId:' + logic_obj.game_id + 'roundNumber:' + logic_obj.round_number + 'playerId:' + logic_obj.player_refereed + 'playerRoundData'
    logic_obj.player_round_data = convert(hgetall_name_dict(rounds_data_db, name))
    category_data = json.loads(logic_obj.player_round_data[category])
    category_data.update({logic_obj.player_id: judgment_about_word})
    logic_obj.player_round_data.update({category: json.dumps(category_data)})
    hset_name_dict(rounds_data_db, name, logic_obj.player_round_data)


def get_forms_completed(logic_obj):
    """gets forms completed for round from redis round data db and stores them in logic_obj.round_data"""
    name_round_data = 'gameId:' + logic_obj.game_id + 'roundNumber:' + logic_obj.round_number + 'roundData'
    logic_obj.round_data = convert(hgetall_name_dict(rounds_data_db, name_round_data))
    players_participated_in_round = json.loads(logic_obj.round_data['playersParticipatedInRound'])
    for playerId in players_participated_in_round:
        # get player round data from redis
        name = 'gameId:' + logic_obj.game_id + 'roundNumber:' + logic_obj.round_number + 'playerId:' + playerId + 'playerRoundData'
        player_round_data = convert(hgetall_name_dict(rounds_data_db, name))
        logic_obj.round_data.update({playerId: player_round_data})


def publish_forms_completed(logic_obj):
    """publish forms completed and categories and players participated in round to all players"""
    get_forms_completed(logic_obj)
    # todo automate this to go to next step with a function go to next step.
    logic_obj.step = 'formRefereeing'  # for prepare_state_json method step and action should be changed.
    logic_obj.game_data['serverStage']['step'] = logic_obj.step
    logic_obj.action = 'refereeForm'
    prepare_publish_store_msg(logic_obj)  # do not receive form anymore




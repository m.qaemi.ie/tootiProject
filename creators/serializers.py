from django.db.models import Q
from django.db import IntegrityError
from rest_framework import serializers
# from creators.models import Game

from django.contrib.auth import get_user_model
from rest_framework.exceptions import ValidationError

from creators.models import Game, Round, Player, Referee, FormTemplate
from interactions.models import DeviceInstalledApp, PlayerFillsForm

ProjectUser = get_user_model()


class ValidIdSerializer(serializers.Serializer):
    valid_id = serializers.CharField()


class DeviceInstalledAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceInstalledApp
        fields = '__all__'

    def create(self, validated_data):
        # user = []
        # device_id = validated_data['device_id']
        # obj = DeviceInstalledApp.objects.create(device_id=device_id)
        obj = DeviceInstalledApp.objects.create(**validated_data)
        return obj


'''
class DeviceSerializer(serializers.ModelSerializer):
    # device_id = serializers.CharField(required=True)
    class Meta:
        model = ProjectUser
        fields = ['devices']

    def create(self, validated_data):
        # device_id = validated_data['devices'][0]
        device_id = validated_data['device_id']
        validated_data['email'] = 'device_user_{}@mail.com'.format(device_id)
        # validated_data['devices'] = {"device_ids": [validated_data['devices']]}
        # try:
            # device = Device.objects.get(device_id=device_id, is_active=True)
            # device_user = ProjectUser.device_set.get(device_id=device)  # improve this. how many times does db hit?
            # return device_user
        # except (Device.DoesNotExist, ProjectUser.DoesNotExist):
        #     device_user = ProjectUser.objects.create(**validated_data)
            # device = Device.objects.create(device_id=device_id, user=device_user)
            # return device_user
        # try:
        #     device_user = ProjectUser.objects.get(Device=device_id)
        # except IntegrityError:
        #     raise ValidationError()
        # try:
        #     device_user = ProjectUser.objects.create_device_user(**validated_data)
        # except IntegrityError:
        #     raise ValidationError()
        # return device_user
'''


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = '__all__'

    def create(self, validated_data):
        player = Player.objects.get_or_create(**validated_data)
        return player[0]


class RoundSerializer(serializers.ModelSerializer):

    class Meta:
        model = Round
        fields = '__all__'

    def create(self, validated_data):
        # device_id = validated_data['device_id']
        round = Round.objects.get_or_create(**validated_data)
        return round[0]


    # def create(self, validated_data):  # todo wrap in a transaction.
    #     player_validated_data = validated_data.pop('players')
    #     round = Round.objects.create(**validated_data)
    #     players_serializer = self.fields['players']
    #     # for each in player_validated_data:
    #     #     each['round'] = round
    #     players = players_serializer.create(player_validated_data)
    #     return round


class GameSerializer(serializers.ModelSerializer):
    round_set = RoundSerializer(many=True)

    class Meta:
        model = Game
        fields = ['device_id', 'gameEnvironment', 'creator', 'typeOfReferee', 'round_set']

    def create(self, validated_data):  # todo wrap in a transaction.
        round_validated_data = validated_data.pop('round_set')
        game = Game.objects.create(**validated_data)
        round_set_serializer = self.fields['round_set']
        for each in round_validated_data:
            each['game'] = game
        rounds = round_set_serializer.create(round_validated_data)
        return game


class FormTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormTemplate
        fields = '__all__'

    # def create(self, validated_data):
    #     formTemplate = FormTemplate.objects.get_or_create(**validated_data)
    #     return form


class RefereeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Referee
        fields = '__all__'

    def create(self, validated_data):
        referee = Referee.objects.get_or_create(**validated_data)
        return referee[0]


class PlayerFillsFormSerializer(serializers.ModelSerializer):
    round = RoundSerializer()
    player = PlayerSerializer()
    formTemplate = FormTemplateSerializer(required=False)
    referee = RefereeSerializer()

    class Meta:
        model = PlayerFillsForm
        # fields = '__all__'
        fields = ['round', 'player', 'formTemplate', 'form', 'referee']

         # = models.ForeignKey(Round, blank=True, null=True, on_delete=models.DO_NOTHING)
         # = models.ForeignKey(Player, blank=False, null=False, on_delete=models.DO_NOTHING)
         # = models.ForeignKey(FormFields, blank=True, null=True, on_delete=models.DO_NOTHING)
         # = JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
         # = models.ForeignKey(Referee, blank=False, null=False, on_delete=models.DO_NOTHING)

    def create(self, validated_data):  # todo wrap in a transaction.
        round_validated_data = validated_data.pop('round')
        round_serializer = self.fields['round']
        round = round_serializer.create(round_validated_data)

        player_validated_data = validated_data.pop('player')
        player_serializer = self.fields['player']
        player = player_serializer.create(player_validated_data)

        # referee_validated_data = validated_data.pop('referee')
        # referee_serializer = self.fields['referee']
        # referee = referee_serializer.create(referee_validated_data)

        # formTemplate_validated_data = validated_data.pop('formTemplate') todo

        playerFillsForm = PlayerFillsForm.objects.create(device_id=player.device_id, round=round, player=player)
        # for each in round_validated_data:
        #     each['game'] = game
        return playerFillsForm

        # round_validated_data = validated_data.pop('round_set')
        # game = Game.objects.create(**validated_data)
        # round_set_serializer = self.fields['round_set']
        # for each in round_validated_data:
        #     each['game'] = game
        # rounds = round_set_serializer.create(round_validated_data)
        # return game

        # # device_id = validated_data['device_id']
        # game_created = Game.objects.create(**validated_data)
        # return game_created


'''
class AnonymousUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectUser
        fields = '__all__'

    def create(self, validated_data):
        return ProjectUser.objects.create(**validated_data)


class GameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Game
        fields = '__all__'

    def create(self, validated_data):
        return Game.objects.create(**validated_data)

'''
'''

class ProviderCreatorSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectUser
        fields = '__all__'

    def create(self, validated_data):
        # todo send sms to mobile. if match continue. needs async with celery.
        return ProjectUser.objects.create_provider_creator(**validated_data)


class ProviderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Service
        fields = '__all__'

    def create(self, validated_data):
        provider = Provider(name=validated_data['name'])
        provider.save()
        validated_data['provider'] = provider
        return Service.objects.create(**validated_data)

'''
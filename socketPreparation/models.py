# """
# Clone server Model Five
#
# Author: Min RK <benjaminrk@gmail.com
# """
# import asyncio
# import concurrent.futures
# import os
# import pathlib
# import uuid
# import sys
# import threading
# # print(sys.path)
# from pathlib import Path
# # from esmFamilProject import settings
# # path_to_current_directory = Path(os.getcwd())
# # print(path_to_current_directory)
# # print(str(path_to_current_directory.parent)+'\esmFamilProject\settings.py')
# # path_to_settings = str(path_to_current_directory.parent)+'\esmFamilProject'
# # path_to_settings = os.path.join(path_to_current_directory.parent, 'esmFamilProject')
# # print(path_to_settings)
# # home_dir = os.path.expanduser("~")
# # from esmFamilProject import settings
#
# # home_dir = path
# # print(home_dir)
# # my_module_file = os.path.join(home_dir, "kvmsg.py")
# # my_module_file = os.path.join(home_dir,  "zhelpers.py")
# # print(my_module_file)
# # Add the home directory to sys.path
# # sys.path.append(home_dir)
# # sys.path.insert(0, os.getcwd())
# # sys.path.append('C:\\Users\\Home\\OneDrive\\EsmfamilProject980919\\socketPreparation')
# # sys.path.append('C:\\Users\\Home\\OneDrive\\EsmfamilProject980919\\esmFamilProject\\settings.py')
# # sys.path.append('C:\\Users\\Home\\OneDrive\\EsmfamilProject980919\\esmFamilProject')
# # sys.path.insert(0, path_to_settings)
# # sys.path.append(path_to_current_directory)
#
# # print(sys.path)
#
# # import django
# # # from django.conf import settings
# #
# # # django project name is adleads, replace adleads with your project name
# # # os.environ.setdefault("DJANGO_SETTINGS_MODULE", "esmFamilProject.settings")
# # os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'esmFamilProject.settings')
# # # os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
# # # django.setup()
# # from django.conf import settings
# # # from myapp import myapp_defaults
# #
# # settings.configure(DEBUG=True)
# # django.setup()
#
# # Now this script or any imported module can use any part of Django it needs.
# # from myapp import models
#
# import logging
# import struct
# import time
# # from struct import calcsize
#
# # import zmq
# import zmq.green as zmq
# from zmq.green.eventloop.zmqstream import ZMQStream
# from gevent import spawn, spawn_later
# # from zmq.eventloop.ioloop import IOLoop, PeriodicCallback
# from tornado.ioloop import IOLoop, PeriodicCallback
# # from zmq.eventloop.zmqstream import ZMQStream
#
# path_to_cwd = Path(os.getcwd())
# # print(path_to_cwd.parent)
# # path_to_cwd = path_to_cwd.parent
#
# sys.path.append(os.path.join(path_to_cwd.parent))
# # sys.path.insert(0, os.path.join(os.getcwd(), 'models.py'))
# # print(sys.path)
# # import Game, Player
# # Create a hi module in your home directory.
# current_directory = pathlib.Path(__file__).parent.absolute()
# sys.path.append(os.path.join(current_directory))
# # print(sys.path)
#
# from creators.models import Game, Player
# # from creators import models
# from kvmsg import KVMsg
# from zhelpers import dump
#
#
# # simple struct for routing information for a key-value snapshot
# class Route:
#     def __init__(self, socket, identity, subtree):
#         self.socket = socket        # ROUTER socket to send to
#         self.identity = identity    # Identity of peer who requested state
#         self.subtree = subtree      # Client subtree specification
#
#
# def send_single(key, kvmsg, route):
#     """Send one state snapshot key-value pair to a socket"""
#     # check front of key against subscription subtree:
#     if kvmsg.key.startswith(route.subtree):
#         # Send identity of recipient first
#         route.socket.send(route.identity, zmq.SNDMORE)
#         kvmsg.send(route.socket)
#
#
# class CloneServer(object):
#
#     # Our server is defined by these properties
#     ctx = None                  # Context wrapper
#     kvmap = None                # Key-value store
#     loop = None                 # IOLoop reactor
#     port = None                 # Main port we're working on
#     sequence = 0                # How many updates we're at
#     snapshot = None             # Handle snapshot requests
#     publisher = None            # Publish updates to clients
#     collector = None            # Collect updates from clients
#
#     def __init__(self, port=5555):
#         self.port = port
#         self.ctx = zmq.Context()
#         self.kvmap = {}
#         self.loop = IOLoop.instance()
#
#         # Set up our clone server sockets
#         self.assign_game = self.ctx.socket(zmq.ROUTER)
#         self.snapshot = self.ctx.socket(zmq.ROUTER)
#         self.publisher = self.ctx.socket(zmq.PUB)
#         self.collector = self.ctx.socket(zmq.PULL)
#         self.assign_game.bind("tcp://*:%d" % self.port)
#         self.snapshot.bind("tcp://*:%d" % (self.port+1))
#         self.publisher.bind("tcp://*:%d" % (self.port + 2))
#         self.collector.bind("tcp://*:%d" % (self.port + 3))
#
#         # Wrap sockets in ZMQStreams for IOLoop handlers
#         self.assign_game = ZMQStream(self.assign_game)
#         self.snapshot = ZMQStream(self.snapshot)
#         self.publisher = ZMQStream(self.publisher)
#         self.collector = ZMQStream(self.collector)
#
#         # Register our handlers with reactor
#         self.assign_game.on_recv(self.handle_assign_game)
#         self.snapshot.on_recv(self.handle_snapshot)
#         self.collector.on_recv(self.handle_collect)
#         self.flush_callback = PeriodicCallback(self.flush_ttl, 1000)
#
#         # basic log formatting:
#         logging.basicConfig(format="%(asctime)s [%(filename)s:%(lineno)d] %(message)s ", datefmt="%Y-%m-%d %H:%M:%S",
#                 level=logging.INFO)
#
#     def start(self):
#         # Run reactor until process interrupted
#         self.flush_callback.start()
#         try:
#             self.loop.start()
#         except KeyboardInterrupt:
#             pass
#
#     def handle_assign_game(self, msg):
#         """assign game requests"""
#         if len(msg) != 4 or msg[1] != b"IWANTTOPLAY?":
#             print("E: bad request, aborting")
#             dump(msg)
#             self.loop.stop()
#             return
#         identity, request, subtree, device_id = msg
#         if subtree:
#             # Send state snapshot to client
#             route = Route(self.assign_game, identity, subtree)
#
#             # For each entry in kvmap, send kvmsg to client
#             for k, v in self.kvmap.items():
#                 send_single(k, v, route)
#
#             # Now send END message with sequence number
#             player = Player.objects.get_or_create(device_id=device_id.decode('utf8'))
#             # it must be checked that a player do not participate in two games simultaneously. if current game lasted for
#             # time that extends max duration of a game. first that game must be finished, second assigned to a game.
#             game = Game.objects.get_or_create(is_full=False)
#             game[0].player.add(player[0])
#             # if attendees of game extends number of players of game, is_full is equal to True.
#             logging.info("I: assign game to player=%d %s", self.sequence, game[0].id)
#             self.assign_game.send(identity, zmq.SNDMORE)
#             kvmsg = KVMsg(self.sequence)
#             kvmsg.key = b"GAMEIDSENT"
#             # kvmsg.body = subtree
#             kvmsg.body = bytes(str(game[0].id), 'utf8')
#             kvmsg.send(self.assign_game)
#
#     def handle_snapshot(self, msg):
#         """snapshot requests"""
#         if len(msg) != 3 or msg[1] != b"ICANHAZ?":
#             print("E: bad request, aborting")
#             dump(msg)
#             self.loop.stop()
#             return
#         identity, request, subtree = msg
#         if subtree:
#             # Send state snapshot to client
#             route = Route(self.snapshot, identity, subtree)
#
#             # For each entry in kvmap, send kvmsg to client
#             for k, v in self.kvmap.items():
#                 send_single(k, v, route)
#
#             # Now send END message with sequence number
#             logging.info("I: Sending state shapshot=%d" % self.sequence)
#             self.snapshot.send(identity, zmq.SNDMORE)
#             kvmsg = KVMsg(self.sequence)
#             kvmsg.key = b"KTHXBAI"
#             kvmsg.body = subtree
#             kvmsg.send(self.snapshot)
#
#     def handle_collect(self, msg):
#         """Collect updates from clients"""
#         kvmsg = KVMsg.from_msg(msg)
#         self.sequence += 1
#         kvmsg.sequence = self.sequence
#         kvmsg.send(self.publisher)
#         try:
#             # logging.info('%b',kvmsg.get(b'ttl', 0))
#             # logging.info('%f',calcsize(kvmsg.get(b'ttl', 0)))
#
#             ttl = struct.unpack('!f', kvmsg.get(b'ttl', 0))[0]
#             # ttl = float(kvmsg.get(b'ttl', 0))
#             # ttl = float(ord(kvmsg.get(b'ttl', 0)))
#             # ttl = int.from_bytes(kvmsg.get(b'ttl', 0), byteorder='big')
#         except ValueError as e:
#             # print("error", e,)
#             logging.error(e)
#         if ttl:
#             # kvmsg[b'ttl'] = b'%f' % (time.time() + ttl)
#             kvmsg[b'ttl'] = b'%f' % (time.time() + ttl)
#         kvmsg.store(self.kvmap)
#         logging.info("I: publishing update=%d", self.sequence)
#
#     def flush_ttl(self):
#         """Purge ephemeral values that have expired"""
#         for key, kvmsg in list(self.kvmap.items()):
#             # used list() to exhaust the iterator before deleting from the dict
#             self.flush_single(kvmsg)
#
#     def flush_single(self, kvmsg):
#         """If key-value pair has expired, delete it and publish the fact
#         to listening clients."""
#         try:
#             ttl = float(kvmsg.get(b'ttl', 0))  # todo can optimize.
#         except:
#             ttl = struct.unpack('!f', kvmsg.get(b'ttl', 0))[0]
#         # ttl = struct.unpack('!d', kvmsg.get(b'ttl', 0))[0]
#
#         # ttl = float(kvmsg.get(b'ttl', 0))
#         # ttl = float(ord(kvmsg.get(b'ttl', 0)))
#         # ttl = int.from_bytes(kvmsg.get(b'ttl', 0), byteorder='big')
#         # print(ttl)
#         # ttl = struct.unpack('=f', kvmsg.get(b'ttl', 0))
#         if ttl and ttl <= time.time():
#             kvmsg.body = b""
#             self.sequence += 1
#             kvmsg.sequence = self.sequence
#             kvmsg.send(self.publisher)
#             del self.kvmap[kvmsg.key]
#             logging.info("I: publishing delete=%d", self.sequence)
#
#
# # def main():
# #     clone = CloneServer()
# #     clone.start()
# def clonesrv():
#     # loop = asyncio.new_event_loop()
#     clone = CloneServer()
#     clone.start()
#
#
# spawn(clonesrv)
#
# # with concurrent.futures.ThreadPoolExecutor() as executor:
# #     t = executor.submit(clonesrv)
#
#
# # t = threading.Thread(target=clonesrv)
# # t.start()
# # t.join()
#
#
# # if __name__ == '__main__':
# #     main()
# #
# # def main():
# #     clone = CloneServer()
# #     clone.start()
# #
# #
# # if __name__ == '__main__':
# #     main()

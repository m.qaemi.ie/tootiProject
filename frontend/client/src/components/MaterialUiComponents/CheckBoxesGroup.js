import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';

const convertArrayToObject = (array, key) => {
    const initialValue = {};
    return array.reduce((obj, item) => {
      return {
        ...obj,
        [item[key]]: item,
      };
    }, initialValue);
  };

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'inline',
    // flexDirection: 'row',
    // justifyContent: 'flex-start'
    // display: 'block-inline',
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));

export default function CheckboxesGroup({ checkboxOptions, setInputs }) {
  const classes = useStyles();
  const [state, setState] = React.useState(checkboxOptions);

  const handleChange = (event) => {
    // event.preventDefault();
    event.persist() //todo check
    setState({ ...state, c: event.target.checked });
    setInputs(inputs => ({ ...inputs, [event.target.name]: {...inputs[event.target.name], c:event.target.checked} }));
  };

// const checkboxOptions = state;
// const error = [gas, water, electricity].filter((v) => v).length !== 1;

  const checkboxComponent = (
  <FormControlLabel
    control={<Checkbox color="primary" checked={state.c} onChange={handleChange} name={checkboxOptions.f} />}
    label={checkboxOptions.f}
    // labelPlacement="top"
    // name={checkboxOption.name}
  />);

  return (
    <div className={classes.root}>
      
      {/* <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Assign responsibility</FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox checked={gas} onChange={handleChange} name="gas" />}
            label="gas"
          />
          <FormControlLabel
            control={<Checkbox checked={water} onChange={handleChange} name="water" />}
            label="water"
          />
          <FormControlLabel
            control={<Checkbox checked={electricity} onChange={handleChange} name="electricity" />}
            label="electricity"
          />
        </FormGroup>
        <FormHelperText>Be careful</FormHelperText>
      </FormControl> */}


      {/* <FormControl required error={error} component="fieldset" className={classes.formControl}> */}
      <FormControl component="fieldset" className={classes.formControl} defaultChecked={true}>
        {/* <FormLabel component="legend">{question}</FormLabel> */}
        <FormGroup row>
            <p>{checkboxOptions.w}: </p>
            {checkboxComponent}
          {/* <FormControlLabel
            control={<Checkbox checked={gas} onChange={handleChange} name="gas" />}
            label="gas"
          />
          <FormControlLabel
            control={<Checkbox checked={water} onChange={handleChange} name="water" />}
            label="water"
          />
          <FormControlLabel
            control={<Checkbox checked={electricity} onChange={handleChange} name="electricity" />}
            label="electricity"
          /> */}
        </FormGroup>
        {/* <FormHelperText>You can display an error</FormHelperText> */}
      </FormControl>
    </div>
  );
}

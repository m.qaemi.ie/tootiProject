import React, { Component } from "react";
import ReactDOM from "react-dom";
// import send_update_to_server from "./cloneclient5";
var k =0;
// send_update_to_server('e3175d25-949f-406a-af01-437fc6c1', 'player_node1');

class Form extends Component {
    constructor() {
        super();

        this.state = {
            value: ""
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const { value } = event.target;
        this.setState(() => {
            return {
                value
            };
        });
    }

    render() {
        return (
            <form>
            <input
        type="text"
        value={this.state.value}
        onChange={this.handleChange}
        />
        </form>
    );
    }
}

export default Form;

const wrapper = document.getElementById("container");
wrapper ? ReactDOM.render(<Form />, wrapper) : false;
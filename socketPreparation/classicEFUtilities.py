import json
from random import choice

steps = ['beforeStart', 'formFilling', 'formSubmitting', 'formRefereeing', 'gameResultShowing']
each_round_steps = ['formFilling', 'formSubmitting', 'formRefereeing']

classicef_game_steps_props = {'joinGame': {'duration': 30}, 'beforeStart': {'duration': 15},
                    'formFilling': {'duration': 60,}, 'formSubmitting': {'duration': 30},
                    'formRefereeing': {'duration': 30}, 'gameResultShowing': {'duration': 30}}

persian_alphabet = ['الف', 'ب', 'پ', 'ت', 'ث', 'ج', 'چ', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'ژ', 'س', 'ش', 'ص', 'ض',
                    'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ک', 'گ', 'ل', 'م', 'ن', 'و', 'ه', 'ی']

classic_game_fields = ['اسم پسر', 'اسم دختر', 'فامیل', 'شهر', 'کشور', 'میوه', 'ماشین', 'اعضای بدن', 'اشیا',
                       'مشاهیر', 'ورزش', 'فیلم',
                       'کارتون', 'شغل', 'پوشاک']


def get_form_of_round(random_selected_letters, player_selected_letters, categories,):  # todo make this forms and cache them. python cache.
    random_letter = choice([x for x in player_selected_letters if x not in random_selected_letters])  # random letter that not selected yet.
    form_round = {}
    for field in categories:
        form_item = {'l': random_letter, 'f': field, 'w': None, 'c': None,}
        # form_round.append(form_item)
        form_round[field] = form_item
    return random_letter, form_round


def classicef_action_related_update(logic_obj):
    """return dict to get updates related to each action"""
    return {
        'makeGame': {
            'action': 'prepareToStart',
            'gameStartTime': '',
            'creator': logic_obj.game_data['creator']
        },
        'joinMeToGame': {
            'action': 'prepareToStart',
            'gameStartTime': '',
            'creator': logic_obj.game_data['creator']
        },
        'startRound': {
            'action': 'fillForm',
            'rounds': {
                logic_obj.round_number: logic_obj.game_data['rounds'][logic_obj.round_number]
            }
        },
        'stopRound': {
            'action': 'submitForm',
        },
        'receiveForm': {
            'action': 'waitToReceiveFormToReferee',
        },
        'confirmResults': {
            'action': 'notifyPlayerConfirmedResults',
            'resultsConfirmed': True,
        },
        'giveTotal': {
            'action': 'showTotal',
        },
        'refereeForm': {
            'action': 'refereeForm',
            'roundData': logic_obj.round_data,
            'selectedCategories': logic_obj.game_data['configOfGame']['selectedCategories'],
            'resultsConfirmed': False,
        },
        'receiveReferee': {
            'action': 'updatePlayerRoundData',
            'playerRoundData': logic_obj.player_round_data,
            'playerRefereed': logic_obj.player_refereed,
        },
    }


def make_classicef_player_round_data(logic_obj):  # todo rename this to player_round_data_to_referee
    """make classic esm famil player round data from form completed"""
    game_players = logic_obj.game_data['gamePlayers']
    for category in logic_obj.game_data['configOfGame']['selectedCategories']:
        word = logic_obj.form_completed[category]['w']
        category_data = {'word': word}
        initial_referee = True if word else False
        category_data.update(dict.fromkeys(game_players, initial_referee))
        logic_obj.player_round_data.update({category: json.dumps(category_data).encode('utf-8')})


steps_props = {
        'steps': {
            'beforeStart': {
                'actions_allowed': ['joinMeToGame', 'startRound'],
                'action_changing_step': 'startRound',
            },
            'formFilling': {
                'actions_allowed': ['stopRound'],
                'action_changing_step': 'stopRound',
            },
            'formSubmitting': {
                'actions_allowed': ['submitForm', 'receiveForm'],  # todo we can delete submit form and all outgoing actions?
                'action_changing_step': 'stopRound',
            },
            'formRefereeing': {
                'actions_allowed': ['receiveReferee', 'startRound', 'confirmResults', ],
                'action_changing_step': '',
            },
            'gameResultShowing': {
                'actions_allowed': [],
                'action_changing_step': '',
            }
        }
}

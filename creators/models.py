from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

# from django.contrib.postgres.fields import JSONField, ArrayField
# from django.core.serializers.json import DjangoJSONEncoder

from django.contrib.auth import get_user_model

import uuid


class CommonFieldForAllModels(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length=500, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    # device_id = models.CharField(max_length=40, blank=False, null=False)
    device_id = models.UUIDField(max_length=40, blank=True, null=True)

    class Meta:
        abstract = True


class ProjectUserManager(BaseUserManager):
    def create_user(self, email=None, password=None, display_name=None,):
        """
        Creates and saves a User with the given device_id, email etc.
        """
        if not email:
            raise ValueError('Users must have an email address')
        # if not devices:
        #     raise ValueError('Users must have a device id.')
        # email = '{}@mail.com'.format(device_id)
        user = self.model(
            # devices=devices,
            email=self.normalize_email(email),
            display_name=display_name,
            # date_of_birth=date_of_birth,
            # gender=gender
        )
        # user.is_device_user = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    # def create_device_user(self, email, devices, password=None, display_name=None,):
    #     """
    #     Creates and saves a User with the given device_id, email etc.
    #     """
    #     if not email:
    #         raise ValueError('Users must have an email address')
    #     if not devices:
    #         raise ValueError('Users must have a device id.')
    #     user = self.create_user(
    #         email=email,
    #         devices=devices,
    #         display_name=display_name,
    #         # date_of_birth=date_of_birth,
    #         # gender=gender,
    #         password=password,
    #     )
    #     user.save(using=self._db)
    #     return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password etc.
        """
        user = self.create_user(
            email,
            # display_name=display_name,
            # date_of_birth=date_of_birth,
            # gender=gender,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

    # def create_logined_user(self, device_id, display_name=None):  # todo this is not meaningful when we had created user with device_id before.
    #     # todo we need a function for assign email of logined user to current device_id fake email. or if the user were logined from another device we will
    #     # todo send records of that user to current device and replace current device fake email with email of this user and make
    #     """
    #     Creates and saves a divice user with the given device id.
    #     """
    #     user = self.create_user(
    #         # email,
    #         device_id=device_id,
    #         display_name=display_name,
    #         # date_of_birth=date_of_birth,
    #         # gender=gender,
    #         # password=password,
    #     )
    #     # user.is_device_user = True
    #     user.save(using=self._db)
    #     return user


class ProjectUser(AbstractBaseUser):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    # device_id = models.UUIDField(blank=False, null=False, editable=False)
    # device_id = models.CharField(max_length=100, blank=False, null=False)
    # devices = ArrayField(
    #         models.CharField(max_length=30, blank=True), size=100,blank=True, null=True
    # )
    # devices = JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)  # todo check null blank
    email = models.EmailField(
        # verbose_name='email address',
        max_length=255,
        unique=True,
    )

    display_name = models.CharField(max_length=150, blank=True, null=True)  # what should we call you? # todo how many char
    # country = models.CharField(max_length=80, blank=True, null=True)   # todo choices?
    # gender = models.CharField(
    #     max_length=1,
    #     choices=(
    #         ('m', 'Male'),
    #         ('f', 'Female'),
    #     ),
    #     help_text='select your gender',
    #     null=True,
    # )
    # date_of_birth = models.DateField()
    # phone = models.BigIntegerField(blank=True, null=True)
    # mobile = models.BigIntegerField(blank=True, null=True)
    # address = models.CharField(max_length=255, blank=True, null=True)
    avatar = models.ImageField(blank=True, null=True)
    last_seen = models.DateTimeField(blank=True, null=True)

    # otherChars = JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)  # other characteristics of ProjectUser

    # is_device_user = models.BooleanField(default=True)  # todo
    # is_logined_user = models.BooleanField(default=False)
    # is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_online = models.BooleanField(default=False)
    # is_staff = models.BooleanField(default=False)
    is_blocked = models.BooleanField(default=False)

    objects = ProjectUserManager()

    USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['display_name', 'date_of_birth', 'gender', ]
    # REQUIRED_FIELDS = ['device_id']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        # "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        # "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    @property
    def is_blocked(self):
        # "Is the user blocked?"
        return self.is_blocked


    # First Name and Last Name do not cover name patterns
    # around the globe.
    # name = CharField(_("Name of User"), blank=True, max_length=255)

    # def get_absolute_url(self):  todo
    #     return reverse("users:detail", kwargs={"username": self.username})


# class Device(CommonFieldForAllModels):
#     device_id = models.UUIDField(blank=False, null=False)
#     user = models.ForeignKey(get_user_model(), blank=False, null=False, on_delete=models.DO_NOTHING)

class Player(CommonFieldForAllModels):
    player = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING)


class Referee(CommonFieldForAllModels):
    referee = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING)


class UserSettings(CommonFieldForAllModels):
    settingsOwner = models.OneToOneField(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING)


class Media(CommonFieldForAllModels):
    creator = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING, help_text='creator can release or create widget.')


class GameWidget(CommonFieldForAllModels):
    medias = models.ForeignKey(Media, blank=False, null=False, on_delete=models.DO_NOTHING)
    creator = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING, help_text='creator can release or create widget.')


class GameEnvironment(CommonFieldForAllModels):
    gameWidget = models.ForeignKey(GameWidget, blank=False, null=False, on_delete=models.DO_NOTHING)
    creator = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING, help_text='creator can release or create widget.')
    # duration = models.DurationField()
    # number_of_player = models.PositiveIntegerField()


class Game(CommonFieldForAllModels):
    gameEnvironment = models.ForeignKey(GameEnvironment, blank=True, null=True, on_delete=models.DO_NOTHING)  # todo GameEnvironment can't be null. correct this after clients would determine.
    creator = models.ForeignKey(Player, related_name='creator', blank=True, null=True, on_delete=models.DO_NOTHING, help_text='playOwner creates game.')
    player = models.ManyToManyField(Player)
    referee = models.ForeignKey(Referee, blank=True, null=True, on_delete=models.DO_NOTHING)
    is_full = models.BooleanField()
    typeOfReferee = models.CharField(
        max_length=1,
        choices=(
            ('o', 'playOwner'),
            ('c', 'crossReferee'),  # todo increase
            ('s', 'server'),  # todo increase
            ('r', 'random'),  # todo increase
            ('t', 'thirdParty'),  # todo increase
        )
        , blank=True, null=True
    )


class RoundWidget(CommonFieldForAllModels):
    creator = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING,
                              help_text='creator can release or create widget.')


class Topic(CommonFieldForAllModels):
    title = models.CharField(max_length=100)


class FormTemplate(CommonFieldForAllModels):
    # creator = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING)
    topics = models.ManyToManyField(Topic)


class RoundEnvironment(CommonFieldForAllModels):
    roundWidget = models.ForeignKey(RoundWidget, blank=False, null=False, on_delete=models.DO_NOTHING)
    # FormTemplate = models.ForeignKey(FormTemplate, blank=False, null=False, on_delete=models.DO_NOTHING)
    creator = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING, help_text='creator can release or create widget or env. .')


class Round(CommonFieldForAllModels):
    game = models.ForeignKey(Game, blank=True, null=False, on_delete=models.DO_NOTHING)
    roundEnvironment = models.ForeignKey(RoundEnvironment, blank=True, null=True, on_delete=models.DO_NOTHING) # todo null = false in production.
    # formTemplate = models.ForeignKey(FormTemplate, blank=True, null=True, on_delete=models.DO_NOTHING) # todo make these false in produciton,
    # players = models.ManyToManyField(Player, blank=False, help_text='players assigned to round.')  # todo blank = false in production?
    # players = models.ManyToManyField(get_user_model(), blank=True, help_text='players assigned to round.')  # todo blank = false in production?


class Letter(CommonFieldForAllModels):
    pass


class Word(CommonFieldForAllModels):
    topics = models.ManyToManyField(Topic)
    letter = models.ForeignKey(Letter, blank=False, null=False, on_delete=models.DO_NOTHING)


class Achievement(CommonFieldForAllModels):
    admin = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=200)
    count = models.BigIntegerField()
    icon = models.ImageField()

import React, { useEffect, useState } from 'react';
import CounterDown from '../CounterDown/CounterDown';
import EnhancedTable from '../MaterialUiComponents/EnhancedTable';

import './GameResult.css';

const GameResult = ({ state, sendMessage, }) => {
  let { name, playerId, gameId, gameName, step, action, roundNumber, startNumber, rounds, totalResult } = state;
  const [isActive, setIsActive] = useState(true);
  const [inputs, setInputs] = useState({});
  const [inputDisabled, setInputDisabled] = useState(false);
  // const formFields = rounds[roundNumber].playersForms[playerId].form;

  const handleInputChange = (event) => {
    event.persist();
    setInputs(inputs => ({ ...inputs, [event.target.name]: event.target.value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setInputDisabled(inputDisabled => true);
    let message = { playerId, gameId, roundNumber, step:'gameResult', action: 'giveTotal', };

    if (isActive) {
      sendMessage('sendUpdate', message);
    }
  };
  const headCells = [
    { id: 'playerId', numeric: false, disablePadding: true, label: 'Player Id' },
    { id: 'nr_c', numeric: true, disablePadding: false, label: 'Number Of Correct' },
    { id: 'nr_inc', numeric: true, disablePadding: false, label: 'Number of Incorrect' },
    { id: 'nr_nf', numeric: true, disablePadding: false, label: 'number Of Not Filled' },
  ];
  console.log(totalResult);
  return (
  // <form onSubmit={handleSubmit}>
    <div>
      <CounterDown startNumber={startNumber} />
      {/*<form onSubmit={sendMessage}>*/}
      {/* <form onSubmit={handleSubmit}> */}
      <EnhancedTable headCells={headCells} rows={totalResult} />
      {/*<form>*/}
        {/* eslint-disable-next-line react/button-has-type */}
        {/* {form} */}
        {/* <button disabled = {(inputDisabled)? "disabled" : ""}>Send game result</button> */}
        {/*<button className="sendButton" onClick={(e) => sendMessage(e)}>Send</button>*/}
      {/* </form> */}
    </div>
  );
};

export default GameResult;

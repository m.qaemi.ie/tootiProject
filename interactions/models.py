from django.db import models
from creators.models import CommonFieldForAllModels, FormTemplate, Word, Achievement, Player, Referee, Round
from django.contrib.auth import get_user_model

from django.contrib.postgres.fields import JSONField
from django.core.serializers.json import DjangoJSONEncoder


class Version(CommonFieldForAllModels):  # todo add api for sending version to server for admins.
    admin = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    versionCode = models.BigIntegerField(blank=False, null=False)
    versionName = models.CharField(max_length=100, blank=False, null=False)
    force_update = models.BooleanField(default=False)


class DeviceInstalledApp(CommonFieldForAllModels):
    # app_version_installed = models.ForeignKey(Version, to_fields=["versionCode", "versionName"], on_delete=models.DO_NOTHING, blank=False, null=False) # todo in production make blank=True, null=True
    app_version_installed = models.ForeignKey(Version, on_delete=models.DO_NOTHING, blank=False, null=False) # todo in production make blank=True, null=True


class PlayerFillsForm(CommonFieldForAllModels):
    round = models.ForeignKey(Round, blank=True, null=True, on_delete=models.DO_NOTHING)
    player = models.ForeignKey(Player, blank=False, null=False, on_delete=models.DO_NOTHING)
    formTemplate = models.ForeignKey(FormTemplate, blank=True, null=True, on_delete=models.DO_NOTHING)
    form = JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
    referee = models.ForeignKey(Referee, blank=False, null=False, on_delete=models.DO_NOTHING)


class UserReportsWord(CommonFieldForAllModels):  # todo
    reporter = models.ForeignKey(get_user_model(), blank=False, null=False, on_delete=models.DO_NOTHING)
    word = models.ForeignKey(Word, blank=False, null=False, on_delete=models.DO_NOTHING)
    typeOfProblem = models.CharField(
        max_length=3,
        choices=(
            ('na', 'topic not appropriate to word ane vise versa.'),
            ('o', 'offensive word'),  # todo increase
            ('t', 'typo'),  # todo increase
        )
    )


class UserObtainsAchievements(CommonFieldForAllModels):
    achiever = models.ForeignKey(get_user_model(), blank=False, null=False, on_delete=models.DO_NOTHING)
    achievement = models.ForeignKey(Achievement, blank=False, null=False, on_delete=models.DO_NOTHING)
    count = models.BigIntegerField()

# class AdminCreatesModels(CommonFieldForAllModels):   # todo build abstract model for this work
#     admin = models.ForeignKey(get_user_model(), blank=False, null=False, on_delete=models.DO_NOTHING)
#
#     class Meta:
#         abstract = True

def convert(data):
    if isinstance(data, bytes): return data.decode('utf8')
    if isinstance(data, dict): return dict(map(convert, data.items()))
    if isinstance(data, tuple): return map(convert, data)
    return data


const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
    module: {
        // noParse: /zeromq/,
        rules: [
            // {
            //     test: /\.(node)$/,
            //     exclude: /zeromq/ ,
            //     use: {
            //         loader: "babel-loader"
            //     }
            // },
            {
                test: /\.(js|jsx)$/,
                // exclude: /node_modules/ |'C:\\Users\\Home\\OneDrive\\esmFamilProject981211\\esmFamilwebpack\\src\\js\\components\\cloneclient5.js',
                // exclude: [/node_modules/ ,/components/],
                exclude: /node_modules/ ,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/index.html",
            filename: "./index.html"
        })
    ]
};
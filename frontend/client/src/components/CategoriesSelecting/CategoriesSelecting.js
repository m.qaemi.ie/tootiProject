import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// core components
import Header from "../MaterialKitComponents/Header.js";
import HeaderLinks from "../MaterialKitComponents/HeaderLinks.js";
import Footer from "../MaterialKitComponents/Footer.js";
import GridContainer from "../MaterialKitComponents/GridContainer.js";
import GridItem from "../MaterialKitComponents/GridItem.js";
import Button from "../MaterialKitComponents/Button.js";
import Card from "../MaterialKitComponents/Card.js";
import CardBody from "../MaterialKitComponents/CardBody.js";
import CardHeader from "../MaterialKitComponents/CardHeader.js";
import CardFooter from "../MaterialKitComponents/CardFooter.js";
import CustomInput from "../MaterialKitComponents/CustomInput.js";
import ChipsList from "../MaterialUiComponents/ChipsList.js";
import Select from "../MaterialUiComponents/Select.js";
import styles from "../assets/jss/views/loginPage.js";
import { Redirect } from "react-router-dom";

import image from "../assets/img/bg7.jpg";
import LinksToJoinGames from "../LinksToJoinGames/LinksToJoinGames";

const categories = ['اسم پسر', 'اسم دختر', 'فامیل', 'شهر', 'کشور', 'میوه', 'ماشین', 'اعضای بدن', 'اشیا',
'مشاهیر', 'ورزش', 'فیلم',
'کارتون', 'شغل', 'پوشاک'];

function makeid(length) {  // todo do this server-side.
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const useStyles = makeStyles(styles);

export default function CategoriesSelecting() {
// export default function MakeOrJoinGame(props) {
  
  const [cardAnimaton, setCardAnimation] = useState("cardHidden");
  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();
  // const { ...rest } = props;
  const [selectedCategories, setSelectedCategories] = useState(['اسم پسر', 'اسم دختر', 'فامیل']);
  const [redirect, setRedirect] = useState("");
  
  setTimeout(function() { // redirects page to home page after 5 minutes.
    setRedirect("/");
  }, 5 * 60 * 1000);
  console.log('player_auth value:', Cookies.getJSON('player_auth') || {});
  let {access_token='', name='', playerId='',} = Cookies.getJSON('player_auth') || {};
  // if player auth was in cookies it will use them but if was not in cookies it will generate them.
  if (!name || !playerId) { //todo check expiration and refresh of token... expiration of playerId and name cookies should be equal to access token.
    let names = ['wasp', 'gaur', 'butterfly', 'bee', 'ant'];
    name = names[Math.floor(Math.random() * names.length)]; // select random from names
    playerId = makeid(6);
    const expireseconds= new Date(new Date().getTime() + 3 * 1000); // after 15 seconds! for test... todo
    Cookies.set('player_auth', { name , playerId }, { expires: expireseconds });
    console.log('random name and player id assigned.', name, playerId);
  } else {
    console.log('player id and name exists because access token exists.', );
  }

  if (redirect) {
    return <Redirect to={redirect} />
  }
  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="Esm Famil"
        rightLinks={<HeaderLinks />}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <h4>Login</h4>
            <h4>player id: {playerId}</h4>
            <h4>name: {name}</h4>
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Create A New Game</h4>
                  </CardHeader>
                  {/* <p className={classes.divider}>Make Game</p> */}
                  <CardBody>
                    <ChipsList items={categories} selectedItems={selectedCategories} setSelectedItems={setSelectedCategories} minOfSelectedItems={3} maxOfSelectedItems={12}/>
                    <Link to={{pathname:'/game-props', state: { selectedCategories }}}>
                      <Button simple color="primary" size="lg">
                      Create New Game
                      </Button>
                    </Link>
                    <LinksToJoinGames/>
                  </CardBody>
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

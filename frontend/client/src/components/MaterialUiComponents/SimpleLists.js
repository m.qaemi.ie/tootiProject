import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

export default function SimpleList({items}) {
  const classes = useStyles();

  const listItems = items.map((item)=> 
        <ListItemLink href="#simple-list">
            <ListItemText primary={item} />
        </ListItemLink>
    );
  return (
    <div className={classes.root}>
        <h3>players joined game:</h3>
      <Divider />
      <List component="nav" aria-label="secondary mailbox folders">
        {listItems}
      </List>
    </div>
  );
}

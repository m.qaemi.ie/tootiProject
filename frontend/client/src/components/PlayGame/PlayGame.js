// do not keep component in states.
import React, { Component } from 'react';
import queryString from 'query-string';
// import socketIOClient from "socket.io-client";
import io from "socket.io-client";
import Cookies from 'js-cookie';

import GameProgress from '../GameProgress/GameProgress';
import handleUpdate from './handleUpdate';
import './PlayGame.css';
require('fast-text-encoding');
const endpoint = `http://${process.env.REACT_APP_IP_ADDRESS_OF_SERVER}:${process.env.REACT_APP_PORT_OF_SERVER}`;

function makeid(length) {  // todo do this server-side.
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

let names = ['wasp', 'gaur', 'butterfly', 'bee', 'ant'];
// var socket_client = {
  
//   socket: null,
//   connect: function() {
//     var self = this;
//     if( self.socket ) {
//       self.socket.destroy();
//       delete self.socket;
//       self.socket = null;
//     }
//     this.socket = io.connect( endpoint, {
//       reconnection: true,
//       reconnectionDelay: 1000,
//       reconnectionDelayMax : 5000,
//       reconnectionAttempts: Infinity
//     } );
//     this.socket.on( 'connect', function () {
//       console.log( 'connected to server' );
//     } );
//     this.socket.on( 'disconnect', function () {
//       console.log( 'disconnected from server' );
//       window.setTimeout( 'socket_client.connect()', 5000 );
//     } );
//   }
  
//   }; // var app

// const socket = new socketIOClient(endpoint, {
//   path: '/',
//   // 'forceNew':true,
//   transports: ['websocket'],
//   'reconnection': true,
//   'reconnectionDelay': 500,
//   'reconnectionDelayMax' : 5000,
//   'reconnectionAttempts': 10
// });

class PlayGame extends Component {
  constructor(props) {
    super(props);
    const {playerId, name} = Cookies.getJSON('player_auth') || {playerId: makeid(6), name: names[Math.floor(Math.random() * names.length)],}; // todo what if saving to cookies not allowed from user.
    this.state = {
      response: false,
      playerId: playerId,
      name: name,
      // player: Cookies.getJSON('player_auth'), // {name: '',playerId: ''}
      gameId:'',
      gameName:'',
      creator:'',
      players:[],
      playersData:{},
      creator:'',
      step:'joinGame',
      action:'',
      roundNumber:1,
      startNumber:30,
      rounds:{},
      uuidForGameId:'',
      playerRefereed:'', // the player id of player whose form is refereed.
      // roundFormsReceived:'', // round forms received from players.
      categories:[],
      roundData:{}, // includes playersParticipatedInRound and categories and forms recieved from players
      lastRound:'', // number of last round // change name of this state to lastRoundNumber
      totalResult:'',
      activeGames:[],
      playerRequestedUpdate:'',
      playersNotSentForm: [], //players not sent form of this round.
      playersSentForm: [],
      resultsConfirmed: false,
    };
  }

  socket = null;
  isConnected= false;
  interval= null;

  changeStep = (step) => {
    this.setState({step});
  };

  sendMessage = (event, message, messageWithMetaData=null) => {
    message = messageWithMetaData?messageWithMetaData:{ name:this.state.name, gameId:this.props.match.params.gameId, playerId:this.state.playerId, message };
    if (this.socket.connected) {
      console.log('socket.connected:', this.socket.connected, this.socket.id);
      this.socket.emit(event, message, () => {});
    } else {
      console.log('socket.connected:', this.socket.connected, this.socket.id);
      // this.socket.disconnect();
      // this.socket.connect();
      console.log('reconnected:', this.socket.connected, this.socket.id);
      this.socket.emit(event, message, () => {});
    }
  };

  componentDidMount() {
    // this.setState({gameId: this.props.match.params.gameId});
    console.log(Cookies.getJSON(), this.props.match.params.gameId);
    this.connect = () => {
      if (this.socket) {
        this.socket.destroy();
        delete this.socket;
        this.socket = null;
      }
      this.socket = io.connect(endpoint, {
        transports: ['websocket'], // todo check websocket or polling? should be changed client and server side.
        path:'/',
        reconnection: false,
      });
      this.socket.on('connect', () => {
        this.isConnected = true;
        this.sendMessage('addMeToRoom', {});
        console.log('emitted addmetoroom.');
        // this.socket.emit('authentication', { user_id: 2751, token: "abc" });
        // this.socket.on('authenticated', function() {
  
        // });
      });
  
      this.socket.on('disconnect', (reason) => {
        console.log('reason:', reason);
        this.isConnected = false;
        this.interval = window.setInterval(() => {
          if (this.isConnected) {
            clearInterval(this.interval);
            this.interval = null;
            return;
          }
          this.connect()
        }, 5000);
      });
  
      return this.socket;
    };
  
    this.connect();
    console.log(this.state);
    let {gameName = '', action = 'joinMeToGame', configOfGame = '' } = this.props.location.state || {};
    let message = {};
    if (action==='makeGame'){
      message = {playerId:this.state.playerId, playerName:this.state.name, gameId:this.props.match.params.gameId, gameName, roundNumber:1, step:'beforeStart', action , configOfGame};
      window.history.replaceState(null, '');
      this.sendMessage('sendSnapshot', message);
    } else {
      message = {playerId:this.state.playerId, playerName:this.state.name, gameId:this.props.match.params.gameId, roundNumber:1, step:'beforeStart', action};
      this.sendMessage('sendUpdate', message);
    }

    this.socket.on('snapshot', (updateMessage) => {  // todo add check of connection socket.on? no you should add reconnectoin after catching disconnect or check it in interval.
      const update_message = JSON.parse(updateMessage);
      console.log('i received snapshot., ', updateMessage);
      try {
        const statesToSet = handleUpdate(update_message, this.state, this.sendMessage);
        this.setState(statesToSet);
      } catch (e) {
        console.log(e);
      }
      console.log('i recieved snapshot');
    });

    this.socket.on('update', (updateMessage) => {
      const update_message = JSON.parse(updateMessage);
      console.log('i received update., ', updateMessage);
      try {
        const statesToSet = handleUpdate(update_message, this.state, this.sendMessage);
        this.setState(statesToSet);
      } catch (e) {
        console.log(e);
      }
      console.log('i recieved update');
    });
    this.socket.on('ping', () => {
      console.log('i sent ping');
    });
    this.socket.on('pong', () => {
      console.log('i received pong');
    });
    this.socket.on('connect_error', () => {
      console.log('i received connect_error');
    });
    this.socket.on('connect_timeout', () => {
      console.log('i received connect_timeout');
    });
    this.socket.on('reconnect', () => {
      console.log('i received reconnect');
    });
    this.socket.on('reconnect_attempt', () => {
      console.log('i received reconnect_attempt');
    });
    this.socket.on('reconnecting', () => {
      console.log('i received reconnecting');
    });
    this.socket.on('reconnect_error', () => {
      console.log('i received reconnect_error');
    });
    this.socket.on('pong', () => {
      console.log('i received reconnect_failed');
    });


    // this.socket.on('disconnect', (reason) => { //todo implement reconnection, etc.
      // if (reason === 'io server disconnect') {
      //   // the disconnection was initiated by the server, you need to reconnect manually
      //   console.log('server disconnected:', socket.id);
      //   socket.connect();
      //   console.log('server connected:', socket.id);
      // }
      // socket.close();
      // socket.open();
    // });
  }

  componentWillUnmount() {
    // socket.emit('disconnectedFromRoom', {gameId:this.state.gameId, playerId:this.state.playerId});
    if (this.socket) {
      this.socket.emit('disconnect');
      this.socket.disconnect()
      console.log('emit disconnect');
      // clearInterval(interval);
      // socket.off();  //is this needed?
    }
  }

  render() {
    setTimeout(()=>console.log('socket.connected:', this.socket.connected, this.socket.id), 2000);
    // const { response } = this.state;
    return (
        <div style={{ textAlign: "center" }}>
          <h4>player id: {this.state.playerId}</h4>
          <h4>name: {this.state.name}</h4>
          <h4>roundNumber: {this.state.roundNumber}</h4>
          <h4>gameName: {this.state.gameName}</h4>
          <GameProgress 
            state = {this.state}
            sendMessage={this.sendMessage}
            changeStep={this.changeStep}
            />
          {/* <pre></pre> */}
          {/* {response
              ? <p>
                The temperature in Florence is: {response} °F
              </p>
              : <p>Loading...</p>} */}
        </div>
    );
  }
}
export default PlayGame;